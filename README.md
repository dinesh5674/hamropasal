# README #

This application is under development using Android Studio and this project can be cloned and run in Android studio. 

### What is this repository for? ###

* This application is supposed to be developed in a team.
* Bit Bucket is used as Version control system
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Contribution by Dinesh Sapkota ###

* SQLite is used to store data locally
* DBHelper.java class is created for database and tables creation.(can be found in dbModel folder) 
* Database model objects are created to facilitate the CRUD operation.(can be found in dbModel folder)
* Database Helpers are created to perform the CRUD operations of objects.(can be found in CRUD folder)
* CustomSelector.java class is created to provide the filtering and suggestion features according to the database entries for EditText.(can be found in Custom folder)

### Who do I talk to? ###

* Repo owner or admin (Dinesh Sapkota)
* Contact(dinesh5674@gmail.com)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions