package com.example.dinesh.hamropasal.Custom;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.dinesh.hamropasal.R;
import com.example.dinesh.hamropasal.dbModel.PBST_Name;

import java.util.ArrayList;

public class CreditViewDetailAdapter extends BaseAdapter {

    private ArrayList<PBST_Name> data;
    private Activity mActivity;
    private LayoutInflater inflater = null;

    public CreditViewDetailAdapter(Activity context, int textViewResourceId, ArrayList<PBST_Name> result) {
        mActivity = context;
        data = result;
        inflater = (LayoutInflater) mActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (convertView == null)
            vi = LayoutInflater.from(mActivity).inflate(R.layout.credit_view_detail_adapter,
                    null);

        TextView product_name= (TextView)vi.findViewById(R.id.product_name);
        TextView brand_name = (TextView)vi.findViewById(R.id.brand_name);
        TextView type_name = (TextView)vi.findViewById(R.id.type_name);
        TextView size_name = (TextView)vi.findViewById(R.id.size_name);
        TextView soldPrice = (TextView)vi.findViewById(R.id.soldPrice);
        TextView soldDate = (TextView)vi.findViewById(R.id.soldDate);

        product_name.setText(data.get(position).getProduct_Name());
        brand_name.setText(data.get(position).getBrand_Name());
        type_name.setText(data.get(position).getType_Name());
        size_name.setText(data.get(position).getSize_Name());
        soldPrice.setText(data.get(position).get_Sold_Price());
        soldDate.setText(data.get(position).get_Sold_Date());


        // SECOND PLACE TO INSERT THE onClickListener
        return vi;
    }
}
