package com.example.dinesh.hamropasal.dbModel;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ANONYMOUS_PAL on 4.7.2016.
 */
public class Details implements Parcelable{

    private int _Detail_Id;
    private int _Product_Id;
    private int _Brand_Id;
    private int _Type_Id;
    private int _Size_Id;
    private int _Creditor_Id;
    private String _Cost_Price;
    private String _Sold_Price;
    private String _Purchase_DateTime;
    private String _Sold_DateTime;
    private String _Product_Code;
    private String _Profit;


    public Details() {
    }

    protected Details(Parcel in) {
        _Detail_Id = in.readInt();
        _Product_Id = in.readInt();
        _Brand_Id = in.readInt();
        _Type_Id = in.readInt();
        _Size_Id = in.readInt();
        _Cost_Price = in.readString();
        _Sold_Price = in.readString();
        _Purchase_DateTime = in.readString();
        _Sold_DateTime = in.readString();
        _Product_Code = in.readString();
        _Profit = in.readString();
    }

    public static final Creator<Details> CREATOR = new Creator<Details>() {
        @Override
        public Details createFromParcel(Parcel in) {
            return new Details(in);
        }

        @Override
        public Details[] newArray(int size) {
            return new Details[size];
        }
    };

    public void set_Detail_Id(int _Detail_Id) {
        this._Detail_Id = _Detail_Id;
    }

    public void set_Creditor_Id(int _Creditor_Id){
        this._Creditor_Id = _Creditor_Id;
    }

    public void set_Product_Id(int _Product_Id) {
        this._Product_Id = _Product_Id;
    }

    public void set_Brand_Id(int _Brand_Id) {
        this._Brand_Id = _Brand_Id;
    }

    public void set_Type_Id(int _Type_Id) {
        this._Type_Id = _Type_Id;
    }

    public void set_Size_Id(int _Size_Id) {
        this._Size_Id = _Size_Id;
    }

    public void set_Cost_Price(String _Cost_Price) {
        this._Cost_Price = _Cost_Price;
    }

    public void set_Sold_Price(String _Sold_Price) {
        this._Sold_Price = _Sold_Price;
    }

    public void set_Purchase_DateTime(String _Purchase_DateTime) {
        this._Purchase_DateTime = _Purchase_DateTime;
    }

    public void set_Sold_DateTime(String _Sold_DateTime) {
        this._Sold_DateTime = _Sold_DateTime;
    }

    public void set_Product_Code(String  _Product_Code) {
        this._Product_Code = _Product_Code;
    }
    public void set_Profit(String  _Profit) {
        this._Profit = _Profit;
    }

    public int get_Product_Id() {
        return _Product_Id;
    }

    public int get_Brand_Id() {
        return _Brand_Id;
    }

    public int get_Type_Id() {
        return _Type_Id;
    }

    public int get_Size_Id() {
        return _Size_Id;
    }

    public int get_Detail_Id() {
        return _Detail_Id;
    }

    public int get_Creditor_Id(){
        return _Creditor_Id;
    }

    public String get_Cost_Price() {
        return _Cost_Price;
    }

    public String get_Sold_Price() {
        return _Sold_Price;
    }

    public String get_Purchase_DateTime() {
        return _Purchase_DateTime;
    }

    public String get_Sold_DateTime() {
        return _Sold_DateTime;
    }

    public String  get_Product_Code() {
        return _Product_Code;
    }
    public String  get_Profit() {
        return _Profit;
    }




    @Override
    public String toString() {
        return this._Product_Id+ " "+ this._Brand_Id+ " " + this._Type_Id+ " "+ this._Size_Id+ " "
              +this._Cost_Price+ "   " + this._Sold_Price+ "   " + this._Purchase_DateTime+ "   "
               + this._Sold_DateTime+ "   " + this._Product_Code+ "   " + this._Profit+ "   "+ this._Creditor_Id+ "   " ;
    }


    @Override
    public int hashCode() {
        final int prime=31;
        int result=1;
        result=prime * result + _Product_Id;
        result=prime * result + _Brand_Id;
        result=prime * result + _Type_Id;
        result=prime * result + _Size_Id;

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        Details pbst;
        if(!(obj instanceof Details)){
            return false;
        }else{
            pbst=(Details) obj;
            /**if(this.Product_Name.equals(pbst.getProduct_Name()) && this.Brand_Name.equals(pbst.getBrand_Name())
             && this.Type_Name.equals(pbst.getType_Name()) && this.Size_Name.equals(pbst.getSize_Name())){
             return true;
             }**/

            if (   this._Product_Id== pbst.get_Product_Id() &&  this._Brand_Id ==pbst.get_Brand_Id()
                    &&  this._Type_Id==pbst.get_Type_Id() && this._Size_Id==pbst.get_Size_Id())
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {


        parcel.writeInt(_Detail_Id);
        parcel.writeInt(_Product_Id);
        parcel.writeInt(_Brand_Id);
        parcel.writeInt(_Type_Id);
        parcel.writeInt(_Size_Id);
        parcel.writeString(_Cost_Price);
        parcel.writeString(_Sold_Price);
        parcel.writeString(_Purchase_DateTime);
        parcel.writeString(_Sold_DateTime);
        parcel.writeString(_Product_Code);
        parcel.writeString(_Profit);
    }
}
