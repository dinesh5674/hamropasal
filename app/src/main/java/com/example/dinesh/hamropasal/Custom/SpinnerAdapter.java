package com.example.dinesh.hamropasal.Custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.dinesh.hamropasal.R;
import com.example.dinesh.hamropasal.dbModel.SalesAndProfit;

import java.util.ArrayList;

/**
 * Created by dinesh on 9/1/2016.
 */
public class SpinnerAdapter extends ArrayAdapter<SalesAndProfit> {

    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<SalesAndProfit> salesAndprofitArray;

    public SpinnerAdapter(Context context, int textViewResourceId, ArrayList <SalesAndProfit> salesAndprofitArray){
        super(context, textViewResourceId, salesAndprofitArray);
        this.context = context;
        this.salesAndprofitArray = salesAndprofitArray;
    }

    @Override
    public int getCount() {
        return salesAndprofitArray.size();
    }

    @Override
    public SalesAndProfit getItem(int position) {
        return salesAndprofitArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {


        SalesAndProfit Sales_Profit = salesAndprofitArray.get(position);


        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout4spinnerview, viewGroup, false);
        }



        if(Sales_Profit != null) {


            TextView month_name= (TextView)convertView.findViewById(R.id.Month_Name);
            TextView total_sale = (TextView)convertView.findViewById(R.id.Total_Sale);
            TextView total_profit = (TextView)convertView.findViewById(R.id.Total_Profit);

            month_name.setText(Sales_Profit.get_Month_Name());
            total_sale.setText(Sales_Profit.get_Monthly_Sales());
            total_profit.setText(Sales_Profit.get_Monthly_Profit());

        }



        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position,convertView,parent);
    }




}
