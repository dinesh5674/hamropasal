package com.example.dinesh.hamropasal;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dinesh.hamropasal.CRUD.CreditHelper;
import com.example.dinesh.hamropasal.CRUD.DBHelper;
import com.example.dinesh.hamropasal.CRUD.DetailHelper;
import com.example.dinesh.hamropasal.Custom.CustomSelector;
import com.example.dinesh.hamropasal.Custom.VariableInitializer;
import com.example.dinesh.hamropasal.dbModel.Brands;
import com.example.dinesh.hamropasal.dbModel.Credits;
import com.example.dinesh.hamropasal.dbModel.Details;
import com.example.dinesh.hamropasal.dbModel.PBST_Name;
import com.example.dinesh.hamropasal.dbModel.Products;
import com.example.dinesh.hamropasal.dbModel.Sizes;
import com.example.dinesh.hamropasal.dbModel.Types;

import java.util.Calendar;

public class SalesCreditForm extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener{

    private DBHelper myDB;
    private DetailHelper myDetail;
    private CreditHelper myCredit;
    private Details details;
    private int detailID;

    private FrameLayout customSelector;
    private FrameLayout salesFrameSearch;
    private FrameLayout proCodeFrame;


    boolean isUpdated;

    CustomSelector cusSelect;
    VariableInitializer varInit;

    EditText firstName, lastName, phoneNumber, costPrice, purchaseDate, productCode;
    TextView soldDate;
    EditText soldPrice;
    Switch creditSwitch;
    Button save;
    Details detailObject;
    PBST_Name relationNameObject;

    int pYear, pMonth, pDay;

    int creditor_id, profit;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sales_credit_form);
        super.onCreateDrawer();



        myDB = new DBHelper(this);
        myDetail = new DetailHelper(this);
        myCredit = new CreditHelper(this);

        cusSelect = new CustomSelector(this);
        varInit = new VariableInitializer(this);


        intent = getIntent();

        customSelector = (FrameLayout) findViewById(R.id.customSelector);
        salesFrameSearch = (FrameLayout) findViewById(R.id.salesFrameSearch);
        proCodeFrame = (FrameLayout) findViewById(R.id.proCodeFrame);

        customSelector.addView(cusSelect);
        //get the view of list view in front
        customSelector.bringToFront();
        customSelector.invalidate();

        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        phoneNumber = (EditText) findViewById(R.id.phoneNumber);

        costPrice = (EditText) findViewById(R.id.costPrice);
        purchaseDate = (EditText) findViewById(R.id.purchasedDate);
        productCode = (EditText) findViewById(R.id.productCode);

        //detail_Id = (EditText) findViewById(R.id.detail_Id);
        soldPrice = (EditText) findViewById(R.id.soldPrice);
        soldDate = (TextView) findViewById(R.id.soldDate);

        save = (Button) findViewById(R.id.save);
        creditSwitch = (Switch) findViewById(R.id.creditBtn);

        if (intent.hasExtra("inventoryObject")) {
            detailObject = intent.getParcelableExtra("inventoryObject");
            System.out.println("Inventory Object: " + detailObject);
            getRelationNames(detailObject);

            cusSelect.productID = String.valueOf(detailObject.get_Product_Id());
            cusSelect.brandID = String.valueOf(detailObject.get_Brand_Id());
            cusSelect.sizeID = String.valueOf(detailObject.get_Size_Id());
            cusSelect.typeID = String.valueOf(detailObject.get_Type_Id());

            costPrice.setText(String.valueOf(detailObject.get_Cost_Price()));
            purchaseDate.setText(detailObject.get_Purchase_DateTime());
            productCode.setText(detailObject.get_Product_Code());
            detailID = detailObject.get_Detail_Id();

            soldPrice.setVisibility(View.INVISIBLE);
            soldDate.setVisibility(View.INVISIBLE);
            creditSwitch.setVisibility(View.INVISIBLE);
            save.setText("Update");
        }

        if (intent.hasExtra("salesObject")) {
            detailObject = intent.getParcelableExtra("salesObject");
            System.out.println("Filtered Object: " + detailObject);
            getRelationNames(detailObject);

            cusSelect.productID = String.valueOf(detailObject.get_Product_Id());
            cusSelect.brandID = String.valueOf(detailObject.get_Brand_Id());
            cusSelect.sizeID = String.valueOf(detailObject.get_Size_Id());
            cusSelect.typeID = String.valueOf(detailObject.get_Type_Id());

            costPrice.setText(String.valueOf(detailObject.get_Cost_Price()));
            purchaseDate.setText(detailObject.get_Purchase_DateTime());
            productCode.setText(detailObject.get_Product_Code());
            detailID = detailObject.get_Detail_Id();
        }


        if (intent.hasExtra("salesProductCodeObject")) {
            relationNameObject = intent.getParcelableExtra("salesProductCodeObject");
            System.out.println("Product Code Object: " + relationNameObject.get_Product_Code() + relationNameObject.get_Detail_Id());

            getRelationID(relationNameObject);

            cusSelect.productName.setText(relationNameObject.getProduct_Name());
            cusSelect.brandName.setText(relationNameObject.getBrand_Name());
            cusSelect.sizeName.setText(relationNameObject.getSize_Name());
            cusSelect.typeName.setText(relationNameObject.getType_Name());
            costPrice.setText(String.valueOf(relationNameObject.get_Cost_Price()));
            purchaseDate.setText(relationNameObject.get_Purchase_DateTime());
            productCode.setText(relationNameObject.get_Product_Code());
            detailID = relationNameObject.get_Detail_Id();

        }



        switchHandler();
        //credit_tableHandler();

        Calendar pCurrentDate = Calendar.getInstance();
        pYear = pCurrentDate.get(Calendar.YEAR);
        pMonth = pCurrentDate.get(Calendar.MONTH) + 1;
        pDay = pCurrentDate.get(Calendar.DAY_OF_MONTH);
        //soldDate.setText("" + pYear + "-" + pMonth + "-" + pDay);

        soldDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar sCurrentDate = Calendar.getInstance();
                int sYear = sCurrentDate.get(Calendar.YEAR);
                int sMonth = sCurrentDate.get(Calendar.MONTH);
                int sDay = sCurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(SalesCreditForm.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {

                        soldDate.setText("" + selectedyear + "-" + ((selectedmonth + 1) < 10 ? ("0" + (selectedmonth + 1)) : (selectedmonth + 1))
                                + "-" + (selectedday < 10 ? ("0" + selectedday) : selectedday));

                    }
                }, sYear, sMonth, sDay);
                mDatePicker.setTitle("Select date");
                mDatePicker.show();
            }
        });

        purchaseDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar sCurrentDate = Calendar.getInstance();
                int sYear = sCurrentDate.get(Calendar.YEAR);
                int sMonth = sCurrentDate.get(Calendar.MONTH);
                int sDay = sCurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(SalesCreditForm.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {

                        purchaseDate.setText("" + selectedyear + "-" + ((selectedmonth + 1) < 10 ? ("0" + (selectedmonth + 1)) : (selectedmonth + 1))
                                + "-" + (selectedday < 10 ? ("0" + selectedday) : selectedday));

                    }
                }, sYear, sMonth, sDay);
                mDatePicker.setTitle("Select date");
                mDatePicker.show();
            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(creditSwitch.isChecked()){
                    /**
                     * We need to check all the inputs for credit inputs are valid
                     */
                    if(firstName.getText().toString().trim().length()>0 && lastName.getText().toString().trim().length()>0
                            && phoneNumber.getText().toString().trim().length()>0){
                        Credits credits = new Credits();
                        credits.set_Firstname(firstName.getText().toString());
                        credits.set_Lastname(lastName.getText().toString());
                        credits.set_Phonenumber(phoneNumber.getText().toString());
                        long result = myCredit.creditInsert(credits);

                        if (result > -1) {

                            Toast.makeText(getApplicationContext(), "Saved to the credit table", Toast.LENGTH_SHORT).show();
                            firstName.setText("");
                            lastName.setText("");
                            phoneNumber.setText("");

                            System.out.println("creditors details:"+credits);

                        }
                        creditor_id = (int) result;
                    }
                }
                /**
                 * Here we need to check if all the inputs for details update are valid
                 * if the user changes the input of product and brand inputs then we need to ask the conformation where product code might need to change
                 * if product code is not changed along with product and brand
                 */

                if(save.getText().toString().equalsIgnoreCase("save")){
                    updateDetails();
                }else {

                    updateInventryDetails();
                }

            }
        });
    }

    public void getRelationID (PBST_Name relnName){
        for (Products product : varInit.productArray) {
            if (product.get_Product_Name().equals(relnName.getProduct_Name())) {
                cusSelect.productID = String.valueOf(product.get_Product_Id());
                System.out.println("IDRELATION product :"+ cusSelect.productID);

                break;
            }
        }

        for (Brands brand : varInit.brandArray) {
            if (brand.get_Brand_Name().equals(relnName.getBrand_Name())) {
                cusSelect.brandID = String.valueOf(brand.get_Brand_Id());

                System.out.println("IDRELATION brand :"+ cusSelect.brandID);

                break;
            }
        }

        for (Types type : varInit.typeArray) {
            if (type.get_Type_Name().equals(relnName.getType_Name())) {
                cusSelect.typeID = String.valueOf(type.get_Type_Id());
                System.out.println("IDRELATION type :"+ cusSelect.typeID);

                break;
            }
        }

        for (Sizes size : varInit.sizeArray) {
            if (size.get_Size_Name().equals(relnName.getSize_Name())) {
                cusSelect.sizeID = String.valueOf(size.get_Size_Id());

                System.out.println("IDRELATION size :"+ cusSelect.sizeID);

                break;
            }
        }
    }

    public void getRelationNames(Details detail){
        for (Products product : varInit.productArray) {
            if (product.get_Product_Id() == detail.get_Product_Id()) {
                cusSelect.productName.setText(product.get_Product_Name());
                break;
            }
        }

        for (Brands brand : varInit.brandArray) {
            if (brand.get_Brand_Id() == detail.get_Brand_Id()) {
                cusSelect.brandName.setText(brand.get_Brand_Name());
                break;
            }
        }

        for (Types type : varInit.typeArray) {
            if (type.get_Type_Id() == detail.get_Type_Id()) {
                cusSelect.typeName.setText(type.get_Type_Name());
                break;
            }
        }

        for (Sizes size : varInit.sizeArray) {
            if (size.get_Size_Id() == detail.get_Size_Id()) {
                cusSelect.sizeName.setText(size.get_Size_Name());
                break;
            }
        }
    }


    public void updateInventryDetails() {
        Details details;

        details = new Details();
        details.set_Detail_Id(detailID);
        details.set_Product_Id(Integer.parseInt(cusSelect.productID));
        details.set_Brand_Id(Integer.parseInt(cusSelect.brandID));
        details.set_Type_Id(Integer.parseInt(cusSelect.typeID));
        details.set_Size_Id(Integer.parseInt(cusSelect.sizeID));

        details.set_Cost_Price(costPrice.getText().toString());
        details.set_Purchase_DateTime(purchaseDate.getText().toString());
        details.set_Product_Code(productCode.getText().toString());

        isUpdated = myDetail.inventry_detail_update(details);

        if (isUpdated == true) {
            Toast.makeText(getApplicationContext(), "Updated to the database", Toast.LENGTH_SHORT).show();
            cusSelect.productName.setText("");
            cusSelect.brandName.setText("");
            cusSelect.sizeName.setText("");
            cusSelect.typeName.setText("");
            costPrice.setText("");
            purchaseDate.setText("");
            productCode.setText("");
            System.out.println("details:"+details);
        } else {
            Toast.makeText(getApplicationContext(), "Not updated to the database", Toast.LENGTH_SHORT).show();

        }
    }

    public void updateDetails() {
        if (detailID != 0) {
            Details details;
            details = new Details();
            profit = Integer.parseInt(soldPrice.getText().toString()) - Integer.parseInt(costPrice.getText().toString());

            details.set_Creditor_Id(creditor_id);
            details.set_Detail_Id(detailID);
            details.set_Product_Id(Integer.parseInt(cusSelect.productID));
            details.set_Brand_Id(Integer.parseInt(cusSelect.brandID));
            details.set_Type_Id(Integer.parseInt(cusSelect.typeID));
            details.set_Size_Id(Integer.parseInt(cusSelect.sizeID));

            details.set_Cost_Price(costPrice.getText().toString());
            details.set_Purchase_DateTime(purchaseDate.getText().toString());
            details.set_Sold_Price(soldPrice.getText().toString());
            details.set_Sold_DateTime(soldDate.getText().toString());
            details.set_Profit(String.valueOf(profit));
            details.set_Product_Code(productCode.getText().toString());


            isUpdated = myDetail.detail_update(details);

            if (isUpdated == true) {

                Toast.makeText(getApplicationContext(), "Saved to the database", Toast.LENGTH_SHORT).show();
                cusSelect.productName.setText("");
                cusSelect.brandName.setText("");
                cusSelect.sizeName.setText("");
                cusSelect.typeName.setText("");
                costPrice.setText("");
                purchaseDate.setText("");
                productCode.setText("");
                soldPrice.setText("");
                soldDate.setText("");
                System.out.println("details:" + details);

            } else {
                Toast.makeText(getApplicationContext(), "Not saved to the database", Toast.LENGTH_SHORT).show();

            }
        } else {

            Toast.makeText(getApplicationContext(), "Select and Insert all values", Toast.LENGTH_SHORT).show();
        }

    }

    public void switchHandler() {

        //set the switch to ON
        creditSwitch.setChecked(false);
        //attach a listener to check for changes in state
        creditSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if(isChecked) {
                    firstName.setVisibility(View.VISIBLE);
                    lastName.setVisibility(View.VISIBLE);
                    phoneNumber.setVisibility(View.VISIBLE);

                } else {

                    firstName.setVisibility(View.GONE);
                    lastName.setVisibility(View.GONE);
                    phoneNumber.setVisibility(View.GONE);
                }
            }
        });
        if (creditSwitch.isChecked()) {
            firstName.setVisibility(View.VISIBLE);
            lastName.setVisibility(View.VISIBLE);
            phoneNumber.setVisibility(View.VISIBLE);
        } else {
            firstName.setVisibility(View.GONE);
            lastName.setVisibility(View.GONE);
            phoneNumber.setVisibility(View.GONE);
        }
    }

    /**

    void credit_tableHandler(){
        enter.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                if(firstName.getText().toString().trim().length()>0){
                    Credits credits = new Credits();
                    credits.set_Firstname(firstName.getText().toString());
                    credits.set_Lastname(lastName.getText().toString());
                    credits.set_Phonenumber(phoneNumber.getText().toString());
                    long isinserted = myCredit.creditInsert(credits);
                    if(isinserted > -1){
                        Toast.makeText(getApplicationContext(), "Saved to the credit table", Toast.LENGTH_LONG).show();
                        System.out.println("creditors details:"+credits);
                    }
                }
            }
        });
    }  **/
    /**
     * void credit_tableHandler(){
     * enter.setOnClickListener(new View.OnClickListener(){
     *
     * @Override public void onClick(View view){
     * if(firstName.getText().toString().trim().length()>0){
     * Credits credits = new Credits();
     * credits.set_Firstname(firstName.getText().toString());
     * credits.set_Lastname(lastName.getText().toString());
     * credits.set_Phonenumber(phoneNumber.getText().toString());
     * long isinserted = myCredit.creditInsert(credits);
     * if(isinserted > -1){
     * Toast.makeText(getApplicationContext(), "Saved to the credit table", Toast.LENGTH_LONG).show();
     * System.out.println("creditors details:"+credits);
     * }
     * }
     * }
     * });
     * }
     **/



    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == android.view.KeyEvent.KEYCODE_BACK ) {

            if (intent.hasExtra("inventoryObject") && (!isUpdated)) {
                Intent myIntent = new Intent(SalesCreditForm.this, CustomGridDetailAdapter.class);
                startActivity(myIntent);
                finish();
                return true;
            } else if (intent.hasExtra("salesObject") && (!isUpdated)) {
                Intent myIntent = new Intent(SalesCreditForm.this, SalesEntry.class);
                startActivity(myIntent);
                finish();
                return true;
            } else if (intent.hasExtra("salesProductCodeObject") && (!isUpdated)) {
                Intent myIntent = new Intent(SalesCreditForm.this, SalesEntry.class);
                startActivity(myIntent);
                finish();
                return true;
            } else {
                Intent myIntent = new Intent(SalesCreditForm.this, MainActivity.class);
                startActivity(myIntent);

                if (intent.hasExtra("inventoryObject")){
                    CustomGridDetailAdapter.customAdaptorObject.finish();
                }
                else 
                SalesEntry.salesEntryObject.finish();
                finish();
                return true;

            }
        }
            return super.onKeyDown(keyCode, event);
        }


}

