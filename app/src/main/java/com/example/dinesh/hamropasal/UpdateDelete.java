package com.example.dinesh.hamropasal;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.example.dinesh.hamropasal.CRUD.DetailHelper;
import com.example.dinesh.hamropasal.Custom.CustomSelector;
import com.example.dinesh.hamropasal.Custom.VariableInitializer;
import com.example.dinesh.hamropasal.dbModel.Brands;
import com.example.dinesh.hamropasal.dbModel.Products;
import com.example.dinesh.hamropasal.dbModel.Sizes;
import com.example.dinesh.hamropasal.dbModel.Types;

public class UpdateDelete extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener{

    private Button updateInvenTime, updateInvenProduct,updateBookKeeping,updateRelation, update;
    private EditText productName, brandName, sizeName, typeName, name;

    private ListView productListView, brandListView, sizeListView, typeListView;

    private FrameLayout productListFrame, brandListFrame, sizeListFrame, typeListFrame;

    CustomSelector cusSelect;
    VariableInitializer varInit;
    DetailHelper myDetail;

    int index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_delete);
        super.onCreateDrawer();





        cusSelect = new CustomSelector(this);
        myDetail = new DetailHelper(this);

        updateInvenTime = (Button)findViewById(R.id.updateInvenTime);
        updateInvenProduct = (Button)findViewById(R.id.updateInvenProduct);
        updateBookKeeping = (Button)findViewById(R.id.updateBookKeeping);
        updateRelation = (Button)findViewById(R.id.updateRelation);

        update = (Button)findViewById(R.id.update);

        productName = (EditText)findViewById(R.id.productName);
        brandName = (EditText)findViewById(R.id.brandName);
        sizeName = (EditText)findViewById(R.id.sizeName);
        typeName = (EditText)findViewById(R.id.typeName);
        name = (EditText)findViewById(R.id.name);

        productListView = (ListView)findViewById(R.id.productListView);
        brandListView = (ListView)findViewById(R.id.brandListView);
        sizeListView = (ListView)findViewById(R.id.sizeListView);
        typeListView = (ListView)findViewById(R.id.typeListView);

        productListFrame = (FrameLayout)findViewById(R.id.productListFrame);

        typeListFrame = (FrameLayout)findViewById(R.id.typeListFrame);

        productListView.setAlpha(1);
        brandListView.setAlpha(1);
        sizeListView.setAlpha(1);
        typeListView.setAlpha(1);

        // hook up for edit texts and list views
        hookupForListviews(this);
        setLongClickForListView(this);
    }

    public void hookupForListviews(final Context context ){

        productName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus){

                    productListView.setVisibility(View.VISIBLE);
                    productListView.setAdapter(cusSelect.productAdapter);
                    productListView.bringToFront();
                    productListView.invalidate();
                }
                if(!hasFocus){
                    productListView.setVisibility(View.GONE);
                }
            }
        });
        AdapterView.OnItemClickListener productListener = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                index = position;
                Products proObject = (Products) cusSelect.productAdapter.getItem(position);
                //Toast.makeText(MainActivity.this,"product Name:"+ proObject.get_Product_Name() ,Toast.LENGTH_SHORT).show();
                productName.setText(proObject.get_Product_Name());
                cusSelect.productID = String.valueOf( proObject.get_Product_Id());
                Toast.makeText(context,"productID value"+ cusSelect.productID ,Toast.LENGTH_SHORT).show();
                System.out.println("The product is  --------"+ proObject);
                //for hiding the list view after selecting the item
                productListView.setVisibility(View.GONE);
                name.setHint("New Product Name");
                name.setVisibility(View.VISIBLE);
                update.setText("Update Product");
                update.setVisibility(View.VISIBLE);

            }
        };

        productListView.setOnItemClickListener(productListener);

        brandName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus){

                    brandListView.setVisibility(View.VISIBLE);
                    brandListView.setAdapter(cusSelect.brandAdapter);
                    brandListView.bringToFront();
                    brandListView.invalidate();
                }
                if(!hasFocus){
                    brandListView.setVisibility(View.GONE);
                }
            }
        });
        AdapterView.OnItemClickListener brandListener = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                index = position;
                Brands braObject = (Brands) cusSelect.brandAdapter.getItem(position);
                //Toast.makeText(MainActivity.this,"Brand Name:"+ braObject.get_Brand_Name() ,Toast.LENGTH_SHORT).show();
                brandName.setText(braObject.get_Brand_Name());
                cusSelect.brandID = String.valueOf( braObject.get_Brand_Id());
                Toast.makeText(context,"brandID value"+ cusSelect.brandID ,Toast.LENGTH_SHORT).show();

                //for hiding the list view after selecting the item
                brandListView.setVisibility(View.GONE);
                name.setHint("New Brand Name");
                name.setVisibility(View.VISIBLE);
                update.setText("Update Brand");
                update.setVisibility(View.VISIBLE);
            }
        };
        brandListView.setOnItemClickListener(brandListener);

        sizeName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus){

                    sizeListView.setVisibility(View.VISIBLE);
                    sizeListView.setAdapter(cusSelect.sizeAdapter);
                    sizeListView.bringToFront();
                    sizeListView.invalidate();

                }
                if(!hasFocus){
                    sizeListView.setVisibility(View.GONE);
                }
            }
        });
        AdapterView.OnItemClickListener sizeListener = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                index = position;
                Sizes sizObject = (Sizes) cusSelect.sizeAdapter.getItem(position);
                //Toast.makeText(MainActivity.this,"Size Name:"+ sizObject.get_Size_Name() ,Toast.LENGTH_SHORT).show();
                sizeName.setText(sizObject.get_Size_Name());
                cusSelect.sizeID = String.valueOf( sizObject.get_Size_Id());
                Toast.makeText(context,"sizeID value"+ cusSelect.sizeID ,Toast.LENGTH_SHORT).show();

                //for hiding the list view after selecting the item
                sizeListView.setVisibility(View.GONE);
                name.setHint("New Size Name");
                name.setVisibility(View.VISIBLE);
                update.setText("Update Size");
                update.setVisibility(View.VISIBLE);
            }
        };

        sizeListView.setOnItemClickListener(sizeListener);

        typeName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus){

                    typeListView.setVisibility(View.VISIBLE);
                    typeListView.setAdapter(cusSelect.typeAdapter);
                    typeListView.bringToFront();
                    typeListView.invalidate();
                }
                if(!hasFocus){
                    typeListView.setVisibility(View.GONE);
                }
            }
        });
        AdapterView.OnItemClickListener typeListener = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                index = position;
                Types typObject = (Types) cusSelect.typeAdapter.getItem(position);
                //Toast.makeText(MainActivity.this,"Type Name:"+ typObject.get_Type_Name() ,Toast.LENGTH_SHORT).show();
                typeName.setText(typObject.get_Type_Name());
                cusSelect.typeID = String.valueOf( typObject.get_Type_Id());
                Toast.makeText(context,"typeID value"+ cusSelect.typeID ,Toast.LENGTH_SHORT).show();

                //for hiding the list view after selecting the item
                typeListView.setVisibility(View.GONE);
                name.setHint("New Type Name");
                name.setVisibility(View.VISIBLE);
                update.setText("Update Type");
                update.setVisibility(View.VISIBLE);
            }
        };

        typeListView.setOnItemClickListener(typeListener);
    }
    public void setLongClickForListView(final Context context ){
        productListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {
                // TODO Auto-generated method stub
                Products product = varInit.productArray.get(pos);
                boolean ifDetailExists = myDetail.checkIfProductExists(product.get_Product_Id());
                if(ifDetailExists){
                    nameExistsDialog(product.get_Product_Name());
                    //System.out.println(" A product with product name already exists: " + product.get_Product_Name());
                }else {
                    deleteConformationProduct(product);

                }
                return true;
            }
        });
        brandListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {
                // TODO Auto-generated method stub
                Brands brand = varInit.brandArray.get(pos);
                boolean ifDetailExists = myDetail.checkIfBrandExists(brand.get_Brand_Id());
                if(ifDetailExists){
                    nameExistsDialog(brand.get_Brand_Name());
                    //System.out.println(" A product with product name already exists: " + brand.get_Brand_Name());
                }else {
                    deleteConformationBrand(brand);
                }
                return true;
            }
        });
        sizeListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {
                // TODO Auto-generated method stub
                Sizes size = varInit.sizeArray.get(pos);
                boolean ifDetailExists = myDetail.checkIfSizeExists(size.get_Size_Id());
                if(ifDetailExists){
                    nameExistsDialog(size.get_Size_Name());
                }else {
                    deleteConformationSize(size);
                }
                return true;
            }
        });
        typeListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {
                // TODO Auto-generated method stub
                Types type = varInit.typeArray.get(pos);
                boolean ifDetailExists = myDetail.checkIfTypeExists(type.get_Type_Id());
                if(ifDetailExists){
                    nameExistsDialog(type.get_Type_Name());
                }else {
                    deleteConformationType(type);
                }
                return true;
            }
        });

    }
    public void nameExistsDialog(String myString){
        //final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(myString +" can not be deleted");
        builder.setMessage("Entire product entry with same name "+myString+" exists. To delete the name "+myString+
                " Either 'Update product entry with different name or 'Delete the entry.'");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "It is for your data safety", Toast.LENGTH_SHORT).show();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
    public void proNamewithDifferentInitials(final Products pro, final int index){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Product entry with previous name "+productName.getText().toString() +" exists");
        builder.setMessage("Are you sure that you are going to update entire new name "+pro.get_Product_Name()+
                " you need change the product code of the entry in inventory and bookkeeping");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                long isUpdated = varInit.myProduct.productUpdate(pro);
                if(isUpdated > 0){
                    Products updateProduct = (Products) cusSelect.productAdapter.getItem(index);
                    cusSelect.productAdapter.remove(updateProduct);
                    varInit.productArray.remove(updateProduct);
                    varInit.productArray.add(index,pro);
                    //cusSelect.productAdapter.insert(product,index);
                    cusSelect.productAdapter.notifyDataSetChanged();
                    Toast.makeText(UpdateDelete.this,"Product Updated",Toast.LENGTH_SHORT).show();
                    productName.setText("");
                    name.setText("");
                }else{
                    Toast.makeText(UpdateDelete.this,"Product not Updated",Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Toast.makeText(getApplicationContext(),pro.get_Product_Name()+ " is not Updated", Toast.LENGTH_SHORT).show();

            }
        });
        AlertDialog alert = builder.create();
        alert.show();


    }
    public void braNamewithDifferentInitials(final Brands bra, final int index){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Product entry with previous name "+brandName.getText().toString() +" exists");
        builder.setMessage("Are you sure that you are going to update entire new name "+bra.get_Brand_Name()+
                " you need change the product code of the entry in inventory and bookkeeping");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                long isUpdated = varInit.myBrand.brandUpdate(bra);
                if(isUpdated > 0){
                    Brands updateBrand = (Brands) cusSelect.brandAdapter.getItem(index);
                    cusSelect.brandAdapter.remove(updateBrand);
                    varInit.brandArray.remove(updateBrand);
                    varInit.brandArray.add(index,bra);
                    //cusSelect.productAdapter.insert(product,index);
                    cusSelect.brandAdapter.notifyDataSetChanged();
                    Toast.makeText(UpdateDelete.this,"Brand Updated",Toast.LENGTH_SHORT).show();
                    brandName.setText("");
                    name.setText("");
                }else{
                    Toast.makeText(UpdateDelete.this,"Brand not Updated",Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Toast.makeText(getApplicationContext(),bra.get_Brand_Name()+ " is not Updated", Toast.LENGTH_SHORT).show();

            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }
    public void deleteConformationProduct(final Products proDelete){
        //final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(proDelete.get_Product_Name() +" name is going to be deleted");
        builder.setMessage("Are you sure that you are going to delete "+proDelete.get_Product_Name());

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                    int isDeleted = varInit.myProduct.productDelete(proDelete);
                    if (isDeleted > 0) {
                        cusSelect.productAdapter.remove(proDelete);
                        varInit.productArray.remove(proDelete);
                        cusSelect.productAdapter.notifyDataSetChanged();
                        Toast.makeText(getApplicationContext(), proDelete.get_Product_Name() + " is Deleted", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), proDelete.get_Product_Name() + " was not Deleted. Try Again", Toast.LENGTH_SHORT).show();
                    }
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Toast.makeText(getApplicationContext(),proDelete.get_Product_Name()+ " is not Deleted", Toast.LENGTH_SHORT).show();

            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }
    public void deleteConformationBrand (final Brands braDelete){
        //final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(braDelete.get_Brand_Name() +" name is going to be deleted");
        builder.setMessage("Are you sure that you are going to delete "+ braDelete.get_Brand_Name());

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                 int isDeleted = varInit.myBrand.brandDelete(braDelete);
                 if(isDeleted > 0){
                     varInit.brandArray.remove(braDelete);
                     cusSelect.brandAdapter.remove(braDelete);
                     cusSelect.brandAdapter.notifyDataSetChanged();
                     Toast.makeText(getApplicationContext(), braDelete.get_Brand_Name() + " is Deleted", Toast.LENGTH_SHORT).show();
                 }else {
                     Toast.makeText(getApplicationContext(), braDelete.get_Brand_Name() + " was not Deleted. Try Again", Toast.LENGTH_SHORT).show();
                 }
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Toast.makeText(getApplicationContext(),braDelete.get_Brand_Name()+ " is not Deleted", Toast.LENGTH_SHORT).show();

            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }
    public void deleteConformationSize (final Sizes sizDelete){
        //final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(sizDelete.get_Size_Name() +" name is going to be deleted");
        builder.setMessage("Are you sure that you are going to delete "+ sizDelete.get_Size_Name());

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                 int isDeleted = varInit.mySize.sizeDelete(sizDelete);
                 if(isDeleted > 0){
                     varInit.sizeArray.remove(sizDelete);
                     cusSelect.sizeAdapter.remove(sizDelete);
                     cusSelect.sizeAdapter.notifyDataSetChanged();
                     Toast.makeText(getApplicationContext(), sizDelete.get_Size_Name() + " is Deleted", Toast.LENGTH_SHORT).show();
                 }else{
                    Toast.makeText(getApplicationContext(), sizDelete.get_Size_Name() + " was not Deleted. Try Again", Toast.LENGTH_SHORT).show();
                 }
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Toast.makeText(getApplicationContext(),sizDelete.get_Size_Name()+ " is not Deleted", Toast.LENGTH_SHORT).show();

            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }
    public void deleteConformationType (final Types typDelete){
        //final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(typDelete.get_Type_Name() +" name is going to be deleted");
        builder.setMessage("Are you sure that you are going to delete "+ typDelete.get_Type_Name());

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                 int isDeleted = varInit.myType.typeDelete(typDelete);
                 if(isDeleted > 0){
                     varInit.typeArray.remove(typDelete);
                     cusSelect.typeAdapter.remove(typDelete);
                     cusSelect.typeAdapter.notifyDataSetChanged();
                     Toast.makeText(getApplicationContext(), typDelete.get_Type_Name() + " is Deleted", Toast.LENGTH_SHORT).show();
                 }else{
                    Toast.makeText(getApplicationContext(), typDelete.get_Type_Name() + " was not Deleted. Try Again", Toast.LENGTH_SHORT).show();
                 }
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Toast.makeText(getApplicationContext(),typDelete.get_Type_Name()+ " is not Deleted", Toast.LENGTH_SHORT).show();

            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    public void updateInventoryTimeClicked (View view){
        Intent intent = new Intent(UpdateDelete.this,SearchInventorybyTime.class);
        startActivity(intent);
        //finish useful for efficiency
        finish();
    }
    public void updateInventoryProduct (View view){
        Intent intent = new Intent(UpdateDelete.this,SearchInvenProduct.class);
        startActivity(intent);
        //finish useful for efficiency
        finish();

    }
    public void updateBookKeeping (View view){
        Intent intent = new Intent(UpdateDelete.this,BookKeeping.class);
        startActivity(intent);
        //finish useful for efficiency
        finish();

    }

    public void updateRelation (View view){
        final CharSequence[] lists = {"Product", "Brand", "Size", "Type"};
        //build the dialog

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select One Product Attribute");
        builder.setItems(lists, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Here we do the selection handling

                if(lists[which].equals("Product")){
                    productName.setVisibility(View.VISIBLE);
                    brandName.setVisibility(View.INVISIBLE);
                    sizeName.setVisibility(View.INVISIBLE);
                    typeName.setVisibility(View.INVISIBLE);
                    name.setVisibility(View.INVISIBLE);
                    update.setVisibility(View.INVISIBLE);

                }
                if(lists[which].equals("Brand")){
                    productName.setVisibility(View.INVISIBLE);
                    brandName.setVisibility(View.VISIBLE);
                    sizeName.setVisibility(View.INVISIBLE);
                    typeName.setVisibility(View.INVISIBLE);
                    name.setVisibility(View.INVISIBLE);
                    update.setVisibility(View.INVISIBLE);

                }
                if(lists[which].equals("Size")){
                    productName.setVisibility(View.INVISIBLE);
                    brandName.setVisibility(View.INVISIBLE);
                    sizeName.setVisibility(View.VISIBLE);
                    typeName.setVisibility(View.INVISIBLE);
                    name.setVisibility(View.INVISIBLE);
                    update.setVisibility(View.INVISIBLE);

                }
                if(lists[which].equals("Type")){
                    productName.setVisibility(View.INVISIBLE);
                    brandName.setVisibility(View.INVISIBLE);
                    sizeName.setVisibility(View.INVISIBLE);
                    typeName.setVisibility(View.VISIBLE);
                    name.setVisibility(View.INVISIBLE);
                    update.setVisibility(View.INVISIBLE);

                }

            }
        });
        AlertDialog alert = builder.create();
        alert.show();


    }

    public void updateClicked (View view){
        if(update.getText().equals("Update Product")){
            updateProduct();
        }
        if(update.getText().equals("Update Brand")){
            updateBrand();
        }
        if(update.getText().equals("Update Size")){
            updateSize();
        }
        if(update.getText().equals("Update Type")){
            updateType();
        }

    }
    public void updateProduct(){
        if((name.getText().toString().trim().length() > 0)){
            Products product = new Products();
            //Products product = new Products(productName.getText().toString());
            product.set_Product_Name(name.getText().toString());
            product.set_Product_Id(Integer.parseInt(cusSelect.productID));
            //Boolean isUpdated = myProduct.productUpdate(Integer.parseInt(detail_Id.getText().toString()),productName.getText().toString());
            boolean productExists = myDetail.checkIfProductExists(product.get_Product_Id());
            char previouName = productName.getText().toString().charAt(0);
            char newName = name.getText().toString().charAt(0);
            if(productExists && !(previouName == newName)){
                proNamewithDifferentInitials(product,index);
            }else {
                long isUpdated = varInit.myProduct.productUpdate(product);
                //int i = 0;
                if (isUpdated > 0) {
                    Products updateProduct = (Products) cusSelect.productAdapter.getItem(index);
                    cusSelect.productAdapter.remove(updateProduct);
                    varInit.productArray.remove(updateProduct);
                    varInit.productArray.add(index, product);
                    //cusSelect.productAdapter.insert(product,index);
                    cusSelect.productAdapter.notifyDataSetChanged();
                    Toast.makeText(UpdateDelete.this, "Product Updated", Toast.LENGTH_SHORT).show();
                    productName.setText("");
                    name.setText("");
                } else {
                    Toast.makeText(UpdateDelete.this, "Product not Updated", Toast.LENGTH_SHORT).show();
                }
            }
        }else{
            Toast.makeText(UpdateDelete.this,"Type product name ",Toast.LENGTH_SHORT).show();
        }

    }
    public void updateBrand(){
        if((name.getText().toString().trim().length() > 0)){
            Brands brand = new Brands();
            brand.set_Brand_Name(name.getText().toString());
            brand.set_Brand_Id(Integer.parseInt(cusSelect.brandID));
            boolean productExists = myDetail.checkIfBrandExists(brand.get_Brand_Id());
            char previouName = brandName.getText().toString().charAt(0);
            char newName = name.getText().toString().charAt(0);
            if(productExists && !(previouName == newName)){
                braNamewithDifferentInitials(brand,index);
            }else {
                long isUpdated = varInit.myBrand.brandUpdate(brand);
                if (isUpdated > 0) {
                    Brands updateBrand = (Brands) cusSelect.brandAdapter.getItem(index);
                    cusSelect.brandAdapter.remove(updateBrand);
                    varInit.brandArray.remove(updateBrand);
                    varInit.brandArray.add(index, brand);
                    //cusSelect.productAdapter.insert(product,index);
                    cusSelect.brandAdapter.notifyDataSetChanged();
                    Toast.makeText(UpdateDelete.this, "Brand Updated", Toast.LENGTH_SHORT).show();
                    brandName.setText("");
                    name.setText("");
                } else {
                    Toast.makeText(UpdateDelete.this, "Brand not Updated", Toast.LENGTH_SHORT).show();
                }
            }
        }else{
            Toast.makeText(UpdateDelete.this,"Type brand name ",Toast.LENGTH_SHORT).show();
        }

    }
    public void updateSize(){
        if((name.getText().toString().trim().length() > 0)){
            Sizes size = new Sizes();
            size.set_Size_Name(name.getText().toString());
            size.set_Size_Id(Integer.parseInt(cusSelect.brandID));
            long isUpdated = varInit.mySize.sizeUpdate(size);
            if(isUpdated >0){
                Sizes updateSize = (Sizes) cusSelect.sizeAdapter.getItem(index);
                cusSelect.sizeAdapter.remove(updateSize);
                varInit.sizeArray.remove(updateSize);
                varInit.sizeArray.add(index,size);
                //cusSelect.productAdapter.insert(product,index);
                cusSelect.sizeAdapter.notifyDataSetChanged();
                Toast.makeText(UpdateDelete.this,"Size Updated",Toast.LENGTH_SHORT).show();
                sizeName.setText("");
                name.setText("");
            }else{
                Toast.makeText(UpdateDelete.this,"Size not Updated",Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(UpdateDelete.this,"Type size name ",Toast.LENGTH_SHORT).show();
        }

    }
    public void updateType(){
        if((name.getText().toString().trim().length() > 0)){
            Types type = new Types();
            type.set_Type_Name(name.getText().toString());
            type.set_Type_Id(Integer.parseInt(cusSelect.brandID));
            long isUpdated = varInit.myType.typeUpdate(type);
            if(isUpdated >0){
                Types updateType = (Types) cusSelect.typeAdapter.getItem(index);
                cusSelect.typeAdapter.remove(updateType);
                varInit.typeArray.remove(updateType);
                varInit.typeArray.add(index,type);
                //cusSelect.productAdapter.insert(product,index);
                cusSelect.typeAdapter.notifyDataSetChanged();
                Toast.makeText(UpdateDelete.this,"Type Updated",Toast.LENGTH_SHORT).show();
                typeName.setText("");
                name.setText("");
            }else{
                Toast.makeText(UpdateDelete.this,"Type not Updated",Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(UpdateDelete.this,"Type type name ",Toast.LENGTH_SHORT).show();
        }

    }



}
