package com.example.dinesh.hamropasal;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.dinesh.hamropasal.CRUD.BrandHelper;
import com.example.dinesh.hamropasal.CRUD.CreditHelper;
import com.example.dinesh.hamropasal.CRUD.DBHelper;
import com.example.dinesh.hamropasal.CRUD.DetailHelper;
import com.example.dinesh.hamropasal.CRUD.ProductHelper;
import com.example.dinesh.hamropasal.CRUD.SizeHelper;
import com.example.dinesh.hamropasal.CRUD.TypeHelper;
import com.example.dinesh.hamropasal.Custom.CreditViewAdapter;
import com.example.dinesh.hamropasal.Custom.CreditViewDetailAdapter;
import com.example.dinesh.hamropasal.dbModel.Brands;
import com.example.dinesh.hamropasal.dbModel.Credits;
import com.example.dinesh.hamropasal.dbModel.Details;
import com.example.dinesh.hamropasal.dbModel.PBST_Name;
import com.example.dinesh.hamropasal.dbModel.Products;
import com.example.dinesh.hamropasal.dbModel.Sizes;
import com.example.dinesh.hamropasal.dbModel.Types;

import java.util.ArrayList;

/**
 * Created by Niroj on 9/3/2016.
 */
public class CreditView extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private DBHelper myDB;
    private DetailHelper myDetail;
    private CreditHelper myCredit;
    private ProductHelper myProduct;
    private BrandHelper myBrand;
    private TypeHelper myType;
    private SizeHelper mySize;

    private Credits creditObject;

    private GridView myGridCredit,myGridDetail;

    private LinearLayout gridHeader;
    private LinearLayout crDetailGridHeader;

    private int creditor_Id;
    private int creditor_Id_Detail;

    private ArrayList<Credits> creditArray;
    private ArrayList<Details> creditorsDetailArray;
    private ArrayList<Products> productArray;
    private ArrayList<Brands> brandArray;
    private ArrayList<Sizes> sizeArray;
    private ArrayList<Types> typeArray;
    private ArrayList<PBST_Name>detailArrayDisplay;

    CreditViewDetailAdapter creViewDetailAdapter;
    CreditViewAdapter creViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.credit_layout);
        super.onCreateDrawer();


        myDB = new DBHelper(this);
        myCredit = new CreditHelper(this);
        myDetail = new DetailHelper(this);
        myProduct = new ProductHelper(this);
        myBrand = new BrandHelper(this);
        myType = new TypeHelper(this);
        mySize = new SizeHelper(this);

        myGridCredit =(GridView)findViewById(R.id.gridLayoutCredit);
        myGridDetail =(GridView)findViewById(R.id.gridlayoutDetail);

        gridHeader = (LinearLayout)findViewById(R.id.gridHeader);
        crDetailGridHeader = (LinearLayout)findViewById(R.id.crDetailGridHeader);

        crDetailGridHeader.setVisibility(View.INVISIBLE);

        creditArray = myCredit.getAllCredits();
        productArray = myProduct.getAllProducts();
        brandArray = myBrand.getAllBrands();
        sizeArray = mySize.getAllSizes();
        typeArray = myType.getAllTypes();

        creViewAdapter = new CreditViewAdapter(CreditView.this,R.layout.credit_view_adapter,creditArray);
        myGridCredit.setAdapter(creViewAdapter);
        creViewAdapter.notifyDataSetChanged();
        //System.out.println("creditArray"+ creditArray);

        myGridCredit.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?>parent, View view, int position, long id){
                crDetailGridHeader.setVisibility(View.VISIBLE);
                PBST_Name pbst_name = new PBST_Name();
                creditObject = (Credits)creViewAdapter.getItem(position);
                creditor_Id = creditObject.get_Creditor_ID();
                creditorsDetailArray = myDetail.getCreditDetails(creditor_Id);

                ArrayList<PBST_Name> detailArrayDisplay = new ArrayList<PBST_Name>();

                for (int i = 0; i < creditorsDetailArray.size(); i++) {
                    for (Details detail:creditorsDetailArray){

                        for (Products product:productArray){
                            if(product.get_Product_Id() == detail.get_Product_Id()){
                                pbst_name.setProduct_Name(product.get_Product_Name());
                            }
                        }

                        for (Brands brand:brandArray){
                            if(brand.get_Brand_Id() == detail.get_Brand_Id()){
                                pbst_name.setBrand_Name(brand.get_Brand_Name());
                            }
                        }

                        for(Types type: typeArray){
                            if(type.get_Type_Id() == detail.get_Type_Id()){
                                pbst_name.setType_Name(type.get_Type_Name());
                            }
                        }

                        for (Sizes size: sizeArray){
                            if(size.get_Size_Id() == detail.get_Size_Id()){
                                pbst_name.setSize_Name(size.get_Size_Name());
                            }
                        }

                        pbst_name.set_Sold_Price(detail.get_Sold_Price());
                        pbst_name.set_Sold_Date(detail.get_Sold_DateTime());
                        pbst_name.set_Detail_Id(detail.get_Detail_Id());

                    }
                    detailArrayDisplay.add(pbst_name);
                }

                creViewDetailAdapter = new CreditViewDetailAdapter(CreditView.this,R.layout.credit_view_detail_adapter,detailArrayDisplay);
                myGridDetail.setAdapter(creViewDetailAdapter);
                creViewDetailAdapter.notifyDataSetChanged();
                //System.out.println("Details array"+ detailArray.toString());

            }
        });

        myGridDetail.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){
            @Override
            public boolean onItemLongClick(AdapterView<?>arg0, View arg1, int position,long id) {
                PBST_Name pbstObject = (PBST_Name)creViewDetailAdapter.getItem(position);
                creditor_Id = pbstObject.get_Creditor_Id();
                getDetailsFromPBST(pbstObject);
                return true;

            }

        });
    }
    public void getDetailsFromPBST(PBST_Name pbstObject){
        int detail_Id = pbstObject.get_Detail_Id();
        for (Details detail:creditorsDetailArray){
            if(detail.get_Detail_Id() == pbstObject.get_Detail_Id())
                detail.set_Creditor_Id(0);
            detailUpdateConfirmation(detail,pbstObject);
        }
    }

    void detailUpdateConfirmation(final Details detail, final PBST_Name pbstObject){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Are you sure you want to delete the credit information of " +pbstObject.getProduct_Name());
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if(creditorsDetailArray.size() == 1){
                    myCredit.creditDelete(creditObject);
                }

                Toast.makeText(getApplicationContext(), "Database Updated", Toast.LENGTH_LONG).show();
                myDetail.detail_update(detail);
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Toast.makeText(getApplicationContext(),"Database is not Updated", Toast.LENGTH_SHORT).show();

            }
        });
        AlertDialog alert = builder.create();
        alert.show();


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == android.view.KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            Intent myIntent = new Intent(CreditView.this, MainActivity.class);
            startActivity(myIntent);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}
