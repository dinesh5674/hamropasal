package com.example.dinesh.hamropasal.CRUD;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.dinesh.hamropasal.dbModel.Types;

import java.util.ArrayList;

/**
 * Created by ANONYMOUS_PAL on 7.7.2016.
 */
public class TypeHelper extends DBHelper {

    public TypeHelper(Context context) {
        super(context);
    }

    //@Override
    public long typeInsert(Types type) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Type_Name,type.get_Type_Name());
        contentValues.put(Quantity,type.get_Quantity());
        long result = db.insert(Type_Table,null,contentValues);
        //this should be before return statement
        db.close();
        return result;

    }

    //@Override
    public long typeUpdate(Types type) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Type_ID,type.get_Type_Id());
        contentValues.put(Type_Name,type.get_Type_Name());
        contentValues.put(Quantity,type.get_Quantity());
        int result = db.update(Type_Table,contentValues, "type_Id = ?", new String [] { String.valueOf(type.get_Type_Id())});
        db.close();
        return result;
    }

    public int typeDelete(Types type) {
        SQLiteDatabase db = this.getWritableDatabase();
        int result=  db.delete(Type_Table,"type_Id = ?", new String [] {String.valueOf(type.get_Type_Id())});
        db.close();
        return result;
    }

    public ArrayList<Types> getAllTypes() {
        ArrayList<Types> arrayList = new ArrayList<Types>();
        SQLiteDatabase db = this.getReadableDatabase();
        Types typ;
        Cursor cur = db.rawQuery("select * from "+Type_Table, null);
        cur.moveToFirst();

        while (cur.isAfterLast() == false) {
            typ= new Types();
            typ.set_Type_Id(Integer.parseInt(cur.getString(0)));
            typ.set_Type_Name(cur.getString(1));
            typ.set_Quantity(Integer.parseInt(cur.getString(2)));
            arrayList.add(typ);
            cur.moveToNext();
        }
        cur.close();
        db.close();
        return arrayList;

    }
}
