package com.example.dinesh.hamropasal;

import android.content.Intent;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.dinesh.hamropasal.CRUD.DetailHelper;
import com.example.dinesh.hamropasal.Custom.CustomDetailGrid;
import com.example.dinesh.hamropasal.Custom.CustomSelector;
import com.example.dinesh.hamropasal.Custom.ProductCodeAdapter;
import com.example.dinesh.hamropasal.Custom.VariableInitializer;
import com.example.dinesh.hamropasal.dbModel.Brands;
import com.example.dinesh.hamropasal.dbModel.Details;
import com.example.dinesh.hamropasal.dbModel.PBST_Name;
import com.example.dinesh.hamropasal.dbModel.Products;
import com.example.dinesh.hamropasal.dbModel.Sizes;
import com.example.dinesh.hamropasal.dbModel.Types;

import java.util.ArrayList;

/**
 * Created by subash on 23/08/2016.
 */
//extends AppCompatActivityimplements NavigationView.OnNavigationItemSelectedListener
public class SalesEntry extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    GridView relationGrid;

    Button proCodeBtn,relationBtn;
    EditText productCode;
    public static SalesEntry salesEntryObject;
    private FrameLayout customSelector;
    private FrameLayout salesFrameSearch;
    private FrameLayout proCodeFrame;
    private LinearLayout gridHeader;
    private LinearLayout proCodeGridHeader;

    DetailHelper myDetail;


    CustomSelector cusSelect;
    VariableInitializer varInit;

    private ArrayList<Details> detailArray;

    ArrayList<Details> filteredObjects;
    ArrayList<PBST_Name> productCodeObject;
    CustomDetailGrid customFilterAdapter;
    ProductCodeAdapter proCodeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sales_entry);
        salesEntryObject= this;
        super.onCreateDrawer();

        cusSelect = new CustomSelector(this);

        myDetail = new DetailHelper(this);

        customSelector = (FrameLayout)findViewById(R.id.customSelector);
        salesFrameSearch = (FrameLayout)findViewById(R.id.salesFrameSearch);
        proCodeFrame = (FrameLayout)findViewById(R.id.proCodeFrame);
        productCode = (EditText)findViewById(R.id.productCode);

        gridHeader = (LinearLayout)findViewById(R.id.gridHeader);

        gridHeader.setVisibility(View.INVISIBLE);

        proCodeGridHeader = (LinearLayout)findViewById(R.id.proCodeGridHeader);
        proCodeGridHeader.setVisibility(View.INVISIBLE);

        customSelector.addView(cusSelect);
        //get the view of list view in front
        customSelector.bringToFront();
        customSelector.invalidate();

        proCodeFrame.setVisibility(View.GONE);

        proCodeBtn = (Button)findViewById(R.id.proCodeBtn);
        relationBtn = (Button)findViewById(R.id.relationBtn);
        relationGrid = (GridView)findViewById(R.id.relationGrid);

        filteredObjects = new ArrayList<Details>();
        productCodeObject = new ArrayList<PBST_Name>();

        detailArray = myDetail.getInventoryDetails();

        customFilterAdapter = new CustomDetailGrid(SalesEntry.this, R.layout.custom_grid, filteredObjects);

        proCodeAdapter = new ProductCodeAdapter(SalesEntry.this, R.layout.product_code_adapter,productCodeObject);


        relationGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                if(gridHeader.getVisibility() == View.VISIBLE){
                    Details detailObject = (Details) customFilterAdapter.getItem(position);
                    System.out.println("Detail Object :"+ detailObject);
                    Intent intent = new Intent(SalesEntry.this,SalesCreditForm.class);
                    intent.putExtra("salesObject", detailObject);
                    startActivity(intent);
                  // not calling finish just to keep data in adapterview as it is incase the user gets back from updating activity, This will keep the activity in background will not affect in the performance of teh app
                    System.out.println("Grid View is clicked");
                }else{
                    PBST_Name relationNameObject = (PBST_Name) proCodeAdapter.getItem(position);
                    Intent intent = new Intent(SalesEntry.this,SalesCreditForm.class);
                    intent.putExtra("salesProductCodeObject", relationNameObject);
                    startActivity(intent);
                    //finish useful for efficiency
                    System.out.println("PBST Object :"+ relationNameObject);
                }
            }
        });

    }
    public void searchWithProductCodeClicked(View view){
        proCodeBtn.getBackground().setColorFilter(new LightingColorFilter(0x00FFFF , 0x00FFFF));
        relationBtn.getBackground().setColorFilter(new LightingColorFilter(0x004F81, 0x004F81));
        proCodeFrame.setVisibility(View.VISIBLE);
        customSelector.setVisibility(View.GONE);

    }
    public void searchWithRelationClicked(View view){
        relationBtn.getBackground().setColorFilter(new LightingColorFilter(0x00FFFF , 0x00FFFF));
        proCodeBtn.getBackground().setColorFilter(new LightingColorFilter(0x004F81, 0x0004F81));

        customSelector.setVisibility(View.VISIBLE);
        proCodeFrame.setVisibility(View.GONE);
    }
    public void getObjectsWithFilter(){
        if(filteredObjects.isEmpty()){

        }else {
            filteredObjects.clear();
        }
        for (Details detail:detailArray) {
            if(detail.get_Product_Id()==Integer.parseInt(cusSelect.productID) && detail.get_Brand_Id()==Integer.parseInt(cusSelect.brandID)
                        && (detail.get_Size_Id() == Integer.parseInt(cusSelect.sizeID)) && (detail.get_Type_Id() == Integer.parseInt(cusSelect.typeID))) {
                    filteredObjects.add(detail);
                }
        }
    }
    public void getProCodeObject(){
        if(productCodeObject.isEmpty()){
        }else {
            productCodeObject.clear();
        }
        for (Details detail:detailArray) {
            if ((detail.get_Product_Code()).equalsIgnoreCase(productCode.getText().toString())) {
                //System.out.println("proCodeVal :" + productCode.getText().toString());
                PBST_Name relnObject = new PBST_Name();
                for (Products product : varInit.productArray) {
                    if (product.get_Product_Id() == detail.get_Product_Id()) {
                        relnObject.setProduct_Name(product.get_Product_Name());
                        break;
                    }
                }

                for (Brands brand : varInit.brandArray) {
                    if (brand.get_Brand_Id() == detail.get_Brand_Id()) {
                        relnObject.setBrand_Name(brand.get_Brand_Name());
                        break;
                    }
                }

                for (Types type : varInit.typeArray) {
                    if (type.get_Type_Id() == detail.get_Type_Id()) {
                        relnObject.setType_Name(type.get_Type_Name());
                        break;
                    }
                }

                for (Sizes size : varInit.sizeArray) {
                    if (size.get_Size_Id() == detail.get_Size_Id()) {
                        relnObject.setSize_Name(size.get_Size_Name());
                        break;
                    }
                }
                relnObject.set_Product_Code(productCode.getText().toString());
                relnObject.set_Cost_Price(detail.get_Cost_Price());
                relnObject.set_Purchase_DateTime(detail.get_Purchase_DateTime());
                relnObject.set_Detail_Id(detail.get_Detail_Id());
                productCodeObject.add(relnObject);
            }
        }
    }

    public void searchClicked (View view){

        if (customSelector.getVisibility() == (View.VISIBLE)) {
            if (cusSelect.productName.getText().toString().trim().length() == 0 ||  cusSelect.brandName.getText().toString().trim().length() == 0
                    ||  cusSelect.sizeName.getText().toString().trim().length() == 0 ||  cusSelect.typeName.getText().toString().trim().length() == 0) {
                Toast.makeText(getApplicationContext(), "please enter all values first", Toast.LENGTH_SHORT).show();
            }
            else {
                proCodeGridHeader.setVisibility(View.INVISIBLE);
                gridHeader.setVisibility(View.VISIBLE);
                getObjectsWithFilter();

                relationGrid.setAdapter(customFilterAdapter);
            }
        }
        if(proCodeFrame.getVisibility() == (View.VISIBLE)){
            if (productCode.getText().toString().trim().length() == 0) {
                Toast.makeText(getApplicationContext(), "please enter the value first", Toast.LENGTH_SHORT).show();
            } else {
                gridHeader.setVisibility(View.INVISIBLE);
                proCodeGridHeader.setVisibility(View.VISIBLE);
                getProCodeObject();

                relationGrid.setAdapter(proCodeAdapter);
            }
        }
    }




    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            Intent myIntent = new Intent(SalesEntry.this, MainActivity.class);
            startActivity(myIntent);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}