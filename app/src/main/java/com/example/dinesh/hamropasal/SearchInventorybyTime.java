package com.example.dinesh.hamropasal;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dinesh.hamropasal.CRUD.BrandHelper;
import com.example.dinesh.hamropasal.CRUD.DetailHelper;
import com.example.dinesh.hamropasal.CRUD.ProductHelper;
import com.example.dinesh.hamropasal.CRUD.SizeHelper;
import com.example.dinesh.hamropasal.CRUD.TypeHelper;
import com.example.dinesh.hamropasal.Custom.GridAdapter;
import com.example.dinesh.hamropasal.dbModel.Brands;
import com.example.dinesh.hamropasal.dbModel.Details;
import com.example.dinesh.hamropasal.dbModel.PBST_Name;
import com.example.dinesh.hamropasal.dbModel.Products;
import com.example.dinesh.hamropasal.dbModel.Sizes;
import com.example.dinesh.hamropasal.dbModel.Types;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Niroj on 7/24/2016.
 */
public class SearchInventorybyTime  extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ProductHelper myProduct;
    private BrandHelper myBrand;
    private TypeHelper myType;
    private SizeHelper mySize;
    private DetailHelper myDetail;
    String toDateString_bet,fromDateString_bet,dateString_bef,dateString_bef_comparision,toDateString_bet_comparision;
    EditText fromDate,toDate;
    Button betTime,befTime,searchButton;
    TextView from,to;
    int pYear, pMonth, pDay;
    GridAdapter arrayAdapterD;


    private ArrayList<Products> productArray;
    private ArrayList<Brands> brandArray;
    private ArrayList<Sizes> sizeArray;
    private ArrayList<Types> typeArray;
    private ArrayList<Details> detailArray;

    Calendar pCurrentDate;

    private ArrayList<Details> selectedDetailsItem;
   // String startDate,endDate,beforeTime;
    Date dateFrom,dateTo;
    int i=0;
    final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    GridView myGrid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_inv_time);
        super.onCreateDrawer();


        fromDate = (EditText) findViewById(R.id.fromDate);
        toDate = (EditText) findViewById(R.id.toDate);
        searchButton = (Button) findViewById(R.id.searchButton);
        betTime = (Button) findViewById(R.id.bet_time);
        befTime = (Button) findViewById(R.id.bef_time);
        from = (TextView) findViewById(R.id.textView2);
        to = (TextView) findViewById(R.id.textView5);


        fromDate.setFocusable(false);
        fromDate.setClickable(false);
        fromDate.setFocusableInTouchMode(false);
        toDate.setFocusable(false);
        toDate.setClickable(false);
        toDate.setFocusableInTouchMode(false);

        myGrid = (GridView) findViewById(R.id.gridView);

        myProduct = new ProductHelper(this);
        myBrand = new BrandHelper(this);
        myType = new TypeHelper(this);
        mySize = new SizeHelper(this);
        myDetail = new DetailHelper(this);
        selectedDetailsItem = new ArrayList<>();




        productArray = myProduct.getAllProducts();
        brandArray = myBrand.getAllBrands();
        sizeArray = mySize.getAllSizes();
        typeArray = myType.getAllTypes();

        toDate.setText("");
        fromDate.setText("");
        detailArray = myDetail.getInventoryDetails();

        inventoryFetch();



        betTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                betTime.getBackground().setColorFilter(new LightingColorFilter(0x00FFFF , 0x00FFFF));//after press color
                befTime.getBackground().setColorFilter(new LightingColorFilter(0x004F81, 0x004F81));

                fromDate.setFocusable(true);
                toDate.setFocusable(true);
                toDate.setVisibility(View.VISIBLE);
                fromDate.setVisibility(View.VISIBLE);
                searchButton.setVisibility(View.VISIBLE);
                fromDate.setVisibility(View.VISIBLE);
                from.setVisibility(View.VISIBLE);
                to.setText("To");

                pCurrentDate = Calendar.getInstance();
                pYear = pCurrentDate.get(Calendar.YEAR);
                pMonth = pCurrentDate.get(Calendar.MONTH);
                pDay = pCurrentDate.get(Calendar.DAY_OF_MONTH);
                fromDate.setText(pYear + "-" +((pCurrentDate.get(Calendar.MONTH)+1)<10?("0"+(pCurrentDate.get(Calendar.MONTH)+1)):(pCurrentDate.get(Calendar.MONTH)+1)) + "-" + (pDay<10?("0"+pDay):pDay));
                fromDateString_bet=pYear + "-" + ((pCurrentDate.get(Calendar.MONTH)+1)<10?("0"+(pCurrentDate.get(Calendar.MONTH)+1)):(pCurrentDate.get(Calendar.MONTH)+1)) + "-" + ((pCurrentDate.get(Calendar.DAY_OF_MONTH)-1)<10?("0"+(pCurrentDate.get(Calendar.DAY_OF_MONTH)-1)):(pCurrentDate.get(Calendar.DAY_OF_MONTH)-1));
                toDate.setText(pYear + "-" +((pCurrentDate.get(Calendar.MONTH)+1)<10?("0"+(pCurrentDate.get(Calendar.MONTH)+1)):(pCurrentDate.get(Calendar.MONTH)+1)) + "-" + (pDay<10?("0"+pDay):pDay));
                toDateString_bet=pYear + "-" + ((pCurrentDate.get(Calendar.MONTH)+1)<10?("0"+(pCurrentDate.get(Calendar.MONTH)+1)):(pCurrentDate.get(Calendar.MONTH)+1)) + "-" +  ((pCurrentDate.get(Calendar.DAY_OF_MONTH)+1)<10?("0"+(pCurrentDate.get(Calendar.DAY_OF_MONTH)+1)):(pCurrentDate.get(Calendar.DAY_OF_MONTH)+1));
                fromDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DatePickerDialog mDatePicker = new DatePickerDialog(SearchInventorybyTime.this, new DatePickerDialog.OnDateSetListener() {
                            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                                fromDate.setText(selectedyear + "-" + ((selectedmonth+1)<10?("0"+(selectedmonth+1)):(selectedmonth+1))
                                        + "-" + (selectedday<10?("0"+selectedday):selectedday));
                                fromDateString_bet=selectedyear + "-" +((selectedmonth+1)<10?("0"+(selectedmonth+1)):(selectedmonth+1))
                                        + "-" + (((selectedday-1)<10)?("0"+(selectedday-1)):(selectedday-1));
                            }
                        }, pYear, pMonth, pDay);
                        mDatePicker.show();
                    }
                });


                toDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DatePickerDialog mDatePicker = new DatePickerDialog(SearchInventorybyTime.this, new DatePickerDialog.OnDateSetListener() {
                            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                                toDate.setText(selectedyear + "-" + ((selectedmonth+1)<10?("0"+(selectedmonth+1)):(selectedmonth+1))
                                        + "-" + (selectedday<10?("0"+selectedday):selectedday));
                                toDateString_bet=selectedyear + "-" + ((selectedmonth+1)<10?("0"+(selectedmonth+1)):(selectedmonth+1))
                                        + "-" + (((selectedday+1)<10)?("0"+(selectedday+1)):(selectedday+1));

                            }
                        }, pYear, pMonth, pDay);
                        mDatePicker.setTitle("Select date");
                        mDatePicker.show();
                    }
                });

                getInvenBetTime();

                    arrayAdapterD.clear();
                    arrayAdapterD.notifyDataSetChanged();

            }
        });
        befTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                befTime.getBackground().setColorFilter(new LightingColorFilter(0x00FFFF , 0x00FFFF));
                betTime.getBackground().setColorFilter(new LightingColorFilter(0x004F81, 0x004F81));

                to.setText("Before");
                from.setVisibility(View.INVISIBLE);
                fromDate.setVisibility(View.INVISIBLE);

                to.setVisibility(View.VISIBLE);
                toDate.setVisibility(View.VISIBLE);
                searchButton.setVisibility(View.VISIBLE);
                pCurrentDate = Calendar.getInstance();
                pYear = pCurrentDate.get(Calendar.YEAR);
                pMonth = pCurrentDate.get(Calendar.MONTH);
                pDay = pCurrentDate.get(Calendar.DAY_OF_MONTH);
                toDate.setText(pYear + "-" + ((pCurrentDate.get(Calendar.MONTH)+1)<10?("0"+(pCurrentDate.get(Calendar.MONTH)+1)):(pCurrentDate.get(Calendar.MONTH)+1)) + "-" + (pDay<10?("0"+pDay):pDay));
                dateString_bef=pYear + "-" + ((pCurrentDate.get(Calendar.MONTH)+1)<10?("0"+(pCurrentDate.get(Calendar.MONTH)+1)):(pCurrentDate.get(Calendar.MONTH)+1)) + "-" +  ((pCurrentDate.get(Calendar.DAY_OF_MONTH)+1)<10?("0"+(pCurrentDate.get(Calendar.DAY_OF_MONTH)+1)):(pCurrentDate.get(Calendar.DAY_OF_MONTH)+1));


                toDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        DatePickerDialog mDatePicker = new DatePickerDialog(SearchInventorybyTime.this, new DatePickerDialog.OnDateSetListener() {
                            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                                toDate.setText(selectedyear + "-" + ((selectedmonth+1)<10?("0"+(selectedmonth+1)):(selectedmonth+1))
                                        + "-" + (selectedday<10?("0"+selectedday):selectedday));
                                dateString_bef=selectedyear + "-" + ((selectedmonth+1)<10?("0"+(selectedmonth+1)):(selectedmonth+1))
                                        + "-" + (((selectedday+1)<10)?("0"+(selectedday+1)):(selectedday+1));
                            }
                        }, pYear, pMonth, pDay);
                        mDatePicker.setTitle("Select date");
                        mDatePicker.show();
                    }
                });
                getInvenBefTime();

                    arrayAdapterD.clear();
                    arrayAdapterD.notifyDataSetChanged();

            }


        });


    }

    private void getInvenBefTime() {

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateString_bef_comparision=pYear + "-" + ((pCurrentDate.get(Calendar.MONTH)+1)<10?("0"+(pCurrentDate.get(Calendar.MONTH)+1)):(pCurrentDate.get(Calendar.MONTH)+1)) + "-" +  ((pCurrentDate.get(Calendar.DAY_OF_MONTH)+1)<10?("0"+(pCurrentDate.get(Calendar.DAY_OF_MONTH)+1)):(pCurrentDate.get(Calendar.DAY_OF_MONTH)+1));
                     if (toDate.toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "please enter desired month value", Toast.LENGTH_SHORT).show();
                     }
                else {
                    try {
                        dateTo = formatter.parse(dateString_bef);
                        System.out.println("2Digit Date:" + dateTo);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    try {
                        if (dateTo.after(formatter.parse(dateString_bef_comparision))){
                            Toast.makeText(getApplicationContext(), "please choose valid date", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            inventoryFetch();
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }                }
           }

        });
    }

    private void  getInvenBetTime() {

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toDateString_bet_comparision=pYear + "-" + ((pCurrentDate.get(Calendar.MONTH)+1)<10?("0"+(pCurrentDate.get(Calendar.MONTH)+1)):(pCurrentDate.get(Calendar.MONTH)+1)) + "-" +  ((pCurrentDate.get(Calendar.DAY_OF_MONTH)+1)<10?("0"+(pCurrentDate.get(Calendar.DAY_OF_MONTH)+1)):(pCurrentDate.get(Calendar.DAY_OF_MONTH)+1));

                if (fromDate.toString().isEmpty() || toDate.toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), "please enter desired month value", Toast.LENGTH_SHORT).show();
                         } else {
                    try {
                        dateFrom = formatter.parse(fromDateString_bet);
                        dateTo = formatter.parse(toDateString_bet);

                        System.out.println("3Digit Date are:" + dateFrom + "and" + dateTo);
                        System.out.println("3            Digit Date are:" +toDateString_bet_comparision);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    try {
                        if (dateTo.after(formatter.parse(toDateString_bet_comparision)) || dateFrom.after(formatter.parse(toDateString_bet_comparision))){
                            Toast.makeText(getApplicationContext(), "please choose valid date", Toast.LENGTH_SHORT).show();

                        }
                        else {
                            inventoryFetch();
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                }
            });

    }




    public void inventoryFetch(){
        ArrayList<Details> filtered4Time = new ArrayList<Details>();
        ArrayList<PBST_Name> obj_and_freq = new ArrayList<PBST_Name>();
        final HashMap<PBST_Name, ArrayList<Details>> hashmap4pbst = new HashMap<PBST_Name, ArrayList<Details>>();
     //   startDate = fromDate.getText().toString();
       // endDate = toDate.getText().toString();

        for (Details det_objs:detailArray){
            if (fromDate.getVisibility()==View.INVISIBLE && toDate.getVisibility()==View.INVISIBLE) {
                        filtered4Time.add(det_objs);
                    }

            else if (fromDate.getVisibility()==View.VISIBLE && toDate.getVisibility()==View.VISIBLE) {
                try {
                    if ((formatter.parse(det_objs.get_Purchase_DateTime()).after(dateFrom)) && (formatter.parse(det_objs.get_Purchase_DateTime()).before(dateTo))) {
                        filtered4Time.add(det_objs);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
                else if (fromDate.getVisibility()==View.INVISIBLE && toDate.getVisibility()==View.VISIBLE){

                    try {
                        if ((formatter.parse(det_objs.get_Purchase_DateTime()).before(dateTo))){
                            filtered4Time.add(det_objs);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }
            }


       // final ArrayList<Details> similarObjectList= new ArrayList<>();
        Set<Details> set4Frequency = new HashSet<Details>(filtered4Time);
                for (Details freqvalue : set4Frequency) {
                    int count1 = 0;
                    PBST_Name pbst_name = new PBST_Name();
                    count1 = Collections.frequency(filtered4Time, freqvalue);
                    pbst_name.setCount(count1);
                    ArrayList<Details> detailArray4hmap = new ArrayList<Details>();

                    for (int k = 0; k < filtered4Time.size(); k++) {

                     if(freqvalue.equals(filtered4Time.get(k))){
                         detailArray4hmap.add(filtered4Time.get(k));

                        }
                    }

                    for (Products product : productArray) {
                        if (product.get_Product_Id() == freqvalue.get_Product_Id()) {
                            pbst_name.setProduct_Name(product.get_Product_Name());
                        }
                    }

                    for (Brands brand : brandArray) {
                        if (brand.get_Brand_Id() == freqvalue.get_Brand_Id()) {
                            pbst_name.setBrand_Name(brand.get_Brand_Name());
                        }
                    }

                    for (Types type : typeArray) {
                        if (type.get_Type_Id() == freqvalue.get_Type_Id()) {
                            pbst_name.setType_Name(type.get_Type_Name());
                        }
                    }

                    for (Sizes size : sizeArray) {
                        if (size.get_Size_Id() == freqvalue.get_Size_Id()) {
                            pbst_name.setSize_Name(size.get_Size_Name());
                        }
                    }

                    obj_and_freq.add(pbst_name);
                    hashmap4pbst.put(pbst_name, detailArray4hmap);
                }




        arrayAdapterD = new GridAdapter(SearchInventorybyTime.this, R.layout.layout4gridview, obj_and_freq);
        myGrid.setAdapter(arrayAdapterD);
        arrayAdapterD.notifyDataSetChanged();
        myGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

              //  similarObjectList.clear();
                myGrid.setVisibility(View.GONE);
                PBST_Name selectedFromList =(PBST_Name) (myGrid.getItemAtPosition(i));
                selectedDetailsItem=hashmap4pbst.get(selectedFromList);


                Intent intent = new Intent(SearchInventorybyTime.this,CustomGridDetailAdapter.class);
                intent.putParcelableArrayListExtra("TimeInventory", selectedDetailsItem);
                startActivity(intent);
               // finish();
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            Intent myIntent = new Intent(SearchInventorybyTime.this, MainActivity.class);
            startActivity(myIntent);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

 }






