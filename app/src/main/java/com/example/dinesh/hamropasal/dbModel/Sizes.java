package com.example.dinesh.hamropasal.dbModel;

/**
 * Created by ANONYMOUS_PAL on 4.7.2016.
 */
public class Sizes {

    private int _Size_Id;
    private String _Size_Name;
    private int _Quantity;

    public Sizes() {

    }

    public Sizes(String _Size_Name) {
        this._Size_Name = _Size_Name;
    }

    public void set_Size_Id(int _Size_Id) {
        this._Size_Id = _Size_Id;
    }

    public void set_Size_Name(String _Size_Name) {
        this._Size_Name = _Size_Name;
    }

    public void set_Quantity(int _Quantity){
        this._Quantity = _Quantity;
    }

    public int get_Quantity(){
        return _Quantity;
    }

    public int get_Size_Id() {
        return _Size_Id;
    }

    public String get_Size_Name() {
        return _Size_Name;
    }

    @Override
    public String toString() {
        return this._Size_Name ;
    }
}
