package com.example.dinesh.hamropasal.Custom;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.dinesh.hamropasal.R;
import com.example.dinesh.hamropasal.dbModel.Details;

import java.util.ArrayList;

/**
 * Created by Niroj on 8/26/2016.
 */
public class CustomDetailGrid extends BaseAdapter {
    private ArrayList<Details> data;
    private Activity mActivity;
    private LayoutInflater inflater = null;

    public CustomDetailGrid(Activity context, int textViewResourceId, ArrayList<Details> result) {
        mActivity = context;
        data = result;
        inflater = (LayoutInflater) mActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (convertView == null)
            vi = LayoutInflater.from(mActivity).inflate(R.layout.custom_grid,
                    null);

        TextView costPrice = (TextView) vi.findViewById(R.id.cost_price);
        TextView purchasedDate = (TextView) vi.findViewById(R.id.purchased_date);
        TextView purchasedCode = (TextView) vi.findViewById(R.id.product_code);



        costPrice.setText(data.get(position).get_Cost_Price());
        purchasedDate.setText(data.get(position).get_Purchase_DateTime());
        purchasedCode.setText(data.get(position).get_Product_Code());

        // SECOND PLACE TO INSERT THE onClickListener
        return vi;
    }
}