package com.example.dinesh.hamropasal.Custom;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.example.dinesh.hamropasal.R;
import com.example.dinesh.hamropasal.dbModel.Brands;
import com.example.dinesh.hamropasal.dbModel.Products;
import com.example.dinesh.hamropasal.dbModel.Sizes;
import com.example.dinesh.hamropasal.dbModel.Types;

import java.util.ArrayList;

/**
 * Created by dinesh on 24/08/2016.
 */
public class CustomSelector extends LinearLayout {

    VariableInitializer varInit;

    // Define the elements (children) of the compound view
    public EditText productName, brandName, sizeName, typeName;
    public ListView productList, brandList, sizeList, typeList;

    public ArrayAdapter productAdapter;
    public ArrayAdapter  brandAdapter;
    public ArrayAdapter  sizeAdapter;
    public ArrayAdapter  typeAdapter;



    //ID values
    public String brandID;
    public String productID;
    public String sizeID;
    public String typeID;

    public Products proObject = new Products();
    public Brands braObject = new Brands();
    public Sizes sizObject = new Sizes();
    public Types typObject = new Types();



    public CustomSelector(Context context) {
        super(context);

        varInit = new VariableInitializer(context);

        //varInit.dbInitializer(context);

        varInit.refreshProductArray();
        varInit.refreshBrandArray();
        varInit.refreshSizeArray();
        varInit.refreshTypeArray();

        //Get string reference to the LayoutInflater Service
        String inflaterService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater layoutInflater;

        //Get a LayoutInflater object
        layoutInflater = (LayoutInflater)getContext().getSystemService(inflaterService);

        // inflate the resource R.layout.custom_selector in the context of this view and automatically attach to this view
        layoutInflater.inflate(R.layout.custom_selector, this, true);

        // Get the child views
        productName = (EditText)findViewById(R.id.productName);
        brandName = (EditText)findViewById(R.id.brandName);
        sizeName = (EditText)findViewById(R.id.sizeName);
        typeName = (EditText)findViewById(R.id.typeName);

        productList = (ListView)findViewById(R.id.productList);
        brandList = (ListView)findViewById(R.id.brandList);
        sizeList = (ListView)findViewById(R.id.sizeList);
        typeList = (ListView)findViewById(R.id.typeList);

        productList.setAlpha(1);
        brandList.setAlpha(1);
        sizeList.setAlpha(1);
        typeList.setAlpha(1);
        /**

         **/

        //enables filtering for the contents of the given ListView
        productList.setTextFilterEnabled(true);
        brandList.setTextFilterEnabled(true);
        sizeList.setTextFilterEnabled(true);
        typeList.setTextFilterEnabled(true);


        productAdapter = new ArrayAdapter(context,R.layout.adapter_item, varInit.productArray);
        brandAdapter = new ArrayAdapter(context, R.layout.adapter_item, varInit.brandArray);
        sizeAdapter = new ArrayAdapter(context, R.layout.adapter_item, varInit.sizeArray);
        typeAdapter = new ArrayAdapter(context, R.layout.adapter_item, varInit.typeArray);

        // hook up for edit texts and list views
        hookupForListviews(context);
    }

    public void hookupForListviews(final Context context ){

        productName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus){
                    /**
                     * Following method for productArray must be implemented to update the list after product insert in DB
                     * proAdapter.notifyDataSetChanged(); should be called after the successful update of array
                     */
                    //productArray= myProduct.getAllProducts();
                    //arrayAdapterP = new ArrayAdapter(MainActivity.this,R.layout.adapter_item, productArray);
                    productList.setVisibility(View.VISIBLE);
                    productList.setAdapter(productAdapter);
                    productList.bringToFront();
                    productList.invalidate();

                }
                if(!hasFocus){
                    productList.setVisibility(View.GONE);

                }
            }
        });

        productName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                productAdapter.getFilter().filter(s.toString());
                productID = null;
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        AdapterView.OnItemClickListener productListener = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                proObject = (Products) productAdapter.getItem(position);
                //Toast.makeText(MainActivity.this,"product Name:"+ proObject.get_Product_Name() ,Toast.LENGTH_SHORT).show();
                productName.setText(proObject.get_Product_Name());
                productID = String.valueOf( proObject.get_Product_Id());
                Toast.makeText(context,"productID value"+ productID ,Toast.LENGTH_SHORT).show();
                System.out.println("The product is  --------"+ proObject);
                //for hiding the list view after selecting the item
                productList.setVisibility(View.GONE);


            }
        };

        productList.setOnItemClickListener(productListener);

        brandName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus){
                    /**
                     * Following method for Array must be implemented to update the list after product insert in DB
                     * Adapter.notifyDataSetChanged(); should be called after the successful update of array
                     */
                    //brandArray= myBrand.getAllBrands();
                    //arrayAdapterB = new ArrayAdapter(MainActivity.this,R.layout.adapter_item, brandArray);
                    brandList.setVisibility(View.VISIBLE);
                    brandList.setAdapter(brandAdapter);
                    brandList.bringToFront();
                    brandList.invalidate();

                }
                if(!hasFocus){
                    brandList.setVisibility(View.GONE);

                }
            }
        });

        brandName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                brandAdapter.getFilter().filter(s.toString());
                brandID = null;
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        AdapterView.OnItemClickListener brandListener = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                braObject = (Brands) brandAdapter.getItem(position);
                //Toast.makeText(MainActivity.this,"Brand Name:"+ braObject.get_Brand_Name() ,Toast.LENGTH_SHORT).show();
                brandName.setText(braObject.get_Brand_Name());
                brandID = String.valueOf( braObject.get_Brand_Id());
                Toast.makeText(context,"brandID value"+ brandID ,Toast.LENGTH_SHORT).show();

                //for hiding the list view after selecting the item
                brandList.setVisibility(View.GONE);

            }
        };
        brandList.setOnItemClickListener(brandListener);

        sizeName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus){
                    /**
                     * Following method for Array must be implemented to update the list after product insert in DB
                     * Adapter.notifyDataSetChanged(); should be called after the successful update of array
                     */
                    //sizeArray= mySize.getAllSizes();
                    //arrayAdapterS = new ArrayAdapter(MainActivity.this,R.layout.adapter_item, sizeArray);
                    sizeList.setVisibility(View.VISIBLE);
                    sizeList.setAdapter(sizeAdapter);
                    sizeList.bringToFront();
                    sizeList.invalidate();
                    
                }
                if(!hasFocus){
                    sizeList.setVisibility(View.GONE);
                }
            }
        });

        sizeName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                sizeAdapter.getFilter().filter(s.toString());
                sizeID = null;
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        AdapterView.OnItemClickListener sizeListener = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                sizObject = (Sizes) sizeAdapter.getItem(position);
                //Toast.makeText(MainActivity.this,"Size Name:"+ sizObject.get_Size_Name() ,Toast.LENGTH_SHORT).show();
                sizeName.setText(sizObject.get_Size_Name());
                sizeID = String.valueOf( sizObject.get_Size_Id());
                Toast.makeText(context,"sizeID value"+ sizeID ,Toast.LENGTH_SHORT).show();

                //for hiding the list view after selecting the item
                sizeList.setVisibility(View.GONE);
            }
        };

        sizeList.setOnItemClickListener(sizeListener);

        typeName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus){
                    /**
                     * Following method for Array must be implemented to update the list after product insert in DB
                     * Adapter.notifyDataSetChanged(); should be called after the successful update of array
                     */
                    //typeArray = myType.getAllTypes();
                    //arrayAdapterT = new ArrayAdapter(MainActivity.this,R.layout.adapter_item, typeArray);
                    typeList.setVisibility(View.VISIBLE);
                    typeList.setAdapter(typeAdapter);
                    typeList.bringToFront();
                    typeList.invalidate();
                }
                if(!hasFocus){
                    typeList.setVisibility(View.GONE);
                }
            }
        });

        typeName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                typeAdapter.getFilter().filter(s.toString());
                typeID = null;
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        AdapterView.OnItemClickListener typeListener = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                typObject = (Types) typeAdapter.getItem(position);
                //Toast.makeText(MainActivity.this,"Type Name:"+ typObject.get_Type_Name() ,Toast.LENGTH_SHORT).show();
                typeName.setText(typObject.get_Type_Name());
                typeID = String.valueOf( typObject.get_Type_Id());
                Toast.makeText(context,"typeID value"+ typeID ,Toast.LENGTH_SHORT).show();

                //for hiding the list view after selecting the item
                typeList.setVisibility(View.GONE);

            }
        };

        typeList.setOnItemClickListener(typeListener);


    }

}
