package com.example.dinesh.hamropasal.dbModel;

public class Products {
    private int _Product_Id;
    private String _Product_Name;
    private int _Quantity;

    public Products() {
    }

   public Products(String productname) {
        this._Product_Name = productname;
    }


    /**
    public Products(int productID, String productname) {
        this._Product_Name = productname;
        this._Product_Id = productID;
    }
     **/

    public void set_Product_Id(int _Product_Id) {
        this._Product_Id = _Product_Id;
    }

    public void set_Product_Name(String _Product_Name) {
        this._Product_Name = _Product_Name;
    }

    public void set_Quantity(int _Quantity){
        this._Quantity = _Quantity;
    }


    public int get_Product_Id() {
        return _Product_Id;
    }

    public String get_Product_Name() {
        return _Product_Name;
    }

    public int get_Quantity(){
        return _Quantity;
    }


  @Override
    public String toString() {
        return this._Product_Name ;
    }
}
