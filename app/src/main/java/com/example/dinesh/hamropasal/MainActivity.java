package com.example.dinesh.hamropasal;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.view.KeyEvent;

public class MainActivity  extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    /**

    EditText  detail_Id,  costPrice, sellPrice, productCode;
    EditText  productName, brandName, sizeName, typeName ,purchasedDt, soldDt;
    //TextView db_message;
    //AutoCompleteTextView productName, brandName, sizeName,typeName;
    Button addProduct, deleteDetail, viewAll, updateProduct, addBrand, addSize, addType, addDetail;
    ListView productList, brandList, sizeList, typeList;

    private ArrayList<Products> productArray;
    private ArrayList<Brands> brandArray;
    private ArrayList<Sizes> sizeArray;
    private ArrayList<Types> typeArray;
    private ArrayList<Details> detailArray;
    //private ArrayList<Relationships> relationArray;

    ArrayAdapter  productAdapter;
    ArrayAdapter  brandAdapter;
    ArrayAdapter  sizeAdapter;
    ArrayAdapter  typeAdapter;

    //ProductsAdapter proAdapter;

    //ID values
    private String brandID;
    private  String productID;
    private  String sizeID;
    private  String typeID;
    private  long detailID;

    //Other product detail values
    String productValue;
    String brandValue;
    String typeValue;
    String sizeValue;
    String productCodeValue;
    String soldPriceValue;
    String costPriceValue;
    String sellDateValue;
    String purchaseDateValue;
    String profitValue;

    int pYear, pMonth, pDay;
    **/

    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    /**
    private DBHelper myDB;
    private ProductHelper myProduct;
    private BrandHelper myBrand;
    private TypeHelper myType;
    private SizeHelper mySize;
    private DetailHelper myDetail;
    //private RelationshipHelper myRelation;
     **/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        super.onCreateDrawer();


        /**

        myDB = new DBHelper(this);
        myProduct = new ProductHelper(this);
        myBrand = new BrandHelper(this);
        myType = new TypeHelper(this);
        mySize = new SizeHelper(this);
        myDetail = new DetailHelper(this);
        //myRelation = new RelationshipHelper(this);
        //pro = new Products();

        //productName = (AutoCompleteTextView) findViewById(R.id.productName);
        productName = (EditText) findViewById(R.id.productName);
        // brandName = (AutoCompleteTextView) findViewById(R.id.brandName);
        brandName = (EditText) findViewById(R.id.brandName);
        detail_Id = (EditText) findViewById(R.id.detail_Id);
        //sizeName = (AutoCompleteTextView) findViewById(R.id.sizeName);
        sizeName = (EditText) findViewById(R.id.sizeName);
        //typeName = (AutoCompleteTextView) findViewById(R.id.typeName);
        typeName = (EditText) findViewById(R.id.typeName);
        costPrice = (EditText) findViewById(R.id.costPrice);
        sellPrice = (EditText) findViewById(R.id.sellPrice);
        productCode = (EditText) findViewById(R.id.productCode);
        purchasedDt = (EditText) findViewById(R.id.purchasedDate);
        soldDt = (EditText) findViewById(R.id.soldDate);


        //db_message = (TextView) findViewById(R.id.db_message);
        addProduct = (Button) findViewById(R.id.addProduct) ;
        viewAll = (Button) findViewById(R.id.viewAll);
        deleteDetail = (Button) findViewById(R.id.deleteDetail);
        updateProduct = (Button) findViewById(R.id.updateProduct);
        addBrand = (Button) findViewById(R.id.addBrand);
        addSize = (Button) findViewById(R.id.addSize);
        addType = (Button) findViewById(R.id.addType);
        addDetail = (Button) findViewById(R.id.addDetail);

        productList = (ListView) findViewById(R.id.productList);
        brandList = (ListView) findViewById(R.id.brandList);
        sizeList = (ListView) findViewById(R.id.sizeList);
        typeList = (ListView) findViewById(R.id.typeList);

        productArray = myProduct.getAllProducts();
        brandArray = myBrand.getAllBrands();
        sizeArray = mySize.getAllSizes();
        typeArray = myType.getAllTypes();
        detailArray = myDetail.getAllDetails();
        //relationArray = myRelation.getAllProductsDetails();

        Calendar pCurrentDate= Calendar.getInstance();
        pYear=pCurrentDate.get(Calendar.YEAR);
        pMonth=pCurrentDate.get(Calendar.MONTH)+1;
        pDay=pCurrentDate.get(Calendar.DAY_OF_MONTH);
        purchasedDt.setText("" + pYear + "-" + pMonth + "-" + pDay);
        soldDt.setText("" + pYear + "-" + pMonth + "-" + pDay);


        purchasedDt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar pCurrentDate= Calendar.getInstance();
                pYear=pCurrentDate.get(Calendar.YEAR);
                pMonth=pCurrentDate.get(Calendar.MONTH);
                pDay=pCurrentDate.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog mDatePicker=new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        purchasedDt.setText("" + selectedyear + "-" + (selectedmonth+1) + "-" + selectedday);            }
                },pYear, pMonth, pDay);
                mDatePicker.show();
            }
        });

        soldDt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar sCurrentDate=Calendar.getInstance();
                int sYear=sCurrentDate.get(Calendar.YEAR);
                int sMonth=sCurrentDate.get(Calendar.MONTH)+1;
                int  sDay=sCurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        soldDt.setText("" + selectedyear + "-" + (selectedmonth+1) + "-" + selectedday);            }
                },sYear, sMonth, sDay);
                mDatePicker.setTitle("Select date");
                mDatePicker.show();
            }
        });




        //enables filtering for the contents of the given ListView
        productList.setTextFilterEnabled(true);
        brandList.setTextFilterEnabled(true);
        sizeList.setTextFilterEnabled(true);
        typeList.setTextFilterEnabled(true);

        //working code without custom adapter
        //Product Filter
        //productArray= myProduct.getAllProducts();
        productAdapter = new ArrayAdapter(MainActivity.this,R.layout.adapter_item, productArray);
        productName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus){
                    //productArray= myProduct.getAllProducts();
                    //arrayAdapterP = new ArrayAdapter(MainActivity.this,R.layout.adapter_item, productArray);
                    productList.setVisibility(View.VISIBLE);
                    productList.setAdapter(productAdapter);
                }
                if(!hasFocus){
                    productList.setVisibility(View.GONE);
                }
            }
        });


        productName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                productAdapter.getFilter().filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        AdapterView.OnItemClickListener productListener = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Products proObject = (Products) productAdapter.getItem(position);
                //Toast.makeText(MainActivity.this,"product Name:"+ proObject.get_Product_Name() ,Toast.LENGTH_SHORT).show();
                productName.setText(proObject.get_Product_Name());
                productID = String.valueOf( proObject.get_Product_Id());
                Toast.makeText(MainActivity.this,"productID value"+ productID ,Toast.LENGTH_SHORT).show();
                System.out.println("The product is  --------"+ proObject);

            }
        };

        productList.setOnItemClickListener(productListener);

        //brandArray= myBrand.getAllBrands();
        brandAdapter = new ArrayAdapter(MainActivity.this,R.layout.adapter_item, brandArray);
        //customAdapter = new CustomAdapter(MainActivity.this,R.layout.adapter_item, productArray);
        brandName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus){
                    //brandArray= myBrand.getAllBrands();
                    //arrayAdapterB = new ArrayAdapter(MainActivity.this,R.layout.adapter_item, brandArray);
                    brandList.setVisibility(View.VISIBLE);
                    brandList.setAdapter(brandAdapter);
                }
                if(!hasFocus){
                    brandList.setVisibility(View.GONE);
                }
            }
        });
        brandName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                brandAdapter.getFilter().filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        AdapterView.OnItemClickListener brandListener = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Brands braObject = (Brands) brandAdapter.getItem(position);
                //Toast.makeText(MainActivity.this,"Brand Name:"+ braObject.get_Brand_Name() ,Toast.LENGTH_SHORT).show();
                brandName.setText(braObject.get_Brand_Name());
                brandID = String.valueOf( braObject.get_Brand_Id());
                Toast.makeText(MainActivity.this,"brandID value"+ brandID ,Toast.LENGTH_SHORT).show();
            }
        };
        brandList.setOnItemClickListener(brandListener);



        //Size Filter
        //sizeArray= mySize.getAllSizes();
        sizeAdapter = new ArrayAdapter(MainActivity.this,R.layout.adapter_item, sizeArray);
        //customAdapter = new CustomAdapter(MainActivity.this,R.layout.adapter_item, productArray);
        sizeName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus){
                    //sizeArray= mySize.getAllSizes();
                    //arrayAdapterS = new ArrayAdapter(MainActivity.this,R.layout.adapter_item, sizeArray);
                    sizeList.setVisibility(View.VISIBLE);
                    sizeList.setAdapter(sizeAdapter);
                }
                if(!hasFocus){
                    sizeList.setVisibility(View.GONE);
                }
            }
        });
        sizeName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                sizeAdapter.getFilter().filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        AdapterView.OnItemClickListener sizeListener = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Sizes sizObject = (Sizes) sizeAdapter.getItem(position);
                //Toast.makeText(MainActivity.this,"Size Name:"+ sizObject.get_Size_Name() ,Toast.LENGTH_SHORT).show();
                sizeName.setText(sizObject.get_Size_Name());
                sizeID = String.valueOf( sizObject.get_Size_Id());
                Toast.makeText(MainActivity.this,"sizeID value"+ sizeID ,Toast.LENGTH_SHORT).show();
            }
        };

        sizeList.setOnItemClickListener(sizeListener);


        //Type Filter
        //typeArray = myType.getAllTypes();
        typeAdapter = new ArrayAdapter(MainActivity.this,R.layout.adapter_item, typeArray);
        typeName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus){
                    //typeArray = myType.getAllTypes();
                    //arrayAdapterT = new ArrayAdapter(MainActivity.this,R.layout.adapter_item, typeArray);
                    typeList.setVisibility(View.VISIBLE);
                    typeList.setAdapter(typeAdapter);
                }
                if(!hasFocus){
                    typeList.setVisibility(View.GONE);
                }
            }
        });
        typeName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                typeAdapter.getFilter().filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        AdapterView.OnItemClickListener typeListener = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Types typObject = (Types) typeAdapter.getItem(position);
                //Toast.makeText(MainActivity.this,"Type Name:"+ typObject.get_Type_Name() ,Toast.LENGTH_SHORT).show();
                typeName.setText(typObject.get_Type_Name());
                typeID = String.valueOf( typObject.get_Type_Id());
                Toast.makeText(MainActivity.this,"typeID value"+ typeID ,Toast.LENGTH_SHORT).show();
            }
        };

        typeList.setOnItemClickListener(typeListener);
        **/

    }




    /**
    public void addProductClicked (View view){
        if(productName.getText().toString().trim().length() >0){
            Products product = new Products();
            product.set_Product_Name(productName.getText().toString());
            long isInserted = myProduct.productInsert(product);
            if(isInserted > -1){
                product.set_Product_Id((int)isInserted);
                productArray.add(product);
                productAdapter.add(product);
                productAdapter.notifyDataSetChanged();
                Toast.makeText(MainActivity.this,"Product was Inserted",Toast.LENGTH_SHORT).show();
                productName.setText("");
            }else{
                Toast.makeText(MainActivity.this,"Product was not Inserted",Toast.LENGTH_SHORT).show();
                productName.setText("");
            }
        }else {
            Toast.makeText(MainActivity.this,"Select or insert Product",Toast.LENGTH_SHORT).show();
        }
    }
    //Add a brand to the database
    public void addBrandClicked (View view){
        if(brandName.getText().toString().trim().length() > 0){
            Brands brand = new Brands();
            brand.set_Brand_Name(brandName.getText().toString());
            long isInserted = myBrand.brandInsert(brand);
            if(isInserted > -1){
                brand.set_Brand_Id((int)isInserted);
                brandArray.add(brand);
                brandAdapter.add(brand);
                brandAdapter.notifyDataSetChanged();
                Toast.makeText(MainActivity.this,"Brand was Inserted",Toast.LENGTH_SHORT).show();
                brandName.setText("");
            }else{
                Toast.makeText(MainActivity.this,"Brand was not Inserted",Toast.LENGTH_SHORT).show();
                brandName.setText("");
            }
        }else {
            Toast.makeText(MainActivity.this,"Select or insert Brand",Toast.LENGTH_SHORT).show();
        }
    }
    //Add a Size to the database
    public void addSizeClicked (View view){
        if(sizeName.getText().toString().trim().length() > 0){
            Sizes size = new Sizes();
            size.set_Size_Name(sizeName.getText().toString());
            long isInserted = mySize.sizeInsert(size);
            //Boolean isInserted = mySize.sizeInsert(sizeName.getText().toString());
            if(isInserted > -1){
                size.set_Size_Id((int) isInserted);
                sizeArray.add(size);
                sizeAdapter.add(size);
                sizeAdapter.notifyDataSetChanged();
                Toast.makeText(MainActivity.this,"Size was Inserted",Toast.LENGTH_SHORT).show();
                sizeName.setText("");
            }else{
                Toast.makeText(MainActivity.this,"Size was not Inserted",Toast.LENGTH_SHORT).show();
                sizeName.setText("");
            }
        }else{
            Toast.makeText(MainActivity.this,"Select or insert Size",Toast.LENGTH_SHORT).show();
        }
    }
    //Add a Type to the database
    public void addTypeClicked (View view){
        if(typeName.getText().toString().trim().length() >0){
            Types type = new Types();
            type.set_Type_Name(typeName.getText().toString());
            long isInserted = myType.typeInsert(type);
            if(isInserted > -1){
                type.set_Type_Id((int)isInserted);
                typeArray.add(type);
                typeAdapter.add(type);
                typeAdapter.notifyDataSetChanged();
                Toast.makeText(MainActivity.this,"Type was Inserted",Toast.LENGTH_SHORT).show();
                typeName.setText("");
            }else{
                Toast.makeText(MainActivity.this,"Type was not Inserted",Toast.LENGTH_SHORT).show();
                typeName.setText("");
            }
        }else{
            Toast.makeText(MainActivity.this,"Select or insert Type",Toast.LENGTH_SHORT).show();
        }
    }
    //Add a Detail to the database
    public void addDetailClicked (View view){

        if((productID !=null) && (brandID !=null) && (sizeID != null) && (typeID !=null) && (costPrice.getText().toString().trim().length()>0)// && (sellPrice.getText().toString().trim().length() > 0 )
                && (productCode.getText().toString().trim().length() > 0 )) {
            Details detail = new Details();
            detail.set_Product_Id(Integer.parseInt(productID));
            detail.set_Brand_Id(Integer.parseInt(brandID));
            detail.set_Size_Id(Integer.parseInt(sizeID));
            detail.set_Type_Id(Integer.parseInt(typeID));
            detail.set_Cost_Price(costPrice.getText().toString());
            detail.set_Sold_Price(sellPrice.getText().toString());
            detail.set_Product_Code(productCode.getText().toString());
            detail.set_Purchase_DateTime(purchasedDt.getText().toString());
            detail.set_Sold_DateTime(soldDt.getText().toString());
            //detailID = myDetail.Detail_insert(costPrice.getText().toString(),sellPrice.getText().toString(),productCode.getText().toString());
            detailID = myDetail.Detail_insert(detail);
            if (detailID > -1) {
                detail.set_Detail_Id((int)detailID);
                detailArray.add(detail);
                Toast.makeText(MainActivity.this, "Detail was Inserted", Toast.LENGTH_SHORT).show();
                costPrice.setText("");
                sellPrice.setText("");
                productCode.setText("");


            } else {
                Toast.makeText(MainActivity.this, "Detail was not Inserted", Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(MainActivity.this, "Select or insert all product information", Toast.LENGTH_SHORT).show();
        }

    }

    //Update a product to the database
    public void updateProductClicked (View view){
        //&& (detail_Id.toString().trim().length() > 0
        if((productName.getText().toString().trim().length() > 0)&& (detail_Id.toString().trim().length() > 0)){
        //if(!productName.getText().toString().isEmpty()){
            Products product = new Products();
            //Products product = new Products(productName.getText().toString());
            product.set_Product_Name(productName.getText().toString());
            product.set_Product_Id(Integer.parseInt(detail_Id.getText().toString()));
            //Boolean isUpdated = myProduct.productUpdate(Integer.parseInt(detail_Id.getText().toString()),productName.getText().toString());
            long isUpdated = myProduct.productUpdate(product);
            int i = 0;
            if(isUpdated > 0){

                 //still we need to find the solution to update the
                 //list view when the product object is updated

               // i = productAdapter.getPosition(product);
               // productAdapter.add(i,product);
                //productAdapter.notifyDataSetChanged();
                Toast.makeText(MainActivity.this,"Product Updated",Toast.LENGTH_SHORT).show();
                detail_Id.setText("");
                productName.setText("");
            }else{
                Toast.makeText(MainActivity.this,"Product not Updated",Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(MainActivity.this,"Type product name ***Provide ID***",Toast.LENGTH_SHORT).show();
        }
    }

    public void viewAllProductClicked (View view){
        //detailArray = myDetail.getAllDetails();
        if(detailArray.isEmpty()){
            //some error
            showData("No Products", "Fill Some Product");
            return;
        }else{
            StringBuffer buffer = new StringBuffer();
            for(Details detail:detailArray){

                for(Products product:productArray){
                    if(product.get_Product_Id() == detail.get_Product_Id()){
                        productValue = product.get_Product_Name();
                    }
                }
                for(Brands brand:brandArray){
                    if(brand.get_Brand_Id() == detail.get_Brand_Id()){
                        brandValue = brand.get_Brand_Name();
                    }
                }
                for(Sizes size:sizeArray){
                    if(size.get_Size_Id() == detail.get_Size_Id()){
                        sizeValue = size.get_Size_Name();
                    }
                }
                for(Types type:typeArray){
                    if(type.get_Type_Id() == detail.get_Type_Id()){
                        typeValue = type.get_Type_Name();
                    }
                }
                //for(Details detail:detailArray){
                    //if(detail.get_Detail_Id() == detail.get_Detail_Id()){
                        productCodeValue = detail.get_Product_Code();
                        soldPriceValue = detail.get_Sold_Price();
                        costPriceValue = detail.get_Cost_Price();
                        sellDateValue = detail.get_Sold_DateTime();
                        purchaseDateValue = detail.get_Purchase_DateTime();
                        profitValue = detail.get_Profit();
                   // }
               // }

                buffer.append("Product: "+ productValue +" Brand: "+ brandValue +" Size: "+sizeValue+" Type: "+typeValue+" ProductCode: "+productCodeValue
                        +" SoldPrice: "+soldPriceValue+" CostPrice: "+costPriceValue+" SellDate: "+sellDateValue+" PurchaseDate: "+purchaseDateValue+" Profit: "+profitValue+"\n");
            }
            //Show all the data
            showData("Data", buffer.toString());
        }
    }


    //Alert Dialog initialization for DB table Data
    public void showData( String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }

    //Delete items

    public void deleteDetailClicked(View view){
        if(detail_Id.getText().toString().trim().length() > 0 ){
            Details detail = new Details();
            detail.set_Detail_Id(Integer.parseInt(detail_Id.getText().toString()));
            //String detailID = detail_Id.getText().toString();
            int isDeleted = myDetail.method_delete(detail);
            //db_message.setText("Delete Button Clicked");

            if(isDeleted > 0){
                Toast.makeText(MainActivity.this,"Product Detail Deleted",Toast.LENGTH_SHORT).show();
                detail_Id.setText("");
            }else{
                Toast.makeText(MainActivity.this,"Product Detail not Deleted",Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(MainActivity.this,"*****Provide Detail ID*******",Toast.LENGTH_SHORT).show();
        }
    }

     **/

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK ) {
            moveTaskToBack(true);

        }
        return super.onKeyDown(keyCode, event);
    }
}
