package com.example.dinesh.hamropasal.Custom;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.dinesh.hamropasal.R;
import com.example.dinesh.hamropasal.dbModel.Credits;

import java.util.ArrayList;

public class CreditViewAdapter extends BaseAdapter {
    private ArrayList<Credits> data;
    private Activity mActivity;
    private LayoutInflater inflater = null;

    public CreditViewAdapter(Activity context, int textViewResourceId, ArrayList<Credits> result) {
        mActivity = context;
        data = result;
        inflater = (LayoutInflater) mActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (convertView == null)
            vi = LayoutInflater.from(mActivity).inflate(R.layout.credit_view_adapter,
                    null);

        TextView firstName = (TextView) vi.findViewById(R.id.firstName);
        TextView lastName = (TextView) vi.findViewById(R.id.lastName);
        TextView phoneNumber = (TextView) vi.findViewById(R.id.phoneNumber);


        firstName.setText(data.get(position).get_Firstname());
        lastName.setText(data.get(position).get_Lastname());
        phoneNumber.setText(data.get(position).get_Phonenumber());

        // SECOND PLACE TO INSERT THE onClickListener
        return vi;
    }
}


