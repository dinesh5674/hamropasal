package com.example.dinesh.hamropasal;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AlertDialog;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dinesh.hamropasal.CRUD.DetailHelper;
import com.example.dinesh.hamropasal.Custom.BookKeepingDetailGrid;

import com.example.dinesh.hamropasal.Custom.CustomDetailGrid;
import com.example.dinesh.hamropasal.dbModel.Details;

import java.util.ArrayList;

/**
 * Created by Niroj on 8/27/2016.
 */
public class CustomGridDetailAdapter extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    ArrayList <Details>  selectedDetails= new ArrayList<>();
    GridView customGrid;
    Details detailObject;
    DetailHelper myDetail;
    Intent intent;
    CustomDetailGrid adapter;
    BookKeepingDetailGrid adapter1;
    TextView profit;
    public static CustomGridDetailAdapter customAdaptorObject;

    protected static final String ACTION_DO_C = "Inventory";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customgridlayout);
        customAdaptorObject = this;
        customGrid= (GridView)findViewById(R.id.gridView2);
        profit= (TextView)findViewById(R.id.profit);
        super.onCreateDrawer();
        detailObject = new Details();
        myDetail = new DetailHelper(this);
        intent=getIntent();

        if (intent.hasExtra("ProductInventory")){
            profit.setVisibility(View.GONE);
            selectedDetails=  getIntent().getParcelableArrayListExtra("ProductInventory");
            adapter= new CustomDetailGrid(CustomGridDetailAdapter.this, R.layout.custom_grid, selectedDetails);
            customGrid.setAdapter(adapter);

        }
        if (intent.hasExtra("TimeInventory")) {
            profit.setVisibility(View.GONE);
            selectedDetails = getIntent().getParcelableArrayListExtra("TimeInventory");
            adapter = new CustomDetailGrid(CustomGridDetailAdapter.this, R.layout.custom_grid, selectedDetails);
            customGrid.setAdapter(adapter);

        }

         if (intent.hasExtra("Bookkeeping")){

            selectedDetails = getIntent().getParcelableArrayListExtra("Bookkeeping");
            adapter1 = new BookKeepingDetailGrid(CustomGridDetailAdapter.this, R.layout.bookkeeping_detail, selectedDetails);
            customGrid.setAdapter(adapter1);
         }







            registerForContextMenu(customGrid);

            customGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Toast.makeText(getApplicationContext(), "please long click to see menu", Toast.LENGTH_SHORT).show();


                }
            });

            customGrid.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view,
                                               int position, long id) {
                    detailObject = selectedDetails.get(position);

               /* Intent intent = new Intent(CustomGridDetailAdapter.this,SalesCreditForm.class);
                intent.putExtra("inventoryObject", detailObject);
                startActivity(intent);
                finish();*/
                    return false;

                }
            });
        }



    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId()==R.id.gridView2) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.context_menu, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.update:
                Intent intent = new Intent(CustomGridDetailAdapter.this,SalesCreditForm.class);
                intent.putExtra("inventoryObject", detailObject);
                startActivity(intent);

               /* Intent intent = new Intent(CustomGridDetailAdapter.this,SalesCreditForm.class);
                intent.setAction(ACTION_DO_C);
               // setResult(Activity.RESULT_OK, resultIntent);
                intent.putParcelableArrayListExtra("inventoryObject", detailObject);
                startActivityForResult (intent,2);*/


                return true;
            case R.id.delete:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                // set title
                alertDialogBuilder.setTitle("Are u Sure?");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Click yes to Delete!")
                        .setCancelable(false)
                        .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                int isDeleted = myDetail.detail_delete(detailObject);
                                if (isDeleted > 0) {
                                            Toast.makeText(getApplicationContext()," This detail is Deleted", Toast.LENGTH_SHORT).show();
                                                   } else {
                                                  Toast.makeText(getApplicationContext()," This detail was not Deleted. Try Again", Toast.LENGTH_SHORT).show();
                                            }
                                CustomGridDetailAdapter.this.finish();
                            }
                        })
                        .setNegativeButton("No",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                      Toast.makeText(getApplicationContext()," Delete process was Canceled", Toast.LENGTH_SHORT).show();
                                dialog.cancel();
                            }
                        });


                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return true;

            default:
                return super.onContextItemSelected(item);
        }
    }



    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK ) {

            if (intent.hasExtra("ProductInventory")){
                Intent myIntent = new Intent(CustomGridDetailAdapter.this, SearchInvenProduct.class);
                startActivity(myIntent);
                this.finish();
            }
            if (intent.hasExtra("TimeInventory")){
                Intent myIntent = new Intent(CustomGridDetailAdapter.this, SearchInventorybyTime.class);
                startActivity(myIntent);
                this.finish();
            }
            if (intent.hasExtra("Bookkeeping")){
                Intent myIntent = new Intent(CustomGridDetailAdapter.this, BookKeeping.class);
                startActivity(myIntent);
                this.finish();
            }

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==2)
        {
            String message=data.getStringExtra("MESSAGE");
            textView1.setText(message);
        }
    }*/

}
