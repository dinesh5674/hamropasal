package com.example.dinesh.hamropasal.Custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.dinesh.hamropasal.R;
import com.example.dinesh.hamropasal.dbModel.PBST_Name;

import java.util.ArrayList;

/**
 * Created by dinesh on 8/25/2016.
 */
public class GridAdapter extends ArrayAdapter<PBST_Name> {

    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<PBST_Name> pbstArray;
    //private boolean grid_label = true;

    /**private ProductHelper gridProduct;
    private BrandHelper gridBrand;
    private TypeHelper gridType;
    private SizeHelper gridSize;**/


    public GridAdapter(Context context, int textViewResourceId, ArrayList <PBST_Name> pbstArray){
        super(context, textViewResourceId, pbstArray);
        this.context = context;
        this.pbstArray = pbstArray;
    }



    /**public gridAdapter(View.OnClickListener onClickListener, DetailHelper myDetail) {
     layoutInflater = LayoutInflater.from(context);
     detailArray = myDetail.getAllDetails();
     }**/

    @Override
    public int getCount() {
        return pbstArray.size();
    }

    @Override
    public PBST_Name getItem(int position) {
        return pbstArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        //View grid;
        // Get the data item for this position
        PBST_Name pbst = pbstArray.get(position);

        /**
         if(convertView==null){
         grid = new View(context);
         grid = layoutInflater.inflate(R.layout.gridlayout, null);
         }else{
         grid = (View)convertView;
         }
         **/
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout4gridview, viewGroup, false);
        }



        if(pbst != null) {
            // Lookup view for data population
            //TextView adapterItem = (TextView) convertView.findViewById(R.id.adapterItem);
            //TextView tvHome = (TextView) convertView.findViewById(R.id.tvHome);
            // Populate the data into the template view using the data object
            //adapterItem.setText(pro.get_Product_Name());


            /** gridProduct = new ProductHelper(this);
             gridBrand = new BrandHelper(this);
             gridType = new TypeHelper(this);
             gridSize = new SizeHelper(this);


             ArrayList <Products> productArray;
             productArray = gridProduct.getAllProducts();
             ArrayList <Brands> BrandArray;
             BrandArray = gridBrand.getAllBrands();
             ArrayList <Sizes> SizeArray;
             SizeArray=gridSize.getAllSizes();
             ArrayList <Types> TypesArray;
             TypesArray=gridType.getAllTypes();**/

            TextView product_name= (TextView)convertView.findViewById(R.id.product_name);
            TextView brand_name = (TextView)convertView.findViewById(R.id.brand_name);
            TextView type_name = (TextView)convertView.findViewById(R.id.type_name);
            TextView size_name = (TextView)convertView.findViewById(R.id.size_name);
            TextView quantity_name = (TextView)convertView.findViewById(R.id.quantity_name);
            product_name.setText(pbst.getProduct_Name());
            brand_name.setText(pbst.getBrand_Name());
            type_name.setText(pbst.getType_Name());
            size_name.setText(pbst.getSize_Name());
            quantity_name.setText(String.valueOf(pbst.getCount()));
            /**
             if (grid_label==true && position==0) {
             textView6.setText("Products");
             textView7.setText("Brands");
             textView8.setText("Types");
             textView9.setText("Sizes");
             //textView10.setText("Quantity");
             grid_label=false;
             }
             **/

           /* else {

                for (Products products : productArray) {
                    if (rel.get_Product_Id() == products.get_Product_Id())
                        textView6.setText(products.get_Product_Name());
                }
                for (Brands brands : BrandArray) {
                    if (rel.get_Brand_Id() == brands.get_Brand_Id())
                        textView7.setText(brands.get_Brand_Name());
                }
                for (Types types : TypesArray) {
                    if (rel.get_Type_Id() == types.get_Type_Id())
                        textView7.setText(types.get_Type_Name());
                }
                for (Sizes sizes : SizeArray) {
                    if (rel.get_Size_Id() == sizes.get_Size_Id())
                        textView7.setText(sizes.get_Size_Name());
                }
            }*/



            /**TextView textView10 = (TextView)convertView.findViewById(R.id.text10);
             textView10.setText(det.get_quantity);**/

        }

        //textView.setText(String.valueOf(position));

        return convertView;
    }

}
