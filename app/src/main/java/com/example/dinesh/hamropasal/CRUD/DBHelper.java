package com.example.dinesh.hamropasal.CRUD;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by ANONYMOUS_PAL on 7.7.2016.
 */
public class DBHelper extends SQLiteOpenHelper {

    public static final String Database_Name = "Smart_Pasal.db";
    public static final int Database_Version = 1;

    public static final String Product_Table = "Product";
    public static final String Product_ID = "product_Id";
    public static final String Product_Name = "productName";

    public static final String Brand_Table = "Brand";
    public static final String Brand_ID = "brand_Id";
    public static final String Brand_Name = "brandName";


    public static final String Gender_Table = "Gender";
    public static final String Gender_ID = "gender_Id";
    public static final String Gender_Name = "genderName";

    public static final String Type_Table = "Type";
    public static final String Type_ID = "type_Id";
    public static final String Type_Name = "typeName";

    public static final String Size_Table = "Size";
    public static final String Size_ID = "size_Id";
    public static final String Size_Name = "sizeName";

    public static final String Color_Table = "Color";
    public static final String Color_ID = "color_Id";
    public static final String Color_Name = "colorName";

    public static final String ProductCode_Table = "ProductCode";
    public static final String ProductCode_ID = "productCode_Id";
    public static final String ProductCode_Name = "productCode";

    public static final String Quantity = "quantity";

    public static final String P_Brand_Table = "P_Brand";
    public static final String PB_ID = "pb_Id";

    public static final String PB_ProductCode_Table = "PB_ProductCode";
    public static final String PB_PC_ID = "pb_pc_Id";

    public static final String PB_Gender_Table = "PB_Gender";
    public static final String PBG_ID = "pbg_Id";

    public static final String PBG_Size_Table = "PBG_Size";
    public static final String PBGS_ID = "pbgs_Id";

    public static final String PBGS_Type_Table = "PBGS_Type";
    public static final String PBGST_ID = "pbgst_Id";

    public static final String PBGST_Color_Table = "PBGST_Color";
    public static final String PBGSTC_ID = "pbgstc_Id";

    public static final String Inventory_Table = "Inventory";
    public static final String Inventory_ID = "inventory_id";

    public static final String BookKeeping_Table = "Bookkeeping";
    public static final String BookKeeping_ID = "bookkeeping_id";

    public static final String Credit_Table = "Credit";
    public static final String Credit_ID = "credit_id";

    public static final String Detail_Table = "Detail";
    public static final String Detail_ID = "detail_Id";
    public static final String D_Product_ID = "product_Id";
    public static final String D_Brand_ID = "brand_Id";
    public static final String D_Type_ID = "type_Id";
    public static final String D_Size_ID = "size_Id";
    public static final String Cost_Price = "costPrice";
    public static final String Sold_Price = "soldPrice";
    public static final String Product_Code = "productCode";
    public static final String Purchase_Date = "purchase_Date";
    public static final String Sell_Date = "sell_Date";
    public static final String Profit = "profit";
    public static final String Vat_Percent = "vatPercent";
    public static final String D_Creditor_ID = "creditor_Id";

    public static final String Creditor_Table = "Creditor";
    public static final String Creditor_ID = "creditor_Id";
    public static final String Creditor_First_Name = "firstName";
    public static final String Creditor_Last_Name = "lastName";
    public static final String Creditor_Phone = "phoneNumber";

    /**
    public static final String Relationship_Table = "Relationship";
    public static final String R_Product_ID = "product_Id";
    public static final String R_Brand_ID = "brand_Id";
    public static final String R_Type_ID = "type_Id";
    public static final String R_size_ID = "size_Id";
    public static final String R_detail_ID = "detail_Id";
     **/

    private static final String TAG = DBHelper.class.getSimpleName();
    //Storing the detail Id value
    public Long detailID;

    public DBHelper(Context context) {
        super(context, Database_Name, null, Database_Version);
        SQLiteDatabase db = this.getWritableDatabase();
    }
    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String productTableQuery = "CREATE TABLE "+Product_Table + "("
                + Product_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Product_Name + " TEXT NOT NULL,"
                + Quantity + " INTEGER,"
                + " CONSTRAINT product_name_unique UNIQUE ("+Product_Name+")"+")";

        String brandTableQuery = "CREATE TABLE "+Brand_Table + "("
                + Brand_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Brand_Name + " TEXT NOT NULL,"
                + Quantity + " INTEGER,"
                + "CONSTRAINT brand_name_unique UNIQUE ("+Brand_Name+")" +")";

        String productCodeTableQuery = "CREATE TABLE "+ProductCode_Table + "("
                + ProductCode_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + ProductCode_Name + " TEXT NOT NULL,"
                + Quantity + " INTEGER NOT NULL,"
                + " CONSTRAINT productCode_name_unique UNIQUE ("+ProductCode_Name+")"+")";

        String genderTableQuery = "CREATE TABLE "+Gender_Table + "("
                + Gender_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Gender_Name + " TEXT NOT NULL,"
                + Quantity + " INTEGER NOT NULL,"
                + " CONSTRAINT gender_name_unique UNIQUE ("+Gender_Name+")"+")";

        String typeTableQuery = "CREATE TABLE "+Type_Table + "("
                + Type_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Type_Name + " TEXT NOT NULL,"
                + Quantity + " INTEGER,"
                + " CONSTRAINT type_name_unique UNIQUE ("+Type_Name+")"+")";

        String sizeTableQuery = "CREATE TABLE "+Size_Table + "("
                + Size_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Size_Name + " TEXT NOT NULL,"
                + Quantity + " INTEGER,"
                + " CONSTRAINT size_name_unique UNIQUE ("+Size_Name+")"+")";

        String colorTableQuery = "CREATE TABLE "+Color_Table + "("
                + Color_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Color_Name + " TEXT NOT NULL,"
                + Quantity + " INTEGER NOT NULL,"
                + " CONSTRAINT color_name_unique UNIQUE ("+Color_Name+")"+")";

        String productBrandTableQuery = "CREATE TABLE "+P_Brand_Table + "("
                + PB_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Product_ID + " INTEGER NOT NULL, "
                + Brand_ID + " INTEGER NOT NULL, "
                + Quantity + " INTEGER NOT NULL, "
                +" UNIQUE (" + Product_ID +", "+ Brand_ID +")"+")";

        String productBrandProductCodeTableQuery = "CREATE TABLE "+PB_ProductCode_Table + "("
                + PB_PC_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + PB_ID + " INTEGER NOT NULL, "
                + ProductCode_ID + " INTEGER NOT NULL, "
                + Quantity + " INTEGER NOT NULL, "
                +" UNIQUE (" + PB_ID +", "+ ProductCode_ID +")"+")";

        String productBrandGenderTableQuery = "CREATE TABLE "+PB_Gender_Table + "("
                + PBG_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + PB_ID + " INTEGER NOT NULL, "
                + Gender_ID + " INTEGER NOT NULL, "
                + Quantity + " INTEGER NOT NULL, "
                +" UNIQUE (" + PB_ID +", "+ Gender_ID +")"+")";

        String productBrandGenderSizeTableQuery = "CREATE TABLE "+PBG_Size_Table + "("
                + PBGS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + PBG_ID + " INTEGER NOT NULL, "
                + Size_ID + " INTEGER NOT NULL, "
                + Quantity + " INTEGER NOT NULL, "
                +" UNIQUE (" + PBG_ID +", "+ Size_ID +")"+")";

        String productBrandGenderSizeTypeTableQuery = "CREATE TABLE "+PBGS_Type_Table + "("
                + PBGST_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + PBGS_ID + " INTEGER NOT NULL, "
                + Type_ID + " INTEGER NOT NULL, "
                + Quantity + " INTEGER NOT NULL, "
                +" UNIQUE (" + PBGS_ID +", "+ Type_ID +")"+")";

        String productBrandGenderSizeTypeColorTableQuery = "CREATE TABLE "+PBGST_Color_Table + "("
                + PBGSTC_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + PBGST_ID + " INTEGER NOT NULL, "
                + Color_ID + " INTEGER NOT NULL, "
                + Quantity + " INTEGER NOT NULL, "
                +" UNIQUE (" + PBGST_ID +", "+ Color_ID +")"+")";

        String creditorTableQuery = "CREATE TABLE "+Creditor_Table + "("
                + Creditor_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Creditor_First_Name + " TEXT,"
                + Creditor_Last_Name + " TEXT,"
                + Creditor_Phone + " TEXT " + ")";
        //public static final String Vat_Percent = "vatPercent";
        //public static final String Credit = "credit";

        String inventoryTableQuery = "CREATE TABLE "+Inventory_Table + "("
                + Inventory_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Product_ID + " INTEGER NOT NULL, "
                + PB_ID + " INTEGER NOT NULL, "
                + ProductCode_ID + " INTEGER NOT NULL, "
                + PBG_ID + " INTEGER NOT NULL, "
                + PBGS_ID + " INTEGER NOT NULL, "
                + PBGST_ID + " INTEGER NOT NULL, "
                + PBGSTC_ID + " INTEGER NOT NULL, "
                + Product_Code + " TEXT, "
                + Cost_Price + " REAL, "
                + Purchase_Date +" TEXT" +")";

        String bookkeepingTableQuery = "CREATE TABLE "+BookKeeping_Table + "("
                + BookKeeping_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Product_ID + " INTEGER NOT NULL, "
                + PB_ID + " INTEGER NOT NULL, "
                + ProductCode_ID + " INTEGER NOT NULL, "
                + PBG_ID + " INTEGER NOT NULL, "
                + PBGS_ID + " INTEGER NOT NULL, "
                + PBGST_ID + " INTEGER NOT NULL, "
                + PBGSTC_ID + " INTEGER NOT NULL, "
                + Product_Code + " TEXT, "
                + Cost_Price + " REAL, "
                + Sold_Price + " REAL, "
                + Purchase_Date +" TEXT, "
                + Sell_Date + " TEXT, "
                + Profit + " REAL, "
                + Vat_Percent + " TEXT" +")";

        String creditTableQuery = "CREATE TABLE "+Credit_Table + "("
                + Credit_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Creditor_ID + " INTEGER NOT NULL, "
                + Product_ID + " INTEGER NOT NULL, "
                + PB_ID + " INTEGER NOT NULL, "
                + ProductCode_ID + " INTEGER NOT NULL, "
                + PBG_ID + " INTEGER NOT NULL, "
                + PBGS_ID + " INTEGER NOT NULL, "
                + PBGST_ID + " INTEGER NOT NULL, "
                + PBGSTC_ID + " INTEGER NOT NULL, "
                + Product_Code + " TEXT, "
                + Cost_Price + " REAL, "
                + Sold_Price + " REAL, "
                + Purchase_Date +" TEXT, "
                + Sell_Date + " TEXT, "
                + Profit + " REAL, "
                + Vat_Percent + " TEXT" +")";

        String detailsTableQuery = "CREATE TABLE "+Detail_Table + "("
                + Detail_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + D_Product_ID + " INTEGER NOT NULL, "
                + D_Brand_ID + " INTEGER NOT NULL, "
                + D_Size_ID + " INTEGER NOT NULL, "
                + D_Type_ID + " INTEGER NOT NULL, "
                + Product_Code + " TEXT, "
                + Sold_Price + " REAL, "
                + Cost_Price + " REAL, "
                + Sell_Date + " TEXT, "
                + Purchase_Date +" TEXT, "
                + Profit + " REAL, "
                + Vat_Percent + " TEXT, "
                + D_Creditor_ID + " INTEGER, "
                +"FOREIGN KEY("+Product_ID+") REFERENCES "+Product_Table+"("+Product_ID+") ON DELETE CASCADE ON UPDATE CASCADE, "
                +"FOREIGN KEY("+Brand_ID+") REFERENCES "+Brand_Table+"("+Brand_ID+") ON DELETE CASCADE ON UPDATE CASCADE, "
                +"FOREIGN KEY("+Type_ID+") REFERENCES "+Type_Table+"("+Type_ID+") ON DELETE CASCADE ON UPDATE CASCADE, "
                +"FOREIGN KEY("+Size_ID+") REFERENCES "+Size_Table+"("+Size_ID+") ON DELETE CASCADE ON UPDATE CASCADE"
                +")";
        /**
        String relationshipTableQuery = "CREATE TABLE "+Relationship_Table + "("
                + R_Product_ID + " INTEGER NOT NULL, "
                + R_Brand_ID + " INTEGER NOT NULL, "
                + R_size_ID+" INTEGER NOT NULL, "
                + R_Type_ID+" INTEGER NOT NULL, "
                +R_detail_ID+" INTEGER NOT NULL, "
                +"FOREIGN KEY("+Product_ID+") REFERENCES "+Product_Table+"("+Product_ID+") ON DELETE CASCADE ON UPDATE CASCADE, "
                +"FOREIGN KEY("+Brand_ID+") REFERENCES "+Brand_Table+"("+Brand_ID+") ON DELETE CASCADE ON UPDATE CASCADE, "
                +"FOREIGN KEY("+Type_ID+") REFERENCES "+Type_Table+"("+Type_ID+") ON DELETE CASCADE ON UPDATE CASCADE, "
                +"FOREIGN KEY("+Size_ID+") REFERENCES "+Size_Table+"("+Size_ID+") ON DELETE CASCADE ON UPDATE CASCADE, "
                +"FOREIGN KEY("+Detail_ID+") REFERENCES "+Detail_Table+"("+Detail_ID+") ON DELETE CASCADE ON UPDATE CASCADE"
                + ")";
         **/

        sqLiteDatabase.execSQL(productTableQuery);
        sqLiteDatabase.execSQL(brandTableQuery);
        sqLiteDatabase.execSQL(productCodeTableQuery);
        sqLiteDatabase.execSQL(genderTableQuery);
        sqLiteDatabase.execSQL(typeTableQuery);
        sqLiteDatabase.execSQL(sizeTableQuery);
        sqLiteDatabase.execSQL(colorTableQuery);

        sqLiteDatabase.execSQL(detailsTableQuery);

        sqLiteDatabase.execSQL(inventoryTableQuery);
        sqLiteDatabase.execSQL(bookkeepingTableQuery);
        sqLiteDatabase.execSQL(creditTableQuery);
        sqLiteDatabase.execSQL(creditorTableQuery);

        sqLiteDatabase.execSQL(productBrandTableQuery);
        sqLiteDatabase.execSQL(productBrandProductCodeTableQuery);
        sqLiteDatabase.execSQL(productBrandGenderTableQuery);
        sqLiteDatabase.execSQL(productBrandGenderSizeTableQuery);
        sqLiteDatabase.execSQL(productBrandGenderSizeTypeTableQuery);
        sqLiteDatabase.execSQL(productBrandGenderSizeTypeColorTableQuery);
        //sqLiteDatabase.execSQL(relationshipTableQuery);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("Drop Product IF EXISTS" + Product_Table);
        sqLiteDatabase.execSQL("Drop Brand IF EXISTS" + Brand_Table);
        sqLiteDatabase.execSQL("Drop ProductCode IF EXISTS" + ProductCode_Table);
        sqLiteDatabase.execSQL("Drop Gender IF EXISTS" + Gender_Table);
        sqLiteDatabase.execSQL("Drop Type IF EXISTS" + Type_Table);
        sqLiteDatabase.execSQL("Drop Size IF EXISTS" + Size_Table);
        sqLiteDatabase.execSQL("Drop Color IF EXISTS" + Color_Table);

        sqLiteDatabase.execSQL("Drop Detail IF EXISTS" + Detail_Table);

        sqLiteDatabase.execSQL("Drop Inventory IF EXISTS" + Inventory_Table);
        sqLiteDatabase.execSQL("Drop Bookkeeping IF EXISTS" + BookKeeping_Table);
        sqLiteDatabase.execSQL("Drop Credit IF EXISTS" + Credit_Table);
        sqLiteDatabase.execSQL("Drop Creditor IF EXISTS" + Creditor_Table);

        sqLiteDatabase.execSQL("Drop P_Brand IF EXISTS" + P_Brand_Table);
        sqLiteDatabase.execSQL("Drop PB_ProductCode IF EXISTS" + PB_ProductCode_Table);
        sqLiteDatabase.execSQL("Drop PB_Gender IF EXISTS" + PB_Gender_Table);
        sqLiteDatabase.execSQL("Drop PBG_Size IF EXISTS" + PBG_Size_Table);
        sqLiteDatabase.execSQL("Drop PBGS_Type IF EXISTS" + PBGS_Type_Table);
        sqLiteDatabase.execSQL("Drop PBGST_Color IF EXISTS" + PBGST_Color_Table);
        //sqLiteDatabase.execSQL("Drop SizeDetails IF EXISTS" + Relationship_Table);
        onCreate(sqLiteDatabase);
    }


}
