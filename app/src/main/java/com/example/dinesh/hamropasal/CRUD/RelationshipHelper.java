package com.example.dinesh.hamropasal.CRUD;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.dinesh.hamropasal.dbModel.Relationships;

import java.util.ArrayList;


/**
 * Created by dinesh on 16/07/2016.
 */
public class RelationshipHelper extends DBHelper {
    public RelationshipHelper(Context context) {
        super(context);
    }
    /**
    public long relation_insert(Relationships relation) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(R_Product_ID,relation.get_Product_Id());
        contentValues.put(R_Brand_ID,relation.get_Brand_Id());
        contentValues.put(R_size_ID,relation.get_Size_Id());
        contentValues.put(R_Type_ID,relation.get_Type_Id());
        //GlobalVariables myGV = ((GlobalVariables)getApplicationContext());
        contentValues.put(R_detail_ID,relation.get_Detail_Id());
        long result = db.insert(Relationship_Table,null,contentValues);
        db.close();
        return result;
    }


    //Get all the details of product
    public ArrayList<Relationships> getAllProductsDetails(){
        ArrayList<Relationships> arrayList = new ArrayList<Relationships>();
        SQLiteDatabase db = this.getReadableDatabase();
        Relationships reln;
        Cursor cur = db.rawQuery("select * from "+Relationship_Table, null);
        cur.moveToFirst();

        while (cur.isAfterLast() == false) {
            reln= new Relationships();

            reln.set_Product_Id(Integer.parseInt(cur.getString(0)));
            reln.set_Brand_Id(Integer.parseInt(cur.getString(1)));
            reln.set_Size_Id(Integer.parseInt(cur.getString(2)));
            reln.set_Type_Id(Integer.parseInt(cur.getString(3)));
            reln.set_Detail_Id(Integer.parseInt(cur.getString(4)));
            arrayList.add(reln);
            cur.moveToNext();
        }
        db.close();
        return arrayList;
    }
     **/
}
