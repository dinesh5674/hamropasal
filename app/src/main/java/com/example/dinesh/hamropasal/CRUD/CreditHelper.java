package com.example.dinesh.hamropasal.CRUD;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.dinesh.hamropasal.dbModel.Credits;

import java.util.ArrayList;

/**
 * Created by subash on 26/08/2016.
 */
public class CreditHelper extends DBHelper {

    public CreditHelper(Context context){
        super(context);
    }

    public long creditInsert(Credits credit){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Creditor_First_Name,credit.get_Firstname());
        contentValues.put(Creditor_Last_Name,credit.get_Lastname());
        contentValues.put(Creditor_Phone,credit.get_Phonenumber());
        long result = db.insert(Creditor_Table,null,contentValues);
        return result;
    }

    public boolean creditUpdate(Credits credit){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Creditor_ID,credit.get_Creditor_ID());
        contentValues.put(Creditor_First_Name,credit.get_Firstname());
        contentValues.put(Creditor_Last_Name,credit.get_Lastname());
        contentValues.put(Creditor_Phone,credit.get_Phonenumber());

        int result = db.update(Creditor_Table,contentValues,"Creditor_Id = ?", new String [] { String.valueOf(credit.get_Creditor_ID())});
        db.close();
        if (result == 0) {
            return false;
        }
        else {
            return true;
        }
    }

    //@Override
    public boolean creditDelete(Credits credit) {
        SQLiteDatabase db = this.getWritableDatabase();
        int result=  db.delete(Creditor_Table,"Creditor_Id = ?", new String [] {String.valueOf(credit.get_Creditor_ID())});
        db.close();
        if(result == 0) {
            return false;
        }
        else {
            return true;
        }
    }

    public ArrayList<Credits> getAllCredits() {
        ArrayList<Credits> arrayList = new ArrayList<Credits>();
        SQLiteDatabase db = this.getReadableDatabase();
        Credits cre;
        Cursor cur = db.rawQuery("select * from "+Creditor_Table, null);
        cur.moveToFirst();

        while (cur.isAfterLast() == false) {
            cre= new Credits();
            cre.set_Creditor_ID(Integer.parseInt(cur.getString(0)));
            cre.set_Firstname(cur.getString(1));
            cre.set_Lastname(cur.getString(2));
            cre.set_Phonenumber(cur.getString(3));

            arrayList.add(cre);
            cur.moveToNext();
        }
        cur.close();
        db.close();
        return arrayList;

    }
}

