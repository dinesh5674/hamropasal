package com.example.dinesh.hamropasal.Custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.dinesh.hamropasal.R;
import com.example.dinesh.hamropasal.dbModel.Brands;
import com.example.dinesh.hamropasal.dbModel.Details;
import com.example.dinesh.hamropasal.dbModel.PBST_Name;
import com.example.dinesh.hamropasal.dbModel.Products;
import com.example.dinesh.hamropasal.dbModel.Sizes;
import com.example.dinesh.hamropasal.dbModel.Types;

import java.util.ArrayList;

/**
 * Created by dinesh on 31/08/2016.
 */
public class ProductCodeAdapter extends ArrayAdapter<PBST_Name> {
    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<PBST_Name> proCodeObjects;

    CustomSelector cusSelect;


    public ProductCodeAdapter(Context context, int textViewResourceId, ArrayList <PBST_Name> proCodeObjects){
        super(context, textViewResourceId, proCodeObjects);
        this.context = context;
        this.proCodeObjects = proCodeObjects;

    }
    @Override
    public int getCount() {
        return proCodeObjects.size();
    }

    @Override
    public PBST_Name getItem(int position) {
        return proCodeObjects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        //View grid;
        // Get the data item for this position
        cusSelect = new CustomSelector(context);
        PBST_Name proCodeObj = proCodeObjects.get(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.product_code_adapter, viewGroup, false);
        }

        if(proCodeObj != null) {

            TextView product_name= (TextView)convertView.findViewById(R.id.product_name);
            TextView brand_name = (TextView)convertView.findViewById(R.id.brand_name);
            TextView type_name = (TextView)convertView.findViewById(R.id.type_name);
            TextView size_name = (TextView)convertView.findViewById(R.id.size_name);
            TextView costPrice = (TextView)convertView.findViewById(R.id.costPrice);
            TextView purchaseDate = (TextView)convertView.findViewById(R.id.purchaseDate);


            product_name.setText(proCodeObj.getProduct_Name());
            brand_name.setText(proCodeObj.getBrand_Name());
            type_name.setText(proCodeObj.getType_Name());
            size_name.setText(proCodeObj.getSize_Name());
            costPrice.setText(proCodeObj.get_Cost_Price());
            purchaseDate.setText(proCodeObj.get_Purchase_DateTime());

        }

        return convertView;
    }
}
