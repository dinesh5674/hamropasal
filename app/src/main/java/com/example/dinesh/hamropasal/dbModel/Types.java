package com.example.dinesh.hamropasal.dbModel;



public class Types {

    private int _Type_Id;
    private String _Type_Name;
    private int _Quantity;

    public Types() {

    }

    public Types(String type_name) {
        this._Type_Name = type_name;
    }

    public void set_Type_Id(int _Type_Id) {
        this._Type_Id = _Type_Id;
    }

    public void set_Type_Name(String _Type_Name) {
        this._Type_Name = _Type_Name;
    }
    public void set_Quantity(int _Quantity){
        this._Quantity = _Quantity;
    }
    public int get_Quantity(){
        return _Quantity;
    }

    public int get_Type_Id() {
        return _Type_Id;
    }

    public String get_Type_Name() {
        return _Type_Name;
    }

    @Override
    public String toString() {
        return this._Type_Name ;
    }
}
