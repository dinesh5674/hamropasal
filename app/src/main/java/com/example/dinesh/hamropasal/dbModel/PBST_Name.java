package com.example.dinesh.hamropasal.dbModel;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by dinesh on 8/24/2016.
 */
public class PBST_Name implements Parcelable {

    private String Product_Name;
    private String Brand_Name;
    private String Type_Name;
    private String Size_Name;
    private String Cost_Price;
    private String Purchase_Date;
    private String Product_Code;
    private String Sold_Price;
    private String Sold_Date;
    private int Detail_Id;
    private int Creditor_Id;
    private int count;

    public PBST_Name() {
    }
    protected PBST_Name(Parcel in) {
        Detail_Id = in.readInt();
        Product_Name = in.readString();
        Brand_Name = in.readString();
        Size_Name = in.readString();
        Type_Name = in.readString();
        Cost_Price = in.readString();
        Purchase_Date = in.readString();
        Product_Code = in.readString();
    }
    public static final Creator<PBST_Name> CREATOR = new Creator<PBST_Name>() {
        @Override
        public PBST_Name createFromParcel(Parcel in) {
            return new PBST_Name(in);
        }

        @Override
        public PBST_Name[] newArray(int size) {
            return new PBST_Name[size];
        }
    };

    public PBST_Name(String product_Name, String brand_Name, String type_Name, String size_Name) {
        Product_Name = product_Name;
        Brand_Name = brand_Name;
        Type_Name = type_Name;
        Size_Name = size_Name;
    }

    public String getProduct_Name() {
        return Product_Name;
    }

    public void setProduct_Name(String product_Name) {
        Product_Name = product_Name;
    }

    public String getBrand_Name() {
        return Brand_Name;
    }

    public void setBrand_Name(String brand_Name) {
        Brand_Name = brand_Name;
    }

    public String getType_Name() {
        return Type_Name;
    }

    public void setType_Name(String type_Name) {
        Type_Name = type_Name;
    }

    public void set_Sold_Price(String _Sold_Price){
        this.Sold_Price = _Sold_Price;
    }

    public String get_Sold_Price(){
        return Sold_Price;
    }

    public void set_Sold_Date(String _Sold_Date){
        this.Sold_Date = _Sold_Date;
    }

    public String get_Sold_Date(){
        return Sold_Date;
    }

    public String getSize_Name() {
        return Size_Name;
    }

    public void setSize_Name(String size_Name) {
        Size_Name = size_Name;
    }
    public void set_Cost_Price(String _Cost_Price) {
        this.Cost_Price = _Cost_Price;
    }
    public void set_Purchase_DateTime(String _Purchase_DateTime) {
        this.Purchase_Date = _Purchase_DateTime;
    }
    public void set_Product_Code(String  _Product_Code) {
        this.Product_Code = _Product_Code;
    }
    public String get_Cost_Price() {
        return Cost_Price;
    }
    public String get_Purchase_DateTime() {
        return Purchase_Date;
    }
    public String  get_Product_Code() {
        return Product_Code;
    }
    public void set_Detail_Id(int _Detail_Id) {
        this.Detail_Id = _Detail_Id;
    }

    public void set_Creditor_Id(int _Creditor_Id){
        this.Creditor_Id = _Creditor_Id;
    }

    public int get_Creditor_Id(){
        return Creditor_Id;
    }

    public int get_Detail_Id() {
        return Detail_Id;
    }
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return this.Product_Name+ " "+ this.Brand_Name+ " " + this.Type_Name+ " "+ this.Size_Name+ " ";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeInt(Detail_Id);
        parcel.writeString(Product_Name);
        parcel.writeString(Brand_Name);
        parcel.writeString(Size_Name);
        parcel.writeString(Type_Name);
        parcel.writeString(Cost_Price);
        parcel.writeString(Purchase_Date);
        parcel.writeString(Product_Code);
    }
}
