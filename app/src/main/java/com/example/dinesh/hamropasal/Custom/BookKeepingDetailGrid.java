package com.example.dinesh.hamropasal.Custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.dinesh.hamropasal.R;
import com.example.dinesh.hamropasal.dbModel.Details;

import java.util.ArrayList;

/**
 * Created by ANONYMOUS_PAL on 6.9.2016.
 */
public class BookKeepingDetailGrid extends ArrayAdapter <Details> {

    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<Details> detailArray;


    public BookKeepingDetailGrid(Context context, int textViewResourceId, ArrayList<Details> detailArray) {
        super(context, textViewResourceId, detailArray);
        this.context = context;
        this.detailArray = detailArray;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public Details getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       Details detail_obj=detailArray.get(position);

            if (convertView==null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.bookkeeping_detail, parent, false);
            }

            if (detail_obj !=null){

                TextView costPrice = (TextView)convertView.findViewById(R.id.cost_price);
                TextView soldPrice = (TextView)convertView.findViewById(R.id.sold_price);
                TextView soldDate = (TextView)convertView.findViewById(R.id.sold_date);
                TextView profit = (TextView)convertView.findViewById(R.id.profit);

                costPrice.setText(detail_obj.get_Cost_Price());
                soldPrice.setText(detail_obj.get_Sold_Price());
                soldDate.setText(detail_obj.get_Sold_DateTime());
                profit.setText(detail_obj.get_Profit());


            }
        return convertView;
    }
}
