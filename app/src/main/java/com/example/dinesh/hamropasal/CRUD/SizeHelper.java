package com.example.dinesh.hamropasal.CRUD;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.dinesh.hamropasal.dbModel.Sizes;

import java.util.ArrayList;

/**
 * Created by ANONYMOUS_PAL on 7.7.2016.
 */
public class SizeHelper extends DBHelper {

    public SizeHelper(Context context) {
        super(context);
    }

    //@Override
    public long sizeInsert(Sizes size) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Size_Name,size.get_Size_Name());
        contentValues.put(Quantity,size.get_Quantity());
        long result = db.insert(Size_Table,null,contentValues);
        db.close();
        return result;
    }

    //@Override
    public long sizeUpdate(Sizes size) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Size_ID,size.get_Size_Id());
        contentValues.put(Size_Name,size.get_Size_Name());
        contentValues.put(Quantity,size.get_Quantity());
        int result = db.update(Size_Table,contentValues, "size_Id = ?", new String [] { String.valueOf(size.get_Size_Id())});
        db.close();
        return result;
    }

    public int sizeDelete(Sizes size) {
        SQLiteDatabase db = this.getWritableDatabase();
        int result=  db.delete(Size_Table,"size_Id = ?", new String [] {String.valueOf(size.get_Size_Id())});
        db.close();
        return result;
    }

    public ArrayList<Sizes> getAllSizes() {
        ArrayList<Sizes> arrayList = new ArrayList<Sizes>();
        SQLiteDatabase db = this.getReadableDatabase();
        Sizes siz;
        Cursor cur = db.rawQuery("select * from "+Size_Table, null);
        cur.moveToFirst();

        while (cur.isAfterLast() == false) {
            siz= new Sizes();
            siz.set_Size_Id(Integer.parseInt(cur.getString(0)));
            siz.set_Size_Name(cur.getString(1));
            siz.set_Quantity(Integer.parseInt(cur.getString(2)));
            arrayList.add(siz);
            cur.moveToNext();
        }
        cur.close();
        db.close();
        return arrayList;

    }

}
