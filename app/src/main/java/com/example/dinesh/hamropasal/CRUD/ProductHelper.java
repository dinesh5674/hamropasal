package com.example.dinesh.hamropasal.CRUD;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.dinesh.hamropasal.dbModel.Products;
import java.util.ArrayList;



/**
 * Created by ANONYMOUS_PAL on 7.7.2016.
 */
public class ProductHelper extends DBHelper {



    public ProductHelper(Context context) {
        super(context);
    }


   public long productInsert(Products product) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Product_Name, product.get_Product_Name());
        contentValues.put(Quantity, product.get_Quantity());
        long result = db.insert(Product_Table,null,contentValues);
        db.close();
        return result;
    }


    public long productUpdate(Products product) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Product_ID, product.get_Product_Id());
        contentValues.put(Product_Name,product.get_Product_Name());
        contentValues.put(Quantity, product.get_Quantity());
        long result = db.update(Product_Table,contentValues, "product_Id = ?", new String[] {String.valueOf(product.get_Product_Id())});
        db.close();
        return result;
    }

    public int productDelete(Products product) {
        SQLiteDatabase db = this.getWritableDatabase();
        int result=  db.delete(Product_Table,"product_Id = ?", new String [] {String.valueOf(product.get_Product_Id())});
        db.close();
        return result;
    }

    public ArrayList<Products> getAllProducts() {
        ArrayList<Products> arrayList = new ArrayList<Products>();
        SQLiteDatabase db = this.getReadableDatabase();
        Products pro;
        Cursor cur = db.rawQuery("select * from "+Product_Table, null);
        cur.moveToFirst();

        while (cur.isAfterLast() == false) {
            pro= new Products();
            pro.set_Product_Id(Integer.parseInt(cur.getString(0)));
            pro.set_Product_Name(cur.getString(1));
            pro.set_Quantity(Integer.parseInt(cur.getString(2)));
            arrayList.add(pro);
            cur.moveToNext();
        }
        cur.close();
        db.close();
        return arrayList;

    }


}
