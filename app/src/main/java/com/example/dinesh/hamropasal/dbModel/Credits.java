
package com.example.dinesh.hamropasal.dbModel;

/**
 * Created by subash on 26/08/2016.
 */
public class Credits {

    private int _Creditor_ID;
    private String _Firstname;
    private String _Lastname;
    private String _Phonenumber;

    public Credits() {
    }

    public void set_Creditor_ID(int _Creditor_ID){
        this._Creditor_ID = _Creditor_ID;
    }
    public void set_Firstname(String _Firstname){this._Firstname = _Firstname;}
    public void set_Lastname(String _Lastname){this._Lastname = _Lastname;}
    public void set_Phonenumber(String _Phonenumber){this._Phonenumber =_Phonenumber;}

    public String get_Firstname(){
        return _Firstname;
    }

    public String get_Lastname(){
        return _Lastname;
    }

    public String get_Phonenumber(){
        return _Phonenumber;
    }

    public int get_Creditor_ID(){
        return _Creditor_ID;
    }

    @Override
    public String toString() {
        return this._Creditor_ID + " " + this._Firstname + " " + this._Lastname + " " + this._Phonenumber + " ";
    }
}


