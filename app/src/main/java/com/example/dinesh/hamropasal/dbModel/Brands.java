package com.example.dinesh.hamropasal.dbModel;

public class Brands {

    private int _Brand_Id;
    private String _Brand_Name;
    private int _Quantity;

    public Brands() {
    }

    public Brands(String brand_name) {
        this._Brand_Name = brand_name;
    }

    public void set_Brand_Id(int _Brand_Id) {
        this._Brand_Id = _Brand_Id;
    }

    public void set_Brand_Name(String _Brand_Name) {
        this._Brand_Name = _Brand_Name;
    }

    public void set_Quantity(int _Quantity){
        this._Quantity = _Quantity;
    }
    public int get_Quantity(){
        return _Quantity;
    }

    public int get_Brand_Id() {
        return _Brand_Id;
    }

    public String get_Brand_Name() {
        return _Brand_Name;
    }

    @Override
    public String toString() {
        return this._Brand_Name ;
    }
}
