package com.example.dinesh.hamropasal.dbModel;

/**
 * Created by dinesh on 9/1/2016.
 */
public class SalesAndProfit {
    String _Month_Name;
    String _Monthly_Sales;
    String _Monthly_Profit;

    public SalesAndProfit() {
    }

    public SalesAndProfit(String _Monthly_Sales, String _Monthly_Profit) {
        this._Monthly_Sales = _Monthly_Sales;
        this._Monthly_Profit = _Monthly_Profit;
    }

    public String get_Month_Name() {
        return _Month_Name;
    }

    public void set_Month_Name(String _Month_Name) {
        this._Month_Name = _Month_Name;
    }

    public String get_Monthly_Sales() {
        return _Monthly_Sales;
    }

    public void set_Monthly_Sales(String _Monthly_Sales) {
        this._Monthly_Sales = _Monthly_Sales;
    }

    public String get_Monthly_Profit() {
        return _Monthly_Profit;
    }

    public void set_Monthly_Profit(String _Monthly_Profit) {
        this._Monthly_Profit = _Monthly_Profit;
    }

    @Override
    public String toString() {
        return    this._Month_Name + " "+ this._Monthly_Sales + " " + this._Monthly_Profit;
    }
}
