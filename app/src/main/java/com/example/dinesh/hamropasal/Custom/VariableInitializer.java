package com.example.dinesh.hamropasal.Custom;

import android.content.Context;

import com.example.dinesh.hamropasal.CRUD.BrandHelper;
import com.example.dinesh.hamropasal.CRUD.DBHelper;
import com.example.dinesh.hamropasal.CRUD.ProductHelper;
import com.example.dinesh.hamropasal.CRUD.SizeHelper;
import com.example.dinesh.hamropasal.CRUD.TypeHelper;
import com.example.dinesh.hamropasal.dbModel.Brands;
import com.example.dinesh.hamropasal.dbModel.Products;
import com.example.dinesh.hamropasal.dbModel.Sizes;
import com.example.dinesh.hamropasal.dbModel.Types;

import java.util.ArrayList;

/**
 * Created by dinesh on 16/11/2016.
 */

public class VariableInitializer {

    public DBHelper myDB;
    public ProductHelper myProduct;
    public BrandHelper myBrand;
    public TypeHelper myType;
    public SizeHelper mySize;

    public ArrayList<Products> productArray;
    public ArrayList<Brands> brandArray;
    public ArrayList<Sizes> sizeArray;
    public ArrayList<Types> typeArray;

    public VariableInitializer(Context context) {
        myDB = new DBHelper(context);
        myProduct = new ProductHelper(context);
        myBrand = new BrandHelper(context);
        myType = new TypeHelper(context);
        mySize = new SizeHelper(context);

        refreshProductArray();
        refreshBrandArray();
        refreshSizeArray();
        refreshTypeArray();
    }
    public void refreshProductArray() {
        productArray = myProduct.getAllProducts();
    }

    public void refreshBrandArray() {
        brandArray = myBrand.getAllBrands();
    }

    public void refreshSizeArray() {
        sizeArray = mySize.getAllSizes();
    }

    public void refreshTypeArray() {
        typeArray = myType.getAllTypes();
    }
    Products pro;
    public Products productExists(String productName){
        for (Products product:productArray) {
            if(product.get_Product_Name().equals(productName)){
                pro = product;
                break;
            }
        }
        return pro;
    }
    Brands bra;
    public Brands brandExists(String brandName){
        for (Brands brand:brandArray) {
            if(brand.get_Brand_Name().equals(brandName)){
                bra = brand;
                break;
            }
        }
        return bra;
    }

    Sizes siz;
    public Sizes sizeExists(String sizeName){
        for (Sizes size:sizeArray) {
            if(size.get_Size_Name().equals(sizeName)){
                siz = size;
                break;
            }
        }
        return siz;
    }

    Types typ;
    public Types typeExists(String typeName){
        for (Types type:typeArray) {
            if(type.get_Type_Name().equals(typeName)){
                typ = type;
                break;
            }
        }
        return typ;
    }
}

