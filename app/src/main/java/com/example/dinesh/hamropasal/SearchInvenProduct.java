package com.example.dinesh.hamropasal;

import android.content.Intent;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.Toast;

import com.example.dinesh.hamropasal.CRUD.DetailHelper;
import com.example.dinesh.hamropasal.Custom.CustomSelector;
import com.example.dinesh.hamropasal.Custom.GridAdapter;
import com.example.dinesh.hamropasal.Custom.VariableInitializer;
import com.example.dinesh.hamropasal.dbModel.Brands;
import com.example.dinesh.hamropasal.dbModel.Details;
import com.example.dinesh.hamropasal.dbModel.PBST_Name;
import com.example.dinesh.hamropasal.dbModel.Products;
import com.example.dinesh.hamropasal.dbModel.Sizes;
import com.example.dinesh.hamropasal.dbModel.Types;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Niroj on 8/11/2016.
 */
public class SearchInvenProduct  extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private  EditText sak_saman,dher_saman ;
    private GridView myGrid;
    private Button search,sakina_saman,dherai_saman,sabai_saman;
    private DetailHelper myDetail;
    private GridAdapter arrayAdapterDetails;

    private ArrayList<Details> detailArray;
    private ArrayList<Details> selectedDetailsItem;

    private FrameLayout customSelector;
    private FrameLayout productInventoryFrame;
    CustomSelector cusSelect;
    VariableInitializer varInit;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inv_product);
        super.onCreateDrawer();

        cusSelect = new CustomSelector(this);
        customSelector = (FrameLayout)findViewById(R.id.customSelector);
        productInventoryFrame = (FrameLayout)findViewById(R.id.productInventoryFrame);
        customSelector.addView(cusSelect);
        customSelector.setVisibility(View.GONE);
        //get the view of list view in front
        customSelector.bringToFront();
        customSelector.invalidate();

        cusSelect.sizeName.setVisibility(View.INVISIBLE);
        cusSelect.typeName.setVisibility(View.INVISIBLE);


        sak_saman = (EditText) findViewById(R.id.sak_saman);
        dher_saman = (EditText) findViewById(R.id.dher_saman);
        sak_saman.setVisibility(View.GONE);
        dher_saman.setVisibility(View.GONE);

        myGrid = (GridView)findViewById(R.id.gridView);
        search = (Button) findViewById(R.id.searchList);
        sakina_saman = (Button) findViewById(R.id.button);
        dherai_saman = (Button) findViewById(R.id.button2);
        sabai_saman = (Button) findViewById(R.id.button3);
        search.setVisibility(View.GONE);

        selectedDetailsItem= new ArrayList<>();
        myDetail = new DetailHelper(this);
        detailArray = myDetail.getInventoryDetails();

        inventoryFetch();

        sakina_saman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sakina_saman.getBackground().setColorFilter(new LightingColorFilter(0x00FFFF , 0x00FFFF));//after press color
                dherai_saman.getBackground().setColorFilter(new LightingColorFilter(0x004F81, 0x004F81));
                sabai_saman.getBackground().setColorFilter(new LightingColorFilter(0x004F81, 0x004F81));

                search.setVisibility(View.VISIBLE);
                sak_saman.setVisibility(View.VISIBLE);
                dher_saman.setVisibility(View.GONE);
                customSelector.setVisibility(View.GONE);
                getInvenSakina();
                arrayAdapterDetails.clear();
                arrayAdapterDetails.notifyDataSetChanged();
            }
        });

        dherai_saman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dherai_saman.getBackground().setColorFilter(new LightingColorFilter(0x00FFFF , 0x00FFFF));//after press color
                sakina_saman.getBackground().setColorFilter(new LightingColorFilter(0x004F81, 0x004F81));
                sabai_saman.getBackground().setColorFilter(new LightingColorFilter(0x004F81, 0x004F81));

                search.setVisibility(View.VISIBLE);
                dher_saman.setVisibility(View.VISIBLE);
                sak_saman.setVisibility(View.GONE);
                customSelector.setVisibility(View.GONE);
                getInvenDherai();
                arrayAdapterDetails.clear();
                arrayAdapterDetails.notifyDataSetChanged();
            }
        });

        sabai_saman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sabai_saman.getBackground().setColorFilter(new LightingColorFilter(0x00FFFF , 0x00FFFF));//after press color
                sakina_saman.getBackground().setColorFilter(new LightingColorFilter(0x004F81, 0x004F81));
                dherai_saman.getBackground().setColorFilter(new LightingColorFilter(0x004F81, 0x004F81));
                search.setVisibility(View.VISIBLE);
                dher_saman.setVisibility(View.GONE);
                sak_saman.setVisibility(View.GONE);
                customSelector.setVisibility(View.VISIBLE);
                getInvenSab();
                arrayAdapterDetails.clear();
                arrayAdapterDetails.notifyDataSetChanged();
            }
        });
    }

    private void getInvenDherai() {
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if ( dher_saman.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), "please enter  value", Toast.LENGTH_SHORT).show();

                }
                else {
                    inventoryFetch();
                }

            }
        });


    }

    private void getInvenSakina() {

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (sak_saman.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), "please enter  value", Toast.LENGTH_SHORT).show();

                }
                else {
                    inventoryFetch();
                }

            }
        });
    }


    private void getInvenSab() {
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cusSelect.productName.getText().toString().trim().length() == 0 &&  cusSelect.brandName.getText().toString().trim().length()== 0){
                    Toast.makeText(getApplicationContext(), "please enter desired product and brand value", Toast.LENGTH_SHORT).show();
                }
                else {
                    inventoryFetch();
                }
            }
        });

    }


    public void inventoryFetch(){

        ArrayList<Details> filtered4Time = new ArrayList<Details>();
        ArrayList<PBST_Name> obj_and_freq = new ArrayList<PBST_Name>();
        final HashMap<PBST_Name, ArrayList<Details>> hashmap4pbst = new HashMap<PBST_Name, ArrayList<Details>>();


        for (Details det_objs:detailArray) {
            if (customSelector.getVisibility()==(View.GONE) && dher_saman.getVisibility()==(View.GONE) && (sak_saman.getVisibility()==(View.GONE)) ) {
                filtered4Time.add(det_objs);

            }
            else if (customSelector.getVisibility()==(View.VISIBLE)) {
                if(det_objs.get_Product_Id()==Integer.parseInt(cusSelect.productID) && det_objs.get_Brand_Id()==Integer.parseInt(cusSelect.brandID)) {
                    filtered4Time.add(det_objs);
                }
            }
            else if  (dher_saman.getVisibility()==(View.VISIBLE) || (sak_saman.getVisibility()==(View.VISIBLE)) ){
                filtered4Time.add(det_objs);

            }
        }
        //final ArrayList<Details> similarObjectList= new ArrayList<>();
        Set<Details> set4Frequency = new HashSet<Details>(filtered4Time);
        for (Details freqvalue : set4Frequency) {
            int count1 = 0;
            System.out.println("FreqValue objects: " + freqvalue);
            PBST_Name pbst_name = new PBST_Name();
            count1 = Collections.frequency(filtered4Time, freqvalue);
            pbst_name.setCount(count1);
            ArrayList<Details> detailArray4hmap = new ArrayList<Details>();


            if  ((dher_saman.getVisibility()==(View.VISIBLE) && (count1>Integer.parseInt(dher_saman.getText().toString())))||(sak_saman.getVisibility()==(View.VISIBLE) && (count1<Integer.parseInt(sak_saman.getText().toString())))||(customSelector.getVisibility()==(View.VISIBLE))
                    ||  (customSelector.getVisibility()==(View.GONE) && dher_saman.getVisibility()==(View.GONE) && (sak_saman.getVisibility()==(View.GONE)) ) ){
                for (int k = 0; k < filtered4Time.size(); k++) {
                    if(freqvalue.equals(filtered4Time.get(k))){
                        detailArray4hmap.add(filtered4Time.get(k));
                        System.out.println("Items in detailArray4hmap____________________________" + detailArray4hmap);
                    }
                }

                for (Products product : varInit.productArray) {
                    if (product.get_Product_Id() == freqvalue.get_Product_Id()) {
                        pbst_name.setProduct_Name(product.get_Product_Name());
                    }
                }

                for (Brands brand : varInit.brandArray) {
                    if (brand.get_Brand_Id() == freqvalue.get_Brand_Id()) {
                        pbst_name.setBrand_Name(brand.get_Brand_Name());
                    }
                }

                for (Types type : varInit.typeArray) {
                    if (type.get_Type_Id() == freqvalue.get_Type_Id()) {
                        pbst_name.setType_Name(type.get_Type_Name());
                    }
                }

                for (Sizes size : varInit.sizeArray) {
                    if (size.get_Size_Id() == freqvalue.get_Size_Id()) {
                        pbst_name.setSize_Name(size.get_Size_Name());
                    }
                }

                obj_and_freq.add(pbst_name);
                hashmap4pbst.put(pbst_name, detailArray4hmap);
            }
        }



        arrayAdapterDetails = new GridAdapter(SearchInvenProduct.this, R.layout.layout4gridview, obj_and_freq);
        myGrid.setAdapter(arrayAdapterDetails);
        arrayAdapterDetails.notifyDataSetChanged();
        myGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                //similarObjectList.clear();
                myGrid.setVisibility(View.GONE);
                PBST_Name selectedFromList =(PBST_Name) (myGrid.getItemAtPosition(i));
                selectedDetailsItem=hashmap4pbst.get(selectedFromList);
                Intent intent = new Intent(SearchInvenProduct.this,CustomGridDetailAdapter.class);
                intent.putParcelableArrayListExtra("ProductInventory", selectedDetailsItem);
                startActivity (intent);
            }
        });
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            Intent myIntent = new Intent(SearchInvenProduct.this, MainActivity.class);
            startActivity(myIntent);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}








