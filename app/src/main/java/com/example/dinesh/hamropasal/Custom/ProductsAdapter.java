package com.example.dinesh.hamropasal.Custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import com.example.dinesh.hamropasal.R;
import com.example.dinesh.hamropasal.dbModel.Products;
import java.util.ArrayList;

/**
 * Created by dinesh on 19/07/2016.
 */
public class ProductsAdapter extends ArrayAdapter<Products> implements Filterable {

    private ArrayList<Products> product_objs;
    private Context mContext;


    CustomFilter mCustomFilter;
    public ProductsAdapter(Context context,  int textViewResourceId, ArrayList<Products> product_objs) {
        super(context, textViewResourceId, product_objs);
        this.product_objs = product_objs;
        this.mContext = context;
    }
    @Override
    public int getCount() {
        return product_objs.size();
    }

    @Override
    public Products getItem(int position) {
        return null;
    }
    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Get the data item for this position
        Products pro = product_objs.get(position);

        // Check if an existing view is being reused, otherwise inflate the view

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_item, parent, false);
        }

        if(pro != null) {
            // Lookup view for data population
            //TextView adapterItem = (TextView) convertView.findViewById(R.id.adapterItem);
            //TextView tvHome = (TextView) convertView.findViewById(R.id.tvHome);
            // Populate the data into the template view using the data object
            //adapterItem.setText(pro.get_Product_Name());

        }

        return convertView;
    }
    //CustomFilter mCustomFilter;
    @Override
    public Filter getFilter() {
        // return a filter that filters data based on a constraint
        if (mCustomFilter == null)

            mCustomFilter = new CustomFilter();

        return mCustomFilter;
    }
    private class CustomFilter extends Filter {


        @Override
        public FilterResults performFiltering(CharSequence constraint) {
            // Create a FilterResults object
            FilterResults results = new FilterResults();

            // If the constraint (search string/pattern) is null
            // or its length is 0, i.e., its empty then
            // we just set the `values` property to the
            // original product list which contains all of them

            if (constraint == null || constraint.length() == 0) {
                results.values = product_objs;
                results.count = product_objs.size();
            }
            else {
                // Some search constraint has been passed
                // so let's filter accordingly
                ArrayList<Products> filteredProducts = new ArrayList<Products>();

                // We'll go through all the contacts and see
                // if they contain the supplied string
                for (Products p : product_objs) {
                    if (p.get_Product_Name().toUpperCase().contains( constraint.toString().toUpperCase())) {
                        // if `contains` == true then add it
                        // to our filtered list
                        filteredProducts.add(p);
                    }
                }

                // Finally set the filtered values and size/count
                results.values = filteredProducts;
                results.count = filteredProducts.size();
            }

            // Return our FilterResults object
            return results;
        }


        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            product_objs = (ArrayList<Products>) results.values;
            notifyDataSetChanged();
            //important method that needs to be addressed later
            //notify();
        }

    }
    public ArrayList<Products> returnArray(){
        return product_objs;
    }


}
