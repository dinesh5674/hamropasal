package com.example.dinesh.hamropasal;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dinesh.hamropasal.CRUD.DetailHelper;
import com.example.dinesh.hamropasal.Custom.CustomSelector;
import com.example.dinesh.hamropasal.Custom.VariableInitializer;
import com.example.dinesh.hamropasal.dbModel.Brands;
import com.example.dinesh.hamropasal.dbModel.Details;
import com.example.dinesh.hamropasal.dbModel.Products;
import com.example.dinesh.hamropasal.dbModel.Sizes;
import com.example.dinesh.hamropasal.dbModel.Types;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Niroj on 7/23/2016.
 */
public class PurchasedEntry  extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener  {


    private FrameLayout customSelector;
    private FrameLayout purchaseFrame;

    private TextView textView3;
    CustomSelector cusSelect;
    VariableInitializer varInit;

    private Switch quantitySwitch;
    private EditText costPrice, productCode, purchasedDt, quantity;
    private Button addDetail,viewAll;

    public long detailID;

    private ArrayList<Details> detailArray;

    private DetailHelper myDetail;

    //Other product detail values
    String productValue;
    String brandValue;
    String typeValue;
    String sizeValue;
    String productCodeValue;
    String soldPriceValue;
    String costPriceValue;
    String sellDateValue;
    String purchaseDateValue;
    String profitValue;
    int creditorid;

    int pYear, pMonth, pDay, entryQuantity;
    int qty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.purchased_entry);
        super.onCreateDrawer();


        cusSelect = new CustomSelector(this);

        varInit = new VariableInitializer(this);

        customSelector = (FrameLayout)findViewById(R.id.customSelector);
        purchaseFrame = (FrameLayout)findViewById(R.id.purchaseFrame);
        textView3 = (TextView)findViewById(R.id.textView3);

        customSelector.addView(cusSelect);


        costPrice = (EditText) findViewById(R.id.costPrice);
        productCode = (EditText) findViewById(R.id.productCode);
        purchasedDt = (EditText) findViewById(R.id.purchasedDate);
        quantity = (EditText) findViewById(R.id.quantity);

        quantitySwitch = (Switch) findViewById(R.id.quantitySwitch);
        addDetail = (Button) findViewById(R.id.addDetail);
        viewAll = (Button) findViewById(R.id.viewAll);

        switchHandler();

        //varInit.dbInitializer(this);
        myDetail = new DetailHelper(this);
        detailArray = myDetail.getAllDetails();

        Calendar pCurrentDate= Calendar.getInstance();
        pYear=pCurrentDate.get(Calendar.YEAR);
        pMonth=pCurrentDate.get(Calendar.MONTH)+1;
        pDay=pCurrentDate.get(Calendar.DAY_OF_MONTH);
        purchasedDt.setText("" + pYear + "-" + (pMonth<10?("0"+pMonth):pMonth) + "-" + (pDay<10?("0"+pDay):pDay));
        //get the view of list view in front
      //  (pMonth<10?("0"+pMonth):pMonth) + "-" + (pDay<10?("0"+pDay):pDay)
        customSelector.bringToFront();
        customSelector.invalidate();

        purchasedDt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar pCurrentDate= Calendar.getInstance();
                pYear=pCurrentDate.get(Calendar.YEAR);
                pMonth=pCurrentDate.get(Calendar.MONTH);
                pDay=pCurrentDate.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog mDatePicker=new DatePickerDialog(PurchasedEntry.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        //purchasedDt.setText("" + selectedyear + "-" + (selectedmonth+1) + "-" + selectedday);
                        purchasedDt.setText("" + selectedyear + "-" + ((selectedmonth+1)<10?("0"+(selectedmonth+1)):(selectedmonth+1))
                                + "-" + (selectedday<10?("0"+selectedday):selectedday));
                    }
                },pYear, pMonth, pDay);
                mDatePicker.show();
            }
        });

    }
    public void switchHandler(){
        //set the switch to ON
        quantitySwitch.setChecked(false);
        //attach a listener to check for changes in state
        quantitySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if(isChecked){
                    quantity.setVisibility(View.VISIBLE);
                }else{
                    quantity.setVisibility(View.GONE);
                }

            }
        });

        //check the current state before we display the screen
        if(quantitySwitch.isChecked()){
            quantity.setVisibility(View.VISIBLE);
        }else{
            quantity.setVisibility(View.GONE);
        }

    }

    public void addDetailClicked (View view){
        if(!containsProductId() || !containsBrandId() || !containsSizeId() || !containsTypeId()){
            if(!containsProductId() && containsProductName()){
                getProductId();
            }
            if(!containsBrandId() && containsBrandName()){
                getBrandId();
            }
            if(!containsSizeId() && containsSizeName()){
                getSizeId();
            }
            if(!containsTypeId() && containsTypeName()){
                getTypeId();
            }
        }
        if(containsProductId() && containsBrandId() && containsSizeId() && containsTypeId()) {
            if ((costPrice.getText().toString().trim().length() > 0)&& (productCode.getText().toString().trim().length() > 0)) {

                checkDetails();

            } else {
                Toast.makeText(PurchasedEntry.this, "Select or insert all product information", Toast.LENGTH_SHORT).show();
            }
        }

    }
    public void checkDetails(){
        Details detail = new Details();
        detail.set_Product_Id(Integer.parseInt(cusSelect.productID));
        detail.set_Brand_Id(Integer.parseInt(cusSelect.brandID));
        detail.set_Size_Id(Integer.parseInt(cusSelect.sizeID));
        detail.set_Type_Id(Integer.parseInt(cusSelect.typeID));
        detail.set_Cost_Price(costPrice.getText().toString());
        detail.set_Product_Code(productCode.getText().toString());
        detail.set_Purchase_DateTime(purchasedDt.getText().toString());
        if(quantity.getText().toString().trim().length() > 0){
            entryQuantity = Integer.parseInt(quantity.getText().toString());
            for(int i = 1; i <= entryQuantity; i++){
                updateProduct(cusSelect.proObject);
                updateBrand(cusSelect.braObject);
                updateSize(cusSelect.sizObject);
                updateType(cusSelect.typObject);
                insertDetails(detail);
            }
            Toast.makeText(PurchasedEntry.this, entryQuantity+ " : New Details of product was Inserted", Toast.LENGTH_SHORT).show();
        }else {
            updateProduct(cusSelect.proObject);
            updateBrand(cusSelect.braObject);
            updateSize(cusSelect.sizObject);
            updateType(cusSelect.typObject);
            insertDetails(detail);
            //Toast.makeText(PurchasedEntry.this,"Details of product was Inserted", Toast.LENGTH_SHORT).show();
        }

    }
    public void insertDetails(Details detail){
        detailID = myDetail.detail_insert(detail);

        if (detailID > -1) {
            detail.set_Detail_Id((int)detailID);
            detailArray.add(detail);
            //Toast.makeText(PurchasedEntry.this, "Detail was Inserted", Toast.LENGTH_SHORT).show();
            costPrice.setText("");
            productCode.setText("");
            cusSelect.productName.setText("");
            cusSelect.brandName.setText("");
            cusSelect.sizeName.setText("");
            cusSelect.typeName.setText("");
            quantity.setText("");
            cusSelect.productID = null;
            cusSelect.brandID = null;
            cusSelect.sizeID = null;
            cusSelect.typeID = null;
        } else {
            Toast.makeText(PurchasedEntry.this, "Detail was not Inserted", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean containsProductId (){
        if(cusSelect.productID !=null) {
            return true;
        }else{
            return false;
        }
    }
    public boolean containsBrandId (){
        if(cusSelect.brandID !=null) {
            return true;
        }else{
            return false;
        }
    }
    public boolean containsSizeId (){
        if(cusSelect.sizeID !=null) {
            return true;
        }else{
            return false;
        }
    }
    public boolean containsTypeId (){
        if(cusSelect.typeID !=null) {
            return true;
        }else{
            return false;
        }
    }
    public boolean containsProductName(){
        if(cusSelect.productName.getText().toString().trim().length() >0){
            return true;
        }else{
            Toast.makeText(PurchasedEntry.this,"Select or insert Product",Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public boolean containsBrandName(){
        if(cusSelect.brandName.getText().toString().trim().length() >0){
            return true;
        }else{
            Toast.makeText(PurchasedEntry.this,"Select or insert Brand",Toast.LENGTH_SHORT).show();
            return false;
        }
    }
    public boolean containsSizeName(){
        if(cusSelect.sizeName.getText().toString().trim().length() >0){
            return true;
        }else{
            Toast.makeText(PurchasedEntry.this,"Select or insert Size",Toast.LENGTH_SHORT).show();
            return false;
        }
    }
    public boolean containsTypeName(){
        if(cusSelect.typeName.getText().toString().trim().length() >0){
            return true;
        }else{
            Toast.makeText(PurchasedEntry.this,"Select or insert Type",Toast.LENGTH_SHORT).show();
            return false;
        }
    }
    public void getProductId(){
        Products pro;
        pro = varInit.productExists(cusSelect.productName.getText().toString());
        if((containsProductName()) && (pro!=null)) {
            cusSelect.productID = String.valueOf(pro.get_Product_Id());
            cusSelect.proObject = pro;
        }else{
            insertProduct();
        }
    }
    public void getBrandId(){
        Brands bra;
        bra = varInit.brandExists(cusSelect.brandName.getText().toString());
        if((containsBrandName()) && (bra!=null)) {
            cusSelect.brandID = String.valueOf(bra.get_Brand_Id());
            cusSelect.braObject = bra;
        }else{
            insertBrand();
        }
    }
    public void getSizeId(){
        Sizes siz;
        siz = varInit.sizeExists(cusSelect.sizeName.getText().toString());
        if((containsSizeName()) && (siz !=null)){
            cusSelect.sizeID = String.valueOf(siz.get_Size_Id());
            cusSelect.sizObject = siz;
        }else{
            insertSize();
        }

    }
    public void getTypeId(){
        Types typ;
        typ = varInit.typeExists(cusSelect.typeName.getText().toString());
        if((containsTypeName()) && (typ !=null)){
            cusSelect.typeID = String.valueOf(typ.get_Type_Id());
            cusSelect.typObject = typ;
        }else {
            insertType();
        }

    }
    public void insertProduct(){
        if(cusSelect.productName.getText().toString().trim().length() >0){
            Products product = new Products();
            product.set_Product_Name(cusSelect.productName.getText().toString());
            product.set_Quantity(0);
            long isInserted = varInit.myProduct.productInsert(product);
            if(isInserted > -1){
                product.set_Product_Id((int)isInserted);
                //cusSelect.productArray.add(product);
                //varInit.refreshProductArray();
                cusSelect.productAdapter.add(product);
                cusSelect.productAdapter.notifyDataSetChanged();
                //Toast.makeText(PurchasedEntry.this,"Product was Inserted",Toast.LENGTH_SHORT).show();
                cusSelect.productID = String.valueOf((int)isInserted);
                cusSelect.proObject = product;
                //cusSelect.productName.setText("");
            }else{
                Toast.makeText(PurchasedEntry.this,"Product was not Inserted",Toast.LENGTH_SHORT).show();
               // cusSelect.productName.setText("");
            }
        }else {
            //Toast.makeText(PurchasedEntry.this,"Select or insert Product",Toast.LENGTH_SHORT).show();
        }

    }
    public void updateProduct( Products pro){
            pro.set_Product_Id(pro.get_Product_Id());
            pro.set_Product_Name(pro.get_Product_Name());
            qty = pro.get_Quantity() + 1;
            pro.set_Quantity(qty);
            long isUpdated = varInit.myProduct.productUpdate(pro);
            if(isUpdated > -1){
                varInit.refreshProductArray();
                cusSelect.productID = String.valueOf((int)isUpdated);
            }else{
                Toast.makeText(PurchasedEntry.this,"Product was not Updated",Toast.LENGTH_SHORT).show();
                // cusSelect.productName.setText("");
            }
    }
    public void insertBrand(){
        if(cusSelect.brandName.getText().toString().trim().length() > 0){
            Brands brand = new Brands();
            brand.set_Brand_Name(cusSelect.brandName.getText().toString());
            brand.set_Quantity(0);
            long isInserted = varInit.myBrand.brandInsert(brand);
            if(isInserted > -1){
                brand.set_Brand_Id((int)isInserted);
                //varInit.refreshBrandArray();
                cusSelect.brandAdapter.add(brand);
                cusSelect.brandAdapter.notifyDataSetChanged();
                //Toast.makeText(PurchasedEntry.this,"Brand was Inserted",Toast.LENGTH_SHORT).show();
                cusSelect.brandID = String.valueOf((int)isInserted);
                cusSelect.braObject = brand;
                //cusSelect.brandName.setText("");
            }else{
                Toast.makeText(PurchasedEntry.this,"Brand was not Inserted",Toast.LENGTH_SHORT).show();
                //cusSelect.brandName.setText("");
            }
        }else {
            //Toast.makeText(PurchasedEntry.this,"Select or insert Brand",Toast.LENGTH_SHORT).show();
        }

    }
    public void updateBrand( Brands bra){
        bra.set_Brand_Id(bra.get_Brand_Id());
        bra.set_Brand_Name(bra.get_Brand_Name());
        qty = bra.get_Quantity() + 1;
        bra.set_Quantity(qty);
        long isUpdated = varInit.myBrand.brandUpdate(bra);
        if(isUpdated > -1){
            varInit.refreshBrandArray();
            cusSelect.brandID = String.valueOf((int)isUpdated);
        }else{
            Toast.makeText(PurchasedEntry.this,"Brand was not Updated",Toast.LENGTH_SHORT).show();
            // cusSelect.productName.setText("");
        }
    }
    public void insertSize(){
        if(cusSelect.sizeName.getText().toString().trim().length() > 0){
            Sizes size = new Sizes();
            size.set_Size_Name(cusSelect.sizeName.getText().toString());
            size.set_Quantity(0);
            long isInserted = varInit.mySize.sizeInsert(size);
            //Boolean isInserted = mySize.sizeInsert(sizeName.getText().toString());
            if(isInserted > -1){
                size.set_Size_Id((int) isInserted);
                //varInit.refreshSizeArray();
                cusSelect.sizeAdapter.add(size);
                cusSelect.sizeAdapter.notifyDataSetChanged();
                //Toast.makeText(PurchasedEntry.this,"Size was Inserted",Toast.LENGTH_SHORT).show();
                cusSelect.sizeID = String.valueOf((int)isInserted);
                cusSelect.sizObject = size;
                //cusSelect.sizeName.setText("");
            }else{
                Toast.makeText(PurchasedEntry.this,"Size was not Inserted",Toast.LENGTH_SHORT).show();
                //cusSelect.sizeName.setText("");
            }
        }else{
            //Toast.makeText(PurchasedEntry.this,"Select or insert Size",Toast.LENGTH_SHORT).show();
        }

    }
    public void updateSize( Sizes siz){
        siz.set_Size_Id(siz.get_Size_Id());
        siz.set_Size_Name(siz.get_Size_Name());
        qty = siz.get_Quantity() + 1;
        siz.set_Quantity(qty);
        long isUpdated = varInit.mySize.sizeUpdate(siz);
        if(isUpdated > -1){
            varInit.refreshSizeArray();
            cusSelect.sizeID = String.valueOf((int)isUpdated);
        }else{
            Toast.makeText(PurchasedEntry.this,"Size was not Updated",Toast.LENGTH_SHORT).show();
            // cusSelect.productName.setText("");
        }
    }
    public void insertType(){
        if(cusSelect.typeName.getText().toString().trim().length() >0){
            Types type = new Types();
            type.set_Type_Name(cusSelect.typeName.getText().toString());
            type.set_Quantity(0);
            long isInserted = varInit.myType.typeInsert(type);
            if(isInserted > -1){
                type.set_Type_Id((int)isInserted);
                //varInit.refreshTypeArray();
                cusSelect.typeAdapter.add(type);
                cusSelect.typeAdapter.notifyDataSetChanged();
                //Toast.makeText(PurchasedEntry.this,"Type was Inserted",Toast.LENGTH_SHORT).show();
                cusSelect.typeID = String.valueOf((int)isInserted);
                cusSelect.typObject = type;
                //cusSelect.typeName.setText("");
            }else{
                Toast.makeText(PurchasedEntry.this,"Type was not Inserted",Toast.LENGTH_SHORT).show();
                //cusSelect.typeName.setText("");
            }
        }else{
            //Toast.makeText(PurchasedEntry.this,"Select or insert Type",Toast.LENGTH_SHORT).show();
        }

    }
    public void updateType( Types typ){
        typ.set_Type_Id(typ.get_Type_Id());
        typ.set_Type_Name(typ.get_Type_Name());
        qty = typ.get_Quantity() + 1;
        typ.set_Quantity(qty);
        long isUpdated = varInit.myType.typeUpdate(typ);
        if(isUpdated > -1){
            varInit.refreshTypeArray();
            cusSelect.typeID = String.valueOf((int)isUpdated);
        }else{
            Toast.makeText(PurchasedEntry.this,"Type was not Updated",Toast.LENGTH_SHORT).show();
            // cusSelect.productName.setText("");
        }
    }
    public void viewAllProductClicked (View view){
        //detailArray = myDetail.getAllDetails();
        if(detailArray.isEmpty()){
            //some error
            showData("No Products", "Fill Some Product");
            return;
        }else{
            StringBuffer buffer = new StringBuffer();
            for(Details detail:detailArray){

                for(Products product:varInit.productArray){
                    if(product.get_Product_Id() == detail.get_Product_Id()){
                        productValue = product.get_Product_Name();
                        break;
                    }
                }
                for(Brands brand:varInit.brandArray){
                    if(brand.get_Brand_Id() == detail.get_Brand_Id()){
                        brandValue = brand.get_Brand_Name();
                        break;
                    }
                }
                for(Sizes size:varInit.sizeArray){
                    if(size.get_Size_Id() == detail.get_Size_Id()){
                        sizeValue = size.get_Size_Name();
                        break;
                    }
                }
                for(Types type:varInit.typeArray){
                    if(type.get_Type_Id() == detail.get_Type_Id()){
                        typeValue = type.get_Type_Name();
                        break;
                    }
                }
                //for(Details detail:detailArray){
                //if(detail.get_Detail_Id() == detail.get_Detail_Id()){
                productCodeValue = detail.get_Product_Code();
                soldPriceValue = detail.get_Sold_Price();
                costPriceValue = detail.get_Cost_Price();
                sellDateValue = detail.get_Sold_DateTime();
                purchaseDateValue = detail.get_Purchase_DateTime();
                profitValue = detail.get_Profit();
                creditorid = detail.get_Creditor_Id();
                // }
                // }

                buffer.append("Product: "+ productValue +" Brand: "+ brandValue +" Size: "+sizeValue+" Type: "+typeValue+" ProductCode: "+productCodeValue
                        +" SoldPrice: "+soldPriceValue+" CostPrice: "+costPriceValue+" SellDate: "+sellDateValue+" PurchaseDate: "+purchaseDateValue+" Profit: "+profitValue+
                        "CreditorId: "+creditorid+"\n");
            }
            //Show all the data
            showData("Data", buffer.toString());
        }
    }


    //Alert Dialog initialization for DB table Data
    public void showData( String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            Intent myIntent = new Intent(PurchasedEntry.this, MainActivity.class);
            startActivity(myIntent);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}