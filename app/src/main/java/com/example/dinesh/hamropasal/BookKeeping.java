package com.example.dinesh.hamropasal;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dinesh.hamropasal.CRUD.BrandHelper;
import com.example.dinesh.hamropasal.CRUD.DetailHelper;
import com.example.dinesh.hamropasal.CRUD.ProductHelper;
import com.example.dinesh.hamropasal.CRUD.SizeHelper;
import com.example.dinesh.hamropasal.CRUD.TypeHelper;
import com.example.dinesh.hamropasal.Custom.GridAdapter;
import com.example.dinesh.hamropasal.Custom.SpinnerAdapter;
import com.example.dinesh.hamropasal.dbModel.Brands;
import com.example.dinesh.hamropasal.dbModel.Details;
import com.example.dinesh.hamropasal.dbModel.PBST_Name;
import com.example.dinesh.hamropasal.dbModel.Products;
import com.example.dinesh.hamropasal.dbModel.SalesAndProfit;
import com.example.dinesh.hamropasal.dbModel.Sizes;
import com.example.dinesh.hamropasal.dbModel.Types;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import static android.R.attr.key;

/**
 * Created by Niroj on 7/23/2016.
 */
public class BookKeeping extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    /**  private ArrayList<Products> productArray;
     private ArrayList<Brands> brandArray;
     private ArrayList<Sizes> sizeArray;
     private ArrayList<Types> typeArray;
     private ArrayList<Details> detailArray;
     private ArrayList<Details> selectedDetailsItem;

     private Boolean isUserAction = false;

     private int countbet_button_clicked=0;
     private int countMon_button_clicked=0;

     private Spinner spinnerMonth, spinnerYear;
     private Button searchByMonth,searchByDate,search;
     private TextView from, to;
     EditText fromDate, toDate;
     private GridView myGrid;
     private GridAdapter arrayAdapterD;

     private ProductHelper myProduct;
     private BrandHelper myBrand;
     private TypeHelper myType;
     private SizeHelper mySize;
     private DetailHelper myDetail;

     String toDateString_bet,fromDateString_bet;


     Date date1,date2,date3,date4;
     int pYear, pMonth, pDay;

     String month,year;
     String day="1";
     String stringToDateFormat;
     String stringToDateFormatEnd;
     private  List<String> listMonth = new ArrayList<String>();
     private List<String> listYear = new ArrayList<String>();
     private ArrayAdapter<String> adapterMonth;
     private ArrayAdapter<String> adapterYear;
     SimpleDateFormat format = new SimpleDateFormat("yyyy-MMMM-dd");
     SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); **/
    private ProductHelper myProduct;
    private BrandHelper myBrand;
    private TypeHelper myType;
    private SizeHelper mySize;
    private DetailHelper myDetail;

    private ArrayList<Products> productArray;
    private ArrayList<Brands> brandArray;
    private ArrayList<Sizes> sizeArray;
    private ArrayList<Types> typeArray;
    private ArrayList<Details> detailArray;
    private ArrayList<Details> selectedDetailsItem;
    private ArrayList<PBST_Name> pbst_object;


    private HashMap<PBST_Name, ArrayList<Details>> hashmap4pbst = new HashMap<PBST_Name, ArrayList<Details>>();

    int pYear, pMonth, pDay;
    int count4detail_obj =0;

    String month;
    Date dateFrom,dateTo;
    //private DetailHelper myDetail;
    //private ArrayList<Details> detailArray;
    private Spinner spinnerMonth, spinnerYear;

    // ArrayList <String> array4Month;
    // ArrayList<Details> filteredArray;
    ArrayList<Details> detObj_arrayList4Hashmap;
    ArrayList<Details> temp_detail_obj;
    //HashMap<String, ArrayList<Details>> hashMap4Spinner;
    LinkedHashMap<String,ArrayList <Details>> hashMap4Spinner;

    String toDateString_bet,fromDateString_bet,toDateString_bet_comparision;

    String preceding_month=" ";
    String current_month_name=" ";
    String key4hashmap=" ";

    boolean firstRun_flag=true;
    boolean isNotSameYear;
    //  private Boolean isUserAction = false;
    boolean isLastobject=false;
    private  List<String> listMonth = new ArrayList<String>();
    private List<String> listYear = new ArrayList<String>();
    //private ArrayAdapter<String> adapterMonth;
    //  private ArrayAdapter<SalesAndProfit> adapterMonth;
    private GridAdapter arrayAdapterD;
    private ArrayAdapter<String> adapterYear;
    private SpinnerAdapter adapter4spinner;
    private GridAdapter adapter4grid;

    private ArrayList<SalesAndProfit> arraylist_sales_profit;
    //public String dateString;
    private String spinYear,spinMonth;
    private boolean spinMonthFlag;
    private boolean spinYearFlag;

    private SalesAndProfit Sales_Profit;
    private SalesAndProfit temp_SP_obj;
    private Date toDate4Spinner,fromDate4Spinner;
    Date date1,date2,date3,date4;

    private Button searchByMonth,searchByDate,search;
    private TextView from, to;
    EditText fromDate, toDate;
    private GridView myGrid;

    //  SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM");
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat month_formatter = new SimpleDateFormat("MMMM");
    Calendar pCurrentDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.book_keeping);
        super.onCreateDrawer();


        search= (Button)findViewById(R.id.search);
        searchByMonth=(Button)findViewById(R.id.search_month);
        searchByDate=(Button)findViewById(R.id.search_date);
        myGrid = (GridView)findViewById(R.id.gridView);
        from=(TextView)findViewById(R.id.fromDate);
        to=(TextView)findViewById(R.id.toDate);
        fromDate=(EditText) findViewById(R.id.editTextFrom);
        toDate=(EditText) findViewById(R.id.editTextTo);

        myProduct = new ProductHelper(this);
        myBrand = new BrandHelper(this);
        myType = new TypeHelper(this);
        mySize = new SizeHelper(this);
        myDetail = new DetailHelper(this);

        productArray = myProduct.getAllProducts();
        brandArray = myBrand.getAllBrands();
        sizeArray = mySize.getAllSizes();
        typeArray = myType.getAllTypes();
        selectedDetailsItem = new ArrayList<>();
        spinnerMonth = (Spinner) findViewById(R.id.month);

        pCurrentDate = Calendar.getInstance();
        int to_Year = pCurrentDate.get(Calendar.YEAR);
        int to_Month = pCurrentDate.get(Calendar.MONTH);
        // System.out.println("Int value of month:" + to_Month);
        // final String to_Date = to_Year + "-" + to_Month;
        int to_Day = pCurrentDate.getActualMaximum(Calendar.DAY_OF_MONTH)+1;
        final String to_Date = to_Year + "-" + (to_Month<10?("0"+to_Month):(to_Month)) + "-" + (to_Day<10?("0"+to_Day):(to_Day));

        System.out.println("To Date is: " + to_Date);
        try {
            toDate4Spinner = formatter.parse(to_Date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //  System.out.println("dsfhsdlöfhsdlhfdsjöhfsdjkhfdsjkfhsdjkf"+new SimpleDateFormat("MMMM").format(date1));

        int from_Year = to_Year - 1;
        int from_Month= to_Month + 1;
        //String from_Date = from_Year + "-" + from_Month;
        int from_Day=1;
        final String from_Date = from_Year + "-" + (from_Month<10?("0"+from_Month):(from_Month)) + "-" + (from_Day<10?("0"+from_Day):(from_Day));

        // final String from_Date = from_Year + "-" + from_Month + "-" + from_Day;
        System.out.println("From Date is: " + from_Date);

        try {
            fromDate4Spinner = formatter.parse(from_Date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        System.out.println("From real Date is: " + fromDate4Spinner);
        System.out.println("To real Date is: " + toDate4Spinner);


        // spinnerMonth = (Spinner) findViewById(R.id.month);
        // spinnerYear = (Spinner) findViewById(R.id.year);

        //myDetail = new DetailHelper(this);

        spinnerMonth.setVisibility(View.INVISIBLE);
        // spinnerYear.setVisibility(View.INVISIBLE);
        toDate.setVisibility(View.INVISIBLE);
        fromDate.setVisibility(View.INVISIBLE);
        search.setVisibility(View.INVISIBLE);
        from.setVisibility(View.INVISIBLE);
        to.setVisibility(View.INVISIBLE);


        searchByMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchByMonth.getBackground().setColorFilter(new LightingColorFilter(0x00FFFF , 0x00FFFF));//after press color
                searchByDate.getBackground().setColorFilter(new LightingColorFilter(0x004F81, 0x004F81));

                fromDate.setVisibility(View.INVISIBLE);
                toDate.setVisibility(View.INVISIBLE);
                spinnerMonth.setVisibility(View.VISIBLE);
                //  spinnerYear.setVisibility(View.VISIBLE);
                from.setVisibility(View.INVISIBLE);
                to.setVisibility(View.INVISIBLE);
                search.setVisibility(View.INVISIBLE);

                //  getValueByMonth();
                /**  if(countMon_button_clicked>1){
                 //arrayAdapterD.clear();
                 //arrayAdapterD.notifyDataSetChanged();
                 }**/

                myDetail = new DetailHelper(BookKeeping.this);
                // detailArray = myDetail.getBookeepingDetails(fromDate4Spinner,toDate4Spinner);
                detailArray = myDetail.getBookeepingDetails(from_Date,to_Date);
                System.out.println("FreqValue objects: " + detailArray);

                arraylist_sales_profit = new ArrayList<SalesAndProfit>();
                // filteredArray = new ArrayList<Details>() ;
                detObj_arrayList4Hashmap = new ArrayList<Details>();
                hashMap4Spinner = new LinkedHashMap<String, ArrayList<Details>>();



                for (Details detail_obj:detailArray) {
                    count4detail_obj++;

                    //   try {
                    /**The date should be sold_date_time...just now we do not have sell date implemented**/

                    //      if ((formatter.parse(detail_obj.get_Sold_DateTime()).after(fromDate4Spinner)) && (formatter.parse(detail_obj.get_Sold_DateTime()).before(toDate4Spinner))) {
                    System.out.println("Object passed getmonthly method: "+ detail_obj);
                    //  isNotSameYear=false;

                    getMonthlyObjects(detail_obj);

                    if(count4detail_obj==detailArray.size()){
                        isLastobject=true;
                        getMonthlyObjects(detail_obj);
                    }

                    System.out.println("HashMap4Spinner contents:" + hashMap4Spinner);
                    /**   } else {
                     isNotSameYear=true;
                     getMonthlyObjects(detail_obj);
                     }  **/



                    /**} catch (ParseException e) {
                     e.printStackTrace();
                     } **/
                }

                for (String key: hashMap4Spinner.keySet()){
                    int total_profit=0;
                    int total_sale=0;
                    Sales_Profit=new SalesAndProfit();
                    System.out.println("key Value is:" + key);
                    System.out.println("HashMap contents for " + key + " is " + hashMap4Spinner.get(key));
                    Sales_Profit.set_Month_Name(key);
                    for (int k = 0; k < hashMap4Spinner.get(key).size(); k++){
                        total_profit = total_profit +  Integer.parseInt(hashMap4Spinner.get(key).get(k).get_Profit());
                        total_sale = total_sale + Integer.parseInt(hashMap4Spinner.get(key).get(k).get_Sold_Price());
                        // total_sale = total_sale + Integer.parseInt(hashMap4Spinner.get(key).get(k).get_Cost_Price());
                    }

                    Sales_Profit.set_Monthly_Profit(String.valueOf(total_profit));
                    //Sales_Profit.set_Monthly_Profit("1000");
                    Sales_Profit.set_Monthly_Sales(String.valueOf(total_sale));
                    arraylist_sales_profit.add(Sales_Profit);
                    System.out.println(Sales_Profit + "is added to arraylist_sales_profit");

                }

                //System.out.println("arraylist_sales_profit values are : " + arraylist_sales_profit);
                //System.out.println("HashMap Contents are : " + hashMap4Spinner);
                // System.out.println("Array4Hashmap contents : " + detObj_arrayList4Hashmap);


                adapter4spinner = new SpinnerAdapter(BookKeeping.this, R.layout.layout4spinnerview, arraylist_sales_profit);
                // adapterMonth = new ArrayAdapter<SalesAndProfit>(this, android.R.layout.simple_spinner_item, arraylist_sales_profit);
                //adapterMonth.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerMonth.setAdapter(adapter4spinner);


                spinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        //     if(i > 0){

                        temp_detail_obj = new ArrayList <Details>();
                        temp_SP_obj=(SalesAndProfit) spinnerMonth.getItemAtPosition(i);
                        month = temp_SP_obj.get_Month_Name();
                        // isUserAction = true;
                        System.out.println("Month from ITEMSELECT:" + month);
                        temp_detail_obj = hashMap4Spinner.get(month);
                        System.out.println("Detail Object from Spinner:" + temp_detail_obj);
                        getQuantity4BookKeeping(temp_detail_obj);

                        adapter4grid = new GridAdapter(BookKeeping.this,R.layout.layout4gridview,pbst_object);
                        myGrid.setAdapter(adapter4grid);
                        adapter4grid.notifyDataSetChanged();

                        myGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                myGrid.setVisibility(View.GONE);
                                PBST_Name selectedFromList =(PBST_Name) (myGrid.getItemAtPosition(i));
                                selectedDetailsItem=hashmap4pbst.get(selectedFromList);


                                Intent intent = new Intent(BookKeeping.this,CustomGridDetailAdapter.class);
                                intent.putParcelableArrayListExtra("Bookkeeping", selectedDetailsItem);
                                startActivity (intent);

                            }
                        });


                        /**   }else{
                         Toast.makeText(getApplicationContext(), "please select month", Toast.LENGTH_SHORT).show();
                         } **/
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                        //isUserAction = false;

                    }
                });

            }
        });

        searchByDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchByDate.getBackground().setColorFilter(new LightingColorFilter(0x00FFFF , 0x00FFFF));//after press color
                searchByMonth.getBackground().setColorFilter(new LightingColorFilter(0x004F81, 0x004F81));


                spinnerMonth.setVisibility(View.INVISIBLE);
                // spinnerYear.setVisibility(View.INVISIBLE);
                toDate.setVisibility(View.VISIBLE);
                fromDate.setVisibility(View.VISIBLE);
                search.setVisibility(View.VISIBLE);
                fromDate.setVisibility(View.VISIBLE);
                from.setVisibility(View.VISIBLE);

              /*  fromDate.setFocusable(true);
                toDate.setFocusable(true);

                fromDate.setVisibility(View.VISIBLE);
                from.setVisibility(View.VISIBLE);*/

                pCurrentDate = Calendar.getInstance();
                pYear = pCurrentDate.get(Calendar.YEAR);
                pMonth = pCurrentDate.get(Calendar.MONTH);
                pDay = pCurrentDate.get(Calendar.DAY_OF_MONTH);
                fromDate.setText(pYear + "-" +((pCurrentDate.get(Calendar.MONTH)+1)<10?("0"+(pCurrentDate.get(Calendar.MONTH)+1)):(pCurrentDate.get(Calendar.MONTH)+1)) + "-" + (pDay<10?("0"+pDay):pDay));
                fromDateString_bet=pYear + "-" + ((pCurrentDate.get(Calendar.MONTH)+1)<10?("0"+(pCurrentDate.get(Calendar.MONTH)+1)):(pCurrentDate.get(Calendar.MONTH)+1)) + "-" + ((pCurrentDate.get(Calendar.DAY_OF_MONTH)-1)<10?("0"+(pCurrentDate.get(Calendar.DAY_OF_MONTH)-1)):(pCurrentDate.get(Calendar.DAY_OF_MONTH)-1));
                toDate.setText(pYear + "-" +((pCurrentDate.get(Calendar.MONTH)+1)<10?("0"+(pCurrentDate.get(Calendar.MONTH)+1)):(pCurrentDate.get(Calendar.MONTH)+1)) + "-" + (pDay<10?("0"+pDay):pDay));
                toDateString_bet=pYear + "-" + ((pCurrentDate.get(Calendar.MONTH)+1)<10?("0"+(pCurrentDate.get(Calendar.MONTH)+1)):(pCurrentDate.get(Calendar.MONTH)+1)) + "-" +  ((pCurrentDate.get(Calendar.DAY_OF_MONTH)+1)<10?("0"+(pCurrentDate.get(Calendar.DAY_OF_MONTH)+1)):(pCurrentDate.get(Calendar.DAY_OF_MONTH)+1));
                fromDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DatePickerDialog mDatePicker = new DatePickerDialog(BookKeeping.this, new DatePickerDialog.OnDateSetListener() {
                            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                                fromDate.setText(selectedyear + "-" + ((selectedmonth+1)<10?("0"+(selectedmonth+1)):(selectedmonth+1))
                                        + "-" + (selectedday<10?("0"+selectedday):selectedday));
                                fromDateString_bet=selectedyear + "-" + ((selectedmonth+1)<10?("0"+(selectedmonth+1)):(selectedmonth+1))
                                        + "-" + (((selectedday-1)<10)?("0"+(selectedday-1)):(selectedday-1));
                            }
                        }, pYear, pMonth, pDay);
                        mDatePicker.show();
                    }
                });


                toDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DatePickerDialog mDatePicker = new DatePickerDialog(BookKeeping.this, new DatePickerDialog.OnDateSetListener() {
                            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                                toDate.setText(selectedyear + "-" + ((selectedmonth+1)<10?("0"+(selectedmonth+1)):(selectedmonth+1))
                                        + "-" + (selectedday<10?("0"+selectedday):selectedday));
                                toDateString_bet=selectedyear + "-" + ((selectedmonth+1)<10?("0"+(selectedmonth+1)):(selectedmonth+1))
                                        + "-" + (((selectedday+1)<10)?("0"+(selectedday+1)):(selectedday+1));

                            }
                        }, pYear, pMonth, pDay);
                        mDatePicker.setTitle("Select date");
                        mDatePicker.show();
                    }
                });
                search.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        toDateString_bet_comparision=pYear + "-" + ((pCurrentDate.get(Calendar.MONTH)+1)<10?("0"+(pCurrentDate.get(Calendar.MONTH)+1)):(pCurrentDate.get(Calendar.MONTH)+1)) + "-" +  ((pCurrentDate.get(Calendar.DAY_OF_MONTH)+1)<10?("0"+(pCurrentDate.get(Calendar.DAY_OF_MONTH)+1)):(pCurrentDate.get(Calendar.DAY_OF_MONTH)+1));


                        try {
                            dateFrom = formatter.parse(fromDateString_bet);
                            dateTo = formatter.parse(toDateString_bet);

                            System.out.println("3Digit Date are:" + dateFrom + "and" + dateTo);
                            System.out.println("3            Digit Date are:" +toDateString_bet_comparision);


                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        try {
                            if (dateTo.after(formatter.parse(toDateString_bet_comparision)) || dateFrom.after(formatter.parse(toDateString_bet_comparision))){
                                Toast.makeText(getApplicationContext(), "please choose valid date", Toast.LENGTH_SHORT).show();

                            }
                            else {

                        System.out.println("3Digit Date are:" + fromDateString_bet);
                        System.out.println("3Digit Date are____________:" +toDateString_bet);

                        myDetail = new DetailHelper(BookKeeping.this);
                        detailArray = myDetail.getBookeepingDetails(fromDateString_bet, toDateString_bet);


                        System.out.println("FreqValue objects: " + detailArray);


                        //temp_detail_obj = new ArrayList <Details>();
                        getQuantity4BookKeeping(detailArray);

                        adapter4grid = new GridAdapter(BookKeeping.this, R.layout.layout4gridview, pbst_object);
                        myGrid.setAdapter(adapter4grid);
                        adapter4grid.notifyDataSetChanged();

                        myGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                myGrid.setVisibility(View.GONE);
                                PBST_Name selectedFromList = (PBST_Name) (myGrid.getItemAtPosition(i));
                                selectedDetailsItem = hashmap4pbst.get(selectedFromList);


                                Intent intent = new Intent(BookKeeping.this, CustomGridDetailAdapter.class);

                                intent.putParcelableArrayListExtra("Bookkeeping", selectedDetailsItem);
                                startActivity(intent);

                            }
                        });
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                    }

                });


                // getValueByDate();
         /*       if(countbet_button_clicked>1){
                    //arrayAdapterD.clear();
                   // arrayAdapterD.notifyDataSetChanged();
                } */
            }
        });




        /** adapterYear = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listYear);
         adapterYear.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
         spinnerYear.setAdapter(adapterYear);
         spinnerYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        if(i > 0){
        year=spinnerYear.getItemAtPosition(i).toString();
        }else{
        Toast.makeText(getApplicationContext(), "please select year", Toast.LENGTH_SHORT).show();
        }

        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {
        }
        })  **/
    }

    /** private void getValueByMonth() {

     search.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {



    stringToDateFormat= year+"-"+month+"-"+day;
    stringToDateFormatEnd=year+"-"+month+"-"+31;
    if (isUserAction = false){
    Toast.makeText(getApplicationContext(), "please enter desired month value", Toast.LENGTH_SHORT).show();
    } else {


    // String dtStart = "2010-10-15";
    try {
    date1 = formatter.parse(stringToDateFormat);
    date2 = formatter.parse(stringToDateFormatEnd);

    System.out.println("from: " + date1);
    System.out.println("To: " + date2);

    } catch (ParseException e) {
    // TODO Auto-generated catch block
    e.printStackTrace();
    }
    inventoryFetch();

    }
    isUserAction = true;
    }
    });

     }           ***/



    private void getMonthlyObjects(Details det){

        try {

            current_month_name  = getMonth(det.get_Sold_DateTime());
            //current_month_name  = getMonth(det.get_Purchase_DateTime());
            System.out.println("Current Month is: " + current_month_name);

        }catch (ParseException e){
            e.printStackTrace();
        }

        if ( firstRun_flag == true){
            detObj_arrayList4Hashmap.add(det);
            key4hashmap=current_month_name;
            firstRun_flag=false;

        }else if (current_month_name.equals(preceding_month) && !isLastobject){

            key4hashmap=preceding_month;
            detObj_arrayList4Hashmap.add(det);

        }else if (!current_month_name.equals(preceding_month) || isLastobject ) {

            hashMap4Spinner.put(key4hashmap,detObj_arrayList4Hashmap);
            detObj_arrayList4Hashmap=new ArrayList<Details>();
            detObj_arrayList4Hashmap.add(det);
            key4hashmap=current_month_name;
        }

        preceding_month=current_month_name;
        System.out.println("Object added to detObj_arrayList4Hashmap is: " + det);


    }



/**    private void getValueByDate(){


 search.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View view) {

if (fromDate.toString().isEmpty() || toDate.toString().isEmpty()){
Toast.makeText(getApplicationContext(), "please enter desired month value", Toast.LENGTH_SHORT).show();
} else {
try {
date3 = formatter.parse(fromDateString_bet);
date4 = formatter.parse(toDateString_bet);


System.out.println("from   : "+date3);
System.out.println("To    : "+date4);

} catch (ParseException e) {
e.printStackTrace();
}

//  inventoryFetch();
}

}
});

 }   **/



    /**  private void inventoryFetch() {

     ArrayList<Details> filtered4Time = new ArrayList<Details>();
     ArrayList<PBST_Name> obj_and_freq = new ArrayList<PBST_Name>();


     for (Details det_objs:detailArray){


     if (spinnerMonth.getVisibility()==View.VISIBLE && spinnerYear.getVisibility()==View.VISIBLE) {
     try {
     if ((formatter.parse(det_objs.get_Purchase_DateTime()).after(date1)) && (formatter.parse(det_objs.get_Purchase_DateTime()).before(date2)) ) {
     System.out.println("from kjsgjksdhsa  : "+det_objs.get_Purchase_DateTime());

     filtered4Time.add(det_objs);
     }
     } catch (ParseException e) {
     e.printStackTrace();
     }
     }

     else if (fromDate.getVisibility()==View.VISIBLE && toDate.getVisibility()==View.VISIBLE) {
     try {
     if ((formatter.parse(det_objs.get_Purchase_DateTime()).after(date3)) && (formatter.parse(det_objs.get_Purchase_DateTime()).before(date4))) {
     filtered4Time.add(det_objs);
     }
     } catch (ParseException e) {
     e.printStackTrace();
     }
     }

     }


     getQuantity4BookKeeping(filtered4Time); **/


    //   final ArrayList<Details> similarObjectList= new ArrayList<>();
    /**    Set<Details> set4Frequency = new HashSet<Details>(filtered4Time);
     for (Details freqvalue : set4Frequency) {
     int count1 = 0;
     PBST_Name pbst_name = new PBST_Name();
     count1 = Collections.frequency(filtered4Time, freqvalue);
     pbst_name.setCount(count1);
     ArrayList<Details> detailArray4hmap = new ArrayList<Details>();

     for (int k = 0; k < filtered4Time.size(); k++) {

     if (freqvalue.equals(filtered4Time.get(k))) {
     detailArray4hmap.add(filtered4Time.get(k));

     }
     }

     for (Products product : productArray) {
     if (product.get_Product_Id() == freqvalue.get_Product_Id()) {
     pbst_name.setProduct_Name(product.get_Product_Name());
     }
     }

     for (Brands brand : brandArray) {
     if (brand.get_Brand_Id() == freqvalue.get_Brand_Id()) {
     pbst_name.setBrand_Name(brand.get_Brand_Name());
     }
     }

     for (Types type : typeArray) {
     if (type.get_Type_Id() == freqvalue.get_Type_Id()) {
     pbst_name.setType_Name(type.get_Type_Name());
     }
     }

     for (Sizes size : sizeArray) {
     if (size.get_Size_Id() == freqvalue.get_Size_Id()) {
     pbst_name.setSize_Name(size.get_Size_Name());
     }
     }

     obj_and_freq.add(pbst_name);
     hashmap4pbst.put(pbst_name, detailArray4hmap);
     }



     arrayAdapterD = new GridAdapter(BookKeeping.this, R.layout.layout4gridview, obj_and_freq);
     myGrid.setAdapter(arrayAdapterD);
     arrayAdapterD.notifyDataSetChanged();
     myGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    //similarObjectList.clear();
    myGrid.setVisibility(View.GONE);
    PBST_Name selectedFromList =(PBST_Name) (myGrid.getItemAtPosition(i));
    selectedDetailsItem=hashmap4pbst.get(selectedFromList);


    Intent intent = new Intent(BookKeeping.this,CustomGridDetailAdapter.class);
    intent.setAction(ACTION_DO_C);
    intent.putParcelableArrayListExtra("key", selectedDetailsItem);
    startActivityForResult (intent,1);



    }
    });
     }   **/


    private void getQuantity4BookKeeping( ArrayList <Details> detArray){

        ArrayList<Details> temp_array = new ArrayList<Details>();
        temp_array=detArray;
        pbst_object=new ArrayList<PBST_Name>();
        Set<Details> set4Frequency = new HashSet<Details>(temp_array);

        for (Details freqvalue : set4Frequency) {
            int count1 = 0;
            PBST_Name pbst_name = new PBST_Name();
            count1 = Collections.frequency(temp_array, freqvalue);
            pbst_name.setCount(count1);
            ArrayList<Details> detailArray4hmap = new ArrayList<Details>();

            for (int k = 0; k < temp_array.size(); k++) {
                if (freqvalue.equals(temp_array.get(k))) {
                    detailArray4hmap.add(temp_array.get(k));
                }
            }

            for (Products product : productArray) {
                if (product.get_Product_Id() == freqvalue.get_Product_Id()) {
                    pbst_name.setProduct_Name(product.get_Product_Name());
                }
            }

            for (Brands brand : brandArray) {
                if (brand.get_Brand_Id() == freqvalue.get_Brand_Id()) {
                    pbst_name.setBrand_Name(brand.get_Brand_Name());
                }
            }

            for (Types type : typeArray) {
                if (type.get_Type_Id() == freqvalue.get_Type_Id()) {
                    pbst_name.setType_Name(type.get_Type_Name());
                }
            }

            for (Sizes size : sizeArray) {
                if (size.get_Size_Id() == freqvalue.get_Size_Id()) {
                    pbst_name.setSize_Name(size.get_Size_Name());
                }
            }

            pbst_object.add(pbst_name);
            hashmap4pbst.put(pbst_name, detailArray4hmap);
        }

    }


    private static String getMonth(String date) throws ParseException{
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date d = formatter.parse(date);
        String monthName = new SimpleDateFormat("MMMM").format(d);
        return monthName;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            Intent myIntent = new Intent(BookKeeping.this, MainActivity.class);
            startActivity(myIntent);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}