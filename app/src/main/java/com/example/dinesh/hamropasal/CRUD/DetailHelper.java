package com.example.dinesh.hamropasal.CRUD;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.dinesh.hamropasal.dbModel.Details;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by ANONYMOUS_PAL on 7.7.2016.
 */
public class DetailHelper extends DBHelper{
    public DetailHelper(Context context) {
        super(context);
    }
   /* private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd-MM-yyyy hh:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }*/
    private java.sql.Date getSqlDate (String date){

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date parsed = format.parse(date);
            java.sql.Date sqlDate = new java.sql.Date(parsed.getTime());
            return sqlDate;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

    }


    public long detail_insert(Details detail) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(D_Product_ID, detail.get_Product_Id());
        contentValues.put(D_Brand_ID, detail.get_Brand_Id());
        contentValues.put(D_Size_ID, detail.get_Size_Id());
        contentValues.put(D_Type_ID, detail.get_Type_Id());
        contentValues.put(D_Creditor_ID,detail.get_Creditor_Id());
        contentValues.put(Product_Code,detail.get_Product_Code());
        contentValues.put(Cost_Price, Double.parseDouble(detail.get_Cost_Price()));
        contentValues.put(Purchase_Date, detail.get_Purchase_DateTime());
        long result = db.insert(Detail_Table,null,contentValues);

        db.close();
        return result;
    }

    public boolean detail_update(Details detail) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Detail_ID,detail.get_Detail_Id());
        contentValues.put(D_Creditor_ID,detail.get_Creditor_Id());
        contentValues.put(D_Product_ID, detail.get_Product_Id());
        contentValues.put(D_Brand_ID, detail.get_Brand_Id());
        contentValues.put(D_Size_ID, detail.get_Size_Id());
        contentValues.put(D_Type_ID, detail.get_Type_Id());
        contentValues.put(Cost_Price,detail.get_Cost_Price());
        contentValues.put(Sold_Price,detail.get_Sold_Price());
        contentValues.put(Product_Code,detail.get_Product_Code());
        contentValues.put(Purchase_Date,detail.get_Purchase_DateTime());
        contentValues.put(Sell_Date,detail.get_Sold_DateTime());
        contentValues.put(Profit,detail.get_Profit());

        int result = db.update(Detail_Table,contentValues, "detail_Id = ?", new String [] { String.valueOf(detail.get_Detail_Id())});
        db.close();
        if(result == 0) {
            return false;
        }
        else {
            return true;
        }
    }
    public boolean inventry_detail_update(Details detail) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Detail_ID,detail.get_Detail_Id());
        contentValues.put(D_Product_ID, detail.get_Product_Id());
        contentValues.put(D_Brand_ID, detail.get_Brand_Id());
        contentValues.put(D_Size_ID, detail.get_Size_Id());
        contentValues.put(D_Type_ID, detail.get_Type_Id());
        contentValues.put(Cost_Price,detail.get_Cost_Price());
        contentValues.put(Product_Code,detail.get_Product_Code());
        contentValues.put(Purchase_Date,detail.get_Purchase_DateTime());

        int result = db.update(Detail_Table,contentValues, "detail_Id = ?", new String [] { String.valueOf(detail.get_Detail_Id())});
        db.close();
        if(result == 0) {
            return false;
        }
        else {
            return true;
        }
    }


    public int detail_delete(Details detail) {
        SQLiteDatabase db = this.getWritableDatabase();
        int result=  db.delete(Detail_Table,"detail_Id = ?", new String [] {String.valueOf(detail.get_Detail_Id())});
        db.close();
       return result;
    }

    public ArrayList<Details> getAllDetails() {
        ArrayList<Details> arrayList = new ArrayList<Details>();
        SQLiteDatabase db = this.getReadableDatabase();
        Details det;
        Cursor cur = db.rawQuery("select * from "+Detail_Table, null);
        cur.moveToFirst();

        while (cur.isAfterLast() == false) {
            det= new Details();
            det.set_Detail_Id(Integer.parseInt(cur.getString(0)));
            det.set_Product_Id(Integer.parseInt(cur.getString(1)));
            det.set_Brand_Id(Integer.parseInt(cur.getString(2)));
            det.set_Size_Id(Integer.parseInt(cur.getString(3)));
            det.set_Type_Id(Integer.parseInt(cur.getString(4)));
            det.set_Product_Code(cur.getString(5));
            det.set_Sold_Price(cur.getString(6));
            det.set_Cost_Price(cur.getString(7));
            det.set_Sold_DateTime(cur.getString(8));
            det.set_Purchase_DateTime(cur.getString(9));
            det.set_Profit(cur.getString(10));
            det.set_Profit(cur.getString(11));
            det.set_Creditor_Id(Integer.parseInt(cur.getString(12)));
            arrayList.add(det);
            cur.moveToNext();
        }
        cur.close();
        db.close();
        return arrayList;

    }

    // public ArrayList<Details> getDetailsByTime(String startdate,String enddate) {
    public ArrayList<Details> getInventoryDetails() {
        ArrayList<Details> arrayList = new ArrayList<Details>();
        SQLiteDatabase db = this.getReadableDatabase();
        Details det;
        //working code
        //  Cursor cur = db.rawQuery("select * from Detail where soldPrice = ?", new String[]{""});
       // Cursor cur=db.rawQuery("SELECT * FROM " + Detail_Table + " WHERE "+ Sold_Price + " = ?"+
        //               " ORDER BY "+Purchase_Date + " DESC"
        //        , new String[] {""});

        //Cursor cur = db.rawQuery("SELECT * FROM " + Detail_Table + " WHERE "+ Sold_Price + " IS NULL OR "+ Sold_Price + " =?", new String[]{""}, null, null,Purchase_Date + " DESC");
        Cursor cur = db.rawQuery("SELECT * FROM " + Detail_Table + " WHERE "+ Sold_Price +" IS NULL"+
                        " ORDER BY "+Purchase_Date + " DESC", null);
        //sort between dates
        //Cursor cur = db.rawQuery("SELECT * FROM Detail WHERE purchase_Date BETWEEN ?  AND ?", new String[]{startdate, enddate});
        cur.moveToFirst();

        while (cur.isAfterLast() == false) {
            det= new Details();
            det.set_Detail_Id(Integer.parseInt(cur.getString(0)));
            det.set_Product_Id(Integer.parseInt(cur.getString(1)));
            det.set_Brand_Id(Integer.parseInt(cur.getString(2)));
            det.set_Size_Id(Integer.parseInt(cur.getString(3)));
            det.set_Type_Id(Integer.parseInt(cur.getString(4)));
            det.set_Product_Code(cur.getString(5));
            det.set_Sold_Price(cur.getString(6));
            det.set_Cost_Price(cur.getString(7));
            det.set_Sold_DateTime(cur.getString(8));
            det.set_Purchase_DateTime(cur.getString(9));
            det.set_Profit(cur.getString(10));

            arrayList.add(det);
            cur.moveToNext();
        }
        cur.close();
        db.close();
        return arrayList;

    }
    public ArrayList<Details> getCreditDetails(int id){
        ArrayList<Details> arrayList = new ArrayList<Details>();
        SQLiteDatabase db = this.getReadableDatabase();
        Details det;
        //Cursor cur=db.rawQuery("SELECT * FROM " + Detail_Table + " WHERE "+ Creditor_ID +" != ?"
          //      , new String[]{""});
        String Query = "Select * from " + Detail_Table + " where " + D_Creditor_ID + " = " +id ;
        Cursor cur = db.rawQuery(Query, null);

        cur.moveToFirst();
        while (cur.isAfterLast() == false){
            det = new Details();
            det.set_Detail_Id(Integer.parseInt(cur.getString(0)));
            det.set_Product_Id(Integer.parseInt(cur.getString(1)));
            det.set_Brand_Id(Integer.parseInt(cur.getString(2)));
            det.set_Size_Id(Integer.parseInt(cur.getString(3)));
            det.set_Type_Id(Integer.parseInt(cur.getString(4)));

            det.set_Product_Code(cur.getString(5));
            det.set_Sold_Price(cur.getString(6));
            det.set_Cost_Price(cur.getString(7));
            det.set_Sold_DateTime(cur.getString(8));
            det.set_Purchase_DateTime(cur.getString(9));
            det.set_Profit(cur.getString(10));
            det.set_Creditor_Id(Integer.parseInt(cur.getString(12)));
            arrayList.add(det);
            cur.moveToNext();
        }
        cur.close();
        db.close();
        return arrayList;
        }


  /**  public ArrayList<Details> getBookeepingDetails() {
        ArrayList<Details> arrayList = new ArrayList<Details>();
        SQLiteDatabase db = this.getReadableDatabase();
        Details det;
        //working code
        //  Cursor cur = db.rawQuery("select * from Detail where soldPrice = ?", new String[]{""});
        Cursor cur=db.rawQuery("SELECT * FROM " + Detail_Table + " WHERE "+ Sold_Price +" IS NOT NULL"+
                        " ORDER BY "+Purchase_Date + " DESC"
                , null);
        //Sold_Price +" != ''" check
      //  Cursor c=db.rawQuery("Select * from Detail where soldPrice IS NOT NULL",null);



//sort between dates
        //Cursor cur = db.rawQuery("SELECT * FROM Detail WHERE purchase_Date BETWEEN ?  AND ?", new String[]{startdate, enddate});
        cur.moveToFirst();

        while (cur.isAfterLast() == false) {
            det= new Details();
            det.set_Detail_Id(Integer.parseInt(cur.getString(0)));
            det.set_Product_Id(Integer.parseInt(cur.getString(1)));
            det.set_Brand_Id(Integer.parseInt(cur.getString(2)));
            det.set_Size_Id(Integer.parseInt(cur.getString(3)));
            det.set_Type_Id(Integer.parseInt(cur.getString(4)));

            det.set_Product_Code(cur.getString(5));
            det.set_Sold_Price(cur.getString(6));
            det.set_Cost_Price(cur.getString(7));
            det.set_Sold_DateTime(cur.getString(8));
            det.set_Purchase_DateTime(cur.getString(9));
            det.set_Profit(cur.getString(10));
            det.set_Creditor_Id(Integer.parseInt(cur.getString(12)));
            arrayList.add(det);
            cur.moveToNext();
        }
        cur.close();
        db.close();
        return arrayList;

    } **/


  public ArrayList<Details> getBookeepingDetails(String date1, String date2) {
 //public ArrayList<Details> getBookeepingDetails(Date date1, Date date2) {
      ArrayList<Details> arrayList = new ArrayList<Details>();
      SQLiteDatabase db = this.getReadableDatabase();
      Details det;


    /*  java.sql.Date sqlDate1 = new java.sql.Date(date1.getTime());
      java.sql.Date sqlDate2 = new java.sql.Date(date2.getTime()); */

   /*  Cursor cur=db.rawQuery(" SELECT * FROM " + Detail_Table + " WHERE Date("+ Sell_Date +") BETWEEN '"+ sqlDate1 + "' AND '" + sqlDate2 +
                     "' ORDER BY "+Sell_Date + " DESC"
             , null); */

      //working code
      //  Cursor cur = db.rawQuery("select * from Detail where soldPrice = ?", new String[]{""});

      Cursor cur=db.rawQuery(" SELECT * FROM " + Detail_Table + " WHERE "+ Sell_Date +" BETWEEN '"+ date1 + "' AND '" + date2 +
                      "' ORDER BY "+Sell_Date + " DESC"
              , null);
   /*   System.out.println(" SELECT * FROM " + Detail_Table + " WHERE "+ Purchase_Date +" BETWEEN '"+ sqlDate1 + "' AND '" + sqlDate2 +
      "' ORDER BY "+Purchase_Date + " DESC"); */

      //Sold_Price +" != ''" check
      // Cursor cur=db.rawQuery("Select * from Detail where Sold_Price IS NOT NULL",null);



//sort between dates
   //   Cursor cur = db.rawQuery("SELECT * FROM Detail WHERE sell_Date BETWEEN ?  AND ?", new String[]{date1, date2});

      cur.moveToFirst();

      while (cur.isAfterLast() == false) {
          det= new Details();
          det.set_Detail_Id(Integer.parseInt(cur.getString(0)));
          det.set_Product_Id(Integer.parseInt(cur.getString(1)));
          det.set_Brand_Id(Integer.parseInt(cur.getString(2)));
          det.set_Size_Id(Integer.parseInt(cur.getString(3)));
          det.set_Type_Id(Integer.parseInt(cur.getString(4)));
          det.set_Product_Code(cur.getString(5));
          det.set_Sold_Price(cur.getString(6));
          det.set_Cost_Price(cur.getString(7));
          det.set_Sold_DateTime(cur.getString(8));
          det.set_Purchase_DateTime(cur.getString(9));
          det.set_Profit(cur.getString(10));

          det.set_Creditor_Id(Integer.parseInt(cur.getString(12)));
          arrayList.add(det);
          cur.moveToNext();
      }
      cur.close();
      db.close();
      return arrayList;

  }


    public boolean checkIfProductExists(int productId) {
        SQLiteDatabase db = this.getReadableDatabase();
        String Query = "Select * from " + Detail_Table + " where " + D_Product_ID + " = " + productId;
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }
    public boolean checkIfBrandExists(int brandId) {
        SQLiteDatabase db = this.getReadableDatabase();
        String Query = "Select * from " + Detail_Table + " where " + D_Brand_ID + " = " + brandId;
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }
    public boolean checkIfSizeExists(int sizeId) {
        SQLiteDatabase db = this.getReadableDatabase();
        String Query = "Select * from " + Detail_Table + " where " + D_Size_ID + " = " +sizeId ;
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }
    public boolean checkIfTypeExists(int typeId) {
        SQLiteDatabase db = this.getReadableDatabase();
        String Query = "Select * from " + Detail_Table + " where " + D_Type_ID + " = " +typeId ;
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

   /* public ArrayList<Details> getdublicateDetailsWithCount() {
        ArrayList<Details> arrayList = new ArrayList<Details>();
        SQLiteDatabase db = this.getReadableDatabase();
        Details det;

        String table = "Detail";
        String[] columns = new String[] { "product_Id", "brand_Id", "size_Id","type_Id" };
        String selection = "id=? and date=?";
        String[] arguments = new String[] { "0", "Sep 15, 2015" };
        String groupBy = "id, date";
        String having = null;
        String orderBy = null;
        db.query(table, columns, selection, arguments, groupBy, having, orderBy);
        Cursor cur = db.rawQuery("SELECT D_Product_ID,D_Brand_ID,D_Type_ID,D_Size_ID COUNT(1) as CNT FROM" + Detail_Table + "GROUP BY	D_Product_ID, D_Brand_ID, D_Size_ID,D_Type_ID ");
        cur.moveToFirst();

        while (cur.isAfterLast() == false) {
            det= new Details();
            det.set_Detail_Id(Integer.parseInt(cur.getString(0)));
            det.set_Product_Id(Integer.parseInt(cur.getString(1)));
            det.set_Brand_Id(Integer.parseInt(cur.getString(2)));
            det.set_Size_Id(Integer.parseInt(cur.getString(3)));
            det.set_Type_Id(Integer.parseInt(cur.getString(4)));
            det.set_Product_Code(cur.getString(5));
            det.set_Sold_Price(cur.getString(6));
            det.set_Cost_Price(cur.getString(7));
            det.set_Sold_DateTime(cur.getString(8));
            det.set_Purchase_DateTime(cur.getString(9));
            det.set_Profit(cur.getString(10));

            arrayList.add(det);
            cur.moveToNext();
        }
        db.close();
        return arrayList;

    }*/



}
