package com.example.dinesh.hamropasal.CRUD;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.dinesh.hamropasal.dbModel.Brands;
import java.util.ArrayList;

/**
 * Created by ANONYMOUS_PAL on 7.7.2016.
 */
public class BrandHelper extends DBHelper {

    public BrandHelper(Context context) {
        super(context);
    }

    //@Override
    public long brandInsert(Brands brand) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Brand_Name,brand.get_Brand_Name());
        contentValues.put(Quantity,brand.get_Quantity());
        long result = db.insert(Brand_Table,null,contentValues);
        db.close();
        return result;
    }

    //@Override
    public long brandUpdate(Brands brand) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Brand_ID,brand.get_Brand_Id());
        contentValues.put(Brand_Name,brand.get_Brand_Name());
        contentValues.put(Quantity,brand.get_Quantity());
        long result = db.update(Brand_Table,contentValues, "brand_Id = ?", new String [] { String.valueOf(brand.get_Brand_Id())});
        db.close();
        return result;
    }

    //@Override
    public int brandDelete(Brands brand) {
        SQLiteDatabase db = this.getWritableDatabase();
        int result=  db.delete(Brand_Table,"brand_Id = ?", new String [] {String.valueOf(brand.get_Brand_Id())});
        db.close();
        return result;
    }

   public ArrayList<Brands> getAllBrands() {
    ArrayList<Brands> arrayList = new ArrayList<Brands>();
    SQLiteDatabase db = this.getReadableDatabase();
    Brands bra;
    Cursor cur = db.rawQuery("select * from "+Brand_Table, null);
    cur.moveToFirst();

    while (cur.isAfterLast() == false) {
        bra= new Brands();
        bra.set_Brand_Id(Integer.parseInt(cur.getString(0)));
        bra.set_Brand_Name(cur.getString(1));
        bra.set_Quantity(Integer.parseInt(cur.getString(2)));
        arrayList.add(bra);
        cur.moveToNext();
    }
    cur.close();
    db.close();
    return arrayList;

    }
}
